#!/bin/bash
#
# run_example1.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


# calculate haplotype likelihoods
../../bin/harp like --bam example1.bam --region 2L:30001-50000 --refseq dmel_chr2L.fasta --snps snps.txt --stem example1 -I
# or:
#../../bin/harp like -c harp_like.config


# estimate frequencies
../../bin/harp freq --hlk example1.hlk --region 2L:30001-50000
# or:
#../../bin/harp freq -c harp_freq.config


