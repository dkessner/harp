#!/bin/bash
#
# build_example1.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#

dest=../example1_single_reference

# clean
rm -r $dest
rm -f build.log build.err

# create simulated data (example1.sam)
simreads simreads.config >> build.log

# convert from SAM to BAM
samtools view -S -b example1.sam > example1.unsorted.bam 2>> build.err

# sort the BAM file
samtools sort example1.unsorted.bam example1

# index the BAM file -- this allows random access by region
samtools index example1.bam

# stage
mkdir -p $dest
cp run_example1.sh $dest
cp dmel_chr2L.fasta dmel_chr2L.fasta.fai $dest
cp harp_freq.config harp_like.config $dest
cp snps.txt snps.txt.idx $dest
mv example1.*.freqs $dest
mv example1.bam $dest
mv example1.bam.bai $dest
rm example1.sam example1.seed example1.unsorted.bam
