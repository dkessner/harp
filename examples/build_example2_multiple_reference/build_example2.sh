#!/bin/bash
#
# build_example2.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#

dest=../example2_multiple_reference

# clean
rm -r $dest
rm -f build.log build.err

# simulate reads:  example2.reads.fastq
simreads_raw example2.simreads_raw.config >> build.log

# ref?.fasta -> ref?.bam
./map_reads.sh >> build.log 2>> build.err

# stage
mkdir -p $dest
cp example2.reads.fastq example2.true.freqs example2.actual.freqs $dest
cp ref*.fasta ref*.bam $dest
cp run_example2.sh $dest
ls ref*.fasta > $dest/refseqlist.txt
ls ref*.bam > $dest/bamlist.txt


