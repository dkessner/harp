#!/bin/bash
#
# run_example2.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


# analyze read base quality scores for haplotype likelihood filtering
# output: example2.harp_like_multi.config, which contains haplotype likelihood filtering parameters
../../bin/qual_hist_fastq 75 example2.reads.fastq example2.harp_like_multi

# calculate haplotype likelihoods
../../bin/harp like_multi --refseqlist refseqlist.txt --bamlist bamlist.txt --stem example2 -c example2.harp_like_multi.config 

# estimate haplotype frequencies
../../bin/harp freq --hlk example2.hlk


