#!/bin/bash
#
# run_harp_pipeline_multi.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


source $HOME/.bash_profile


if [ "$SGE_TASK_ID" == "" ]
then
    echo '$SGE_TASK_ID not set'
    exit 1
fi

id=$SGE_TASK_ID
targets=$id.freqs

startdir=$(pwd)
workdir=$(echo $(pwd) | sed "s^$HOME^$SCRATCH^g")

pushd $workdir
make -f $startdir/harp_pipeline_multi.mak $targets
cp $id*.freqs $id.actual.unknown_freq $startdir
popd

