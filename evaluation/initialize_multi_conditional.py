#!/usr/bin/env python
#
# initialize_multi_conditional.py
#
# Darren Kessner
# Novembre Lab, UCLA
#

from __future__ import print_function
import sys
import os
import random
import subprocess
import numpy as np


class Config0:
    def __init__(self):

        # simreads config

        self.replicate_count = 1
        self.coverage = 100
        self.haplotype_frequencies = [0.25, 0.25, 0.25, 0.25]
        self.unknown_frequencies = [.5]
        self.logl_min_zscores = [-2]
        self.seed = 1

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config1:
    def __init__(self):

        # simreads config

        self.replicate_count = 100
        self.coverage = 100
        self.alpha = .2
        self.haplotype_count = 20
        self.unknown_frequencies = [.1*i for i in range(6)]
        self.logl_min_zscores = [-2]

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config2:
    def __init__(self):

        # simreads config

        self.replicate_count = 100
        self.coverage = 100
        self.adjust_coverage = True
        self.alpha = .2
        self.haplotype_count = 20
        self.unknown_frequencies = [.1*i for i in range(6)]
        self.logl_min_zscores = [-2]

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    



def draw_haplotype_frequencies(haplotype_count, alpha):
    p = np.random.mtrand.dirichlet(np.ones(haplotype_count) * alpha)
    return p


def write_config_files(config):

    filename_refseq_common = "16S_filtered_random_20_Clostridium.fasta"
    filename_refseq_unknown = "16S_filtered_random_480_no_clostridium.fasta"

    index = 0

    for replicate in range(config.replicate_count):

        if "seed" in dir(config): 
            seed = config.seed
            np.random.seed(seed)
        else:
            seed = random.getrandbits(32)

        if "alpha" in dir(config):
            haplotype_frequencies = draw_haplotype_frequencies(config.haplotype_count, alpha = config.alpha)
        else:
            haplotype_frequencies = config.haplotype_frequencies

        for unknown_frequency in config.unknown_frequencies:

            for logl_min_zscore in config.logl_min_zscores:

                index += 1

                # refseq

                filename_refseq = str(index) + '.refseq.fasta'
                subprocess.call(['cp', filename_refseq_common, filename_refseq]) 

                # simreads_raw config

                with open(str(index) + ".simreads_raw.config", 'w') as f:
                    print("filename_refseq", filename_refseq, file=f)
                    print("filename_refseq_unknown", filename_refseq_unknown, file=f)

                    print("haplotype_frequencies", end=" ", file=f)
                    for frequency in haplotype_frequencies: print(frequency, end=" ", file=f)
                    print(file=f)

                    print("unknown_frequency", unknown_frequency, file=f)
                    print("unknown_related_haplotype_index", "-1", file=f)
                    print("seed", seed, file=f)
                    print("filename_stem", index, file=f) # important for makefile: this must match filename

                    coverage = config.coverage
                    if config.adjust_coverage:
                        coverage /= (1 - unknown_frequency)
                    print("coverage", coverage, file=f)

                # harp like_multi config

                with open(str(index) + ".harp.like_multi.config", "w") as f:
                    print("logl_min_zscore =", logl_min_zscore, file=f)

                # harp freq config

                with open(str(index) + ".harp.freq.config", "w") as f:
                    print("em_iter =", config.em_iter, file=f)
                    print("em_converge =", config.em_converge, file=f)
                    print("em_min_freq_cutoff = ", config.em_min_freq_cutoff, file=f)


def main():
    configs = [Config0(), Config1(), Config2()]

    if len(sys.argv) != 2:
        print("Usage: initialize_multi_conditional.py n")
        sys.exit(0)

    config_index = int(sys.argv[1]) 
    write_config_files(configs[config_index])
    

if __name__ == '__main__':
    main()

