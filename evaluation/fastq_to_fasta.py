#!/usr/bin/env python
#
# fastq_to_fastq.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys


def main():
    if len(sys.argv) != 2:
        print("Usage: fastq_to_fasta.py filename")
        sys.exit(0)

    filename = sys.argv[1]
    
    for i,line in enumerate(open(filename)):
        if i%4==0: 
            print('>', line[1:], sep='', end='')
        elif i%4==1: 
            print(line, end='')


if __name__ == '__main__':
    main()

