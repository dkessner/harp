#!/usr/bin/env python
#
# initialize_potential_haplotypes.py
#
# Darren Kessner
# Novembre Lab, UCLA
#

from __future__ import print_function
import sys
import os
import random



class Config0:
    def __init__(self):

        # simreads config

        self.replicate_count = 5
        self.coverage = 200
        self.regions = ["2L:15000001-15200000"]
        self.haplotype_frequencies = [.2 for i in range(5)]
        self.haplotype_filters = ["1-"+str(count) for count in range(5,51,5)]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config1:
    def __init__(self):

        # simreads config

        self.replicate_count = 20
        self.coverage = 200
        self.regions = ["2L:15000001-15200000", "3L:11000001-11200000", "X:8000001-8200000"]
        self.haplotype_frequencies = [.35, .3, .2, .1, .05]
        self.haplotype_filters = ["1-"+str(count) for count in range(5,51,5)]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config2: # dummy, for EM iteration visualization
    def __init__(self):

        # simreads config

        self.replicate_count = 1
        self.coverage = 200
        self.regions = ["3L:11000001-11200000"] # ["2L:6000001-6200000"]
        self.haplotype_frequencies = [0, .1, 0, .2, 0, .3, 0, .4]
        self.haplotype_filters = ["1-10"]
        self.error_rate = 0.05

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    




def arm(region):
    if region.startswith("X"): return "X"
    else: return region[:2]


def write_config_files(config):

    home = os.environ["HOME"]
    filename_refseq = home + "/data/refseq/dmel-all-chromosome-r5.34.fasta"

    index = 0
    for region in config.regions:
        filename_snps = home + "/data/dgrp_snps/Variants_Sparse_" + arm(region) + ".sample_swap_fixed.txt"
        for haplotype_filter in config.haplotype_filters:
            for replicate in range(config.replicate_count):
                index += 1

                # simreads config

                filename = str(index) + ".simreads.config"
                f = open(filename, "w")
                print("filename_refseq", filename_refseq, file=f)
                print("filename_snps", filename_snps, file=f)
                print("region", region, file=f)
                print("haplotype_frequencies", end=" ", file=f)
                for frequency in config.haplotype_frequencies: print(frequency, end=" ", file=f)
                print(file=f)
                print("error_rate", config.error_rate, file=f)
                if "seed" in dir(config): 
                    print("seed", config.seed, file=f)
                else:
                    print("seed", random.getrandbits(32), file=f)
                print("filename_stem", index, file=f) # important for makefile: this must match filename
                print("coverage", config.coverage, file=f)
                f.close()

                # harp like config

                filename = str(index) + ".harp.like.config"
                f = open(filename, "w")
                print("refseq =", filename_refseq, file=f)
                print("snps =", filename_snps, file=f)
                print("region =", region, file=f)
                f.close()

                # harp freq config

                filename = str(index) + ".harp.freq.config"
                f = open(filename, "w")
                print("refseq =", filename_refseq, file=f)
                print("snps =", filename_snps, file=f)
                print("region =", region, file=f)
                print("em_iter =", config.em_iter, file=f)
                print("em_converge =", config.em_converge, file=f)
                print("em_min_freq_cutoff =", config.em_min_freq_cutoff, file=f)
                print("haplotype_filter =", haplotype_filter, file=f)
                f.close()


def main():
    configs = [Config0(), Config1(), Config2()]

    if len(sys.argv) != 2:
        print("Usage: initialize_potential_haplotypes.py n")
        sys.exit(0)

    config_index = int(sys.argv[1]) 
    write_config_files(configs[config_index])
    

if __name__ == '__main__':
    main()

