#
# harp_pipeline.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#



# $* stem of implicit rule match
# $@ target
# $< first prerequisite


dummy:
	@echo Usage: make -f harp_pipeline.mak filename.freqs [...]

%.freqs : %.hlk %.harp.freq.config
	harp freq -c $*.harp.freq.config --hlk $< --stem $*
	rm -r $*.output # comment for debugging

%.stringmatch.freqs : %.hlk %.harp.freq.config
	harp freq_stringmatch -c $*.harp.freq.config --stem $*.stringmatch --bam $*.bam
	rm -r $*.stringmatch.output # comment for debugging

%.likelihood.freqs: %.hlk %.harp.freq.config
	harp freq --em_iter 1 -c $*.harp.freq.config --hlk $< --stem $*.likelihood
	rm -r $*.likelihood.output # comment for debugging

%.hlk : %.bam.bai %.harp.like.config
	harp like -c $*.harp.like.config --bam $*.bam --stem $*

%.hippo.freqs : %.hippo
	cd $<; hippo $*.hippo_par
	hippo_to_freqs $*.hippo.config $*.hippo/results.out > $@

%.aeml.freqs : %.hippo
	cd $<; AEML $*.aeml_par
	hippo_to_freqs $*.hippo.config $*.hippo/AEML.out > $*.aeml.freqs

%.hippo : %.pileup %.hippo.config
	pileup_to_hippo $*.hippo.config $*.pileup $*.hippo

%.pileup : %.bam.bai
	samtools mpileup $*.bam > $@

%.bam.bai : %.bam
	samtools index $<

%.bam : %.unsorted.bam
	samtools sort $< $* ;\

%.unsorted.bam : %.sam
	samtools view -S -b $< > $@ ;\

%.sam : %.simreads.config     # note: assumes filename_stem is set to % in %.simreads.config
	simreads $<

clean:
	rm -f *.seed *.sam *.bam *.bam.bai *.hlk *.freqs
	rm -rf *.output *.hippo

# uncomment to preserve intermediate files (.sam, .bam, .hlk)
#.SECONDARY :


