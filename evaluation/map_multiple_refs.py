#!/usr/bin/env python
#
# map_multiple_refs.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import subprocess


filestem_ = 'ref'


def split_refseq(filename_refseq):
    index = 0
    with open(filename_refseq) as f:
        while True:
            id_tokens = f.readline().strip().split()
            sequence = f.readline().strip()
            if not sequence: break
            id = filestem_ + str(index) + '_' + id_tokens[2] + '_' + id_tokens[3]

            filename_fasta = filestem_ + str(index) + '.fasta'
            with open(filename_fasta, 'w') as f_fasta:
                print('>', id, sep='', file=f_fasta)
                print(sequence, file=f_fasta)

            index += 1
    return index


def run_alignments(filename_reads, refseq_count):
    with open("alignment.log", 'w') as f_log:
        with open("bamlist.txt", 'w') as f_bamlist:
            for index in range(refseq_count):
                filename_fasta = filestem_ + str(index) + '.fasta'
                subprocess.call(['bwa', 'index', filename_fasta], stderr=f_log)

                max_mismatch_count = 20 # liberal mapping
                filename_sai = filestem_ + str(index) + '.sai'
                with open(filename_sai, 'w') as f_sai:
                    subprocess.call(['bwa', 'aln', '-n', str(max_mismatch_count), filename_fasta, filename_reads], 
                            stdout=f_sai, stderr=f_log)

                filename_sam = filestem_ + str(index) + '.sam'
                with open(filename_sam, 'w') as f_sam:
                    subprocess.call(['bwa', 'samse', filename_fasta, filename_sai, filename_reads], 
                            stdout=f_sam, stderr=f_log)

                filename_bam = filestem_ + str(index) + '.bam'
                with open(filename_bam, 'w') as f_bam:
                    subprocess.call(['samtools', 'view', '-S', '-b', filename_sam], stdout=f_bam, stderr=f_log)

                print(filename_bam, file=f_bamlist)


def map_multiple_refs(filename_refseq, filename_reads):
    refseq_count = split_refseq(filename_refseq)
    print("refseqs:", refseq_count)
    run_alignments(filename_reads, refseq_count)


def main():
    if len(sys.argv) < 3:
        print("Usage: map_multiple_refs.py refseqs.fasta reads.fastq")
        sys.exit(0)

    filename_refseq = sys.argv[1]
    filename_reads = sys.argv[2]

    map_multiple_refs(filename_refseq, filename_reads)


if __name__ == '__main__':
    main()

