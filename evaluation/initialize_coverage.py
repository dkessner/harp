#!/usr/bin/env python
#
# initialize_coverage.py
#
# Darren Kessner
# Novembre Lab, UCLA
#

from __future__ import print_function
import sys
import os
import random
import numpy as np


class Config0:
    def __init__(self):

        # simreads config

        self.replicate_count = 1
        self.coverages = [25, 50]
        self.regions = ["2L:15000001-15100000"]
        self.haplotype_frequencies = [0.25, 0.25, 0.25, 0.25]
        self.error_rate = 0.066
        self.seed = 1

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config1:
    def __init__(self):

        # simreads config

        self.replicate_count = 10
        self.coverages = [25*i for i in range(1,13)]
        self.regions = ["2L:15000001-15400000"]
        self.haplotype_frequencies = [0.25, 0.25, 0.25, 0.25]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config2:
    def __init__(self):

        # simreads config

        self.replicate_count = 20
        self.coverages = [25*i for i in range(1,13)]
        self.regions = ["2L:15000001-15400000"]
        self.haplotype_frequencies = [0.3, 0.1, 0.1]
        for i in range(10): self.haplotype_frequencies.append(.05)
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config3:
    def __init__(self):

        # simreads config

        self.replicate_count = 100
        self.coverages = [25*i for i in range(1,13)]
        self.regions = ["2L:15000001-15025000", "2L:15000001-15050000", "2L:15000001-15100000", "2L:15000001-15200000", "2L:15000001-15400000"]
        self.haplotype_frequencies = [0.3, 0.1, 0.1]
        for i in range(10): self.haplotype_frequencies.append(.05)
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4



class Config4:
    def __init__(self):

        # simreads config

        self.replicate_count = 100
        self.coverages = [25*i for i in range(1,13)]
        self.regions = ["3R:11000001-11025000", "3R:11000001-11050000", "3R:11000001-11100000", "3R:11000001-11200000", "3R:11000001-11400000"]
        self.haplotype_frequencies = [0.3, 0.1, 0.1]
        for i in range(10): self.haplotype_frequencies.append(.05)
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config5: # check weirdness in 25kb sub-regions of 2L:15000001-15100000
    def __init__(self):

        # simreads config

        self.replicate_count = 20
        self.coverages = [25*i for i in range(1,13)]
        self.regions = ["2L:15000001-15025000", "2L:15025001-15050000", "2L:15050001-15075000", "2L:15075001-15100000"]
        self.haplotype_frequencies = [0.3, 0.1, 0.1]
        for i in range(10): self.haplotype_frequencies.append(.05)
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4


class Config6:
    def __init__(self):

        self.alpha = .2
        self.haplotype_count = 162

        # simreads config

        self.replicate_count = 100
        self.coverages = [25*i for i in range(1,13)]
        self.regions = ["3L:8000001-8025000", "3L:8000001-8050000", "3L:8000001-8100000", "3L:8000001-8200000", "3L:8000001-8400000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    



def arm(region):
    if region.startswith("X"): return "X"
    else: return region[:2]


def draw_haplotype_frequencies(haplotype_count, alpha):
    p = np.random.mtrand.dirichlet(np.ones(haplotype_count) * alpha)
    return p


def write_config_files(config):

    home = os.environ["HOME"]
    filename_refseq = home + "/data/refseq/dmel-all-chromosome-r5.34.fasta"

    index = 0
    for region in config.regions:
        filename_snps = home + "/data/dgrp_snps/Variants_Sparse_" + arm(region) + ".sample_swap_fixed.txt"
        for coverage in config.coverages:
            for replicate in range(config.replicate_count):
                index += 1

                if "alpha" in dir(config):
                    haplotype_frequencies = draw_haplotype_frequencies(config.haplotype_count, alpha = config.alpha)
                else:
                    haplotype_frequencies = config.haplotype_frequencies

                # simreads config

                filename = str(index) + ".simreads.config"
                f = open(filename, "w")
                print("filename_refseq", filename_refseq, file=f)
                print("filename_snps", filename_snps, file=f)
                print("region", region, file=f)
                print("haplotype_frequencies", end=" ", file=f)
                for frequency in haplotype_frequencies: print(frequency, end=" ", file=f)
                print(file=f)
                print("error_rate", config.error_rate, file=f)
                if "seed" in dir(config): 
                    print("seed", config.seed, file=f)
                else:
                    print("seed", random.getrandbits(32), file=f)
                print("filename_stem", index, file=f) # important for makefile: this must match filename
                print("coverage", coverage, file=f)
                f.close()

                # harp like config

                filename = str(index) + ".harp.like.config"
                f = open(filename, "w")
                print("refseq =", filename_refseq, file=f)
                print("snps =", filename_snps, file=f)
                print("region =", region, file=f)
                f.close()

                # harp freq config

                filename = str(index) + ".harp.freq.config"
                f = open(filename, "w")
                print("function = freq", file=f)
                print("refseq =", filename_refseq, file=f)
                print("snps =", filename_snps, file=f)
                print("region =", region, file=f)
                print("em_iter =", config.em_iter, file=f)
                print("em_converge =", config.em_converge, file=f)
                print("em_min_freq_cutoff = ", config.em_min_freq_cutoff, file=f)
                f.close()


def main():
    configs = [Config0(), Config1(), Config2(), Config3(), Config4(), Config5(), Config6()]

    if len(sys.argv) != 2:
        print("Usage: initialize_coverage.py n")
        sys.exit(0)

    config_index = int(sys.argv[1]) 
    write_config_files(configs[config_index])
    

if __name__ == '__main__':
    main()

