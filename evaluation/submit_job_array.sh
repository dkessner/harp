#!/bin/bash
#
# submit_job_array.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


taskid_low=1
taskid_high=$(ls *.simreads.config | wc -l)

if [ "$1" == "3" ]
then
    echo "Running EM, likelihood, stringmatch"
elif [ "$1" == "5" ] 
then
    echo "Running EM, likelihood, stringmatch, hippo, aeml"
else
    echo "Running EM only"
fi


startdir=$(pwd)
workdir=$(echo $(pwd) | sed "s^$HOME^$SCRATCH^g")

echo Ready to copy $startdir to $workdir
read

if [ -e $workdir ]
then
    echo "Warning: $workdir already exists -- nothing copied."
else
    mkdir -p $workdir
    cp * $workdir
fi


echo Ready to submit job array: $taskid_low $taskid_high
read

mkdir -p output
jobarray.q -jl $taskid_low -jh $taskid_high -ji 1 -e -t 2 -o output run_harp_pipeline.sh $*


