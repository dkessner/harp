#
# harp_pipeline_multi.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#



# $* stem of implicit rule match
# $@ target
# $< first prerequisite


dummy:
	@echo Usage: make -f harp_pipeline_multi.mak filename.freqs [...]

%.freqs : %.hlk %.harp.freq.config
	harp freq -c $*.harp.freq.config --hlk $< --region 16S:1-100000
	#rm -r $*.output # comment for debugging

%.hlk : %.bamfiles/bamlist.txt
	pushd $*.bamfiles ;\
	harp like_multi --refseq ../$*.refseq.fasta --bamlist bamlist.txt --hlk ../$*.hlk -c ../$*.harp.like_multi.config ;\
	popd

%.bamfiles/bamlist.txt : %.reads.fastq
	mkdir $*.bamfiles ;\
	pushd $*.bamfiles ;\
	../map_multiple_refs.py ../$*.refseq.fasta ../$< ;\
	popd

%.reads.fastq : %.simreads_raw.config     # note: assumes filename_stem is set to % in %.simreads.config
	simreads_raw $<

clean:
	rm -f *.seed *.sam *.bam *.bam.bai *.hlk *.freqs
	rm -rf *.output

# uncomment to preserve intermediate files (.sam, .bam, .hlk)
.SECONDARY :


