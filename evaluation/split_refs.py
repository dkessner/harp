#!/usr/bin/env python
#
# split_refs.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import subprocess


filestem_ = 'ref'


def split_refseq(filename_refseq):
    index = 1
    with open(filename_refseq) as f:
        while True:
            id = f.readline().strip()
            sequence = f.readline().strip()
            if not sequence: break

            filename_fasta = filestem_ + str(index) + '.fasta'
            with open(filename_fasta, 'w') as f_fasta:
                print(id, file=f_fasta)
                print(sequence, file=f_fasta)

            index += 1
    return index - 1


def main():
    if len(sys.argv) < 2:
        print("Usage: split_refs.py refseqs.fasta")
        sys.exit(0)

    filename_refseq = sys.argv[1]

    refseq_count = split_refseq(filename_refseq)
    print("refseqs:", refseq_count)


if __name__ == '__main__':
    main()

