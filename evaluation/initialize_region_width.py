#!/usr/bin/env python
#
# initialize_region_width.py
#
# Darren Kessner
# Novembre Lab, UCLA
#

from __future__ import print_function
import sys
import os
import random
import numpy as np


class Config0:
    def __init__(self):

        self.haplotype_count = 100
        self.alpha = .05

        # simreads config

        self.replicate_count = 20
        self.coverage = 200
        self.regions = ["3R:11000001-11000500", "3R:11000001-11001000", "3R:11000001-11002000", "3R:11000001-11005000", "3R:11000001-11010000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config1:
    def __init__(self):

        self.haplotype_count = 100
        self.alpha = .05

        # simreads config

        self.replicate_count = 100
        self.coverage = 200
        self.regions = ["3R:11000001-11000500", "3R:11000001-11001000", "3R:11000001-11002000", "3R:11000001-11005000", "3R:11000001-11010000", "3R:11000001-11020000", "3R:11000001-11050000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config2:
    def __init__(self):

        self.haplotype_count = 100
        self.alpha = .05

        # simreads config

        self.replicate_count = 100
        self.coverage = 200
        self.regions = ["3L:8000001-8000500", "3L:8000001-8001000", "3L:8000001-8002000", "3L:8000001-8005000", "3L:8000001-8010000", "3L:8000001-8020000", "3L:8000001-8050000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config3:
    def __init__(self):

        self.haplotype_count = 20
        self.alpha = .2

        # simreads config

        self.replicate_count = 100
        self.coverage = 200
        self.regions = ["3L:8000001-8000500", "3L:8000001-8001000", "3L:8000001-8002000", "3L:8000001-8005000", "3L:8000001-8010000", "3L:8000001-8020000", "3L:8000001-8050000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config4:
    def __init__(self):

        self.haplotype_count = 20
        self.alpha = .2

        # simreads config

        self.replicate_count = 100
        self.coverage = 200
        self.regions = ["3L:8000001-8000500", "3L:8000001-8001000", "3L:8000001-8002000", "3L:8000001-8005000", "3L:8000001-8010000", "3L:8000001-8020000", "3L:8000001-8050000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    
        self.em_random_start_count = 10
        self.em_random_start_alpha = .2


class Config5:
    def __init__(self):

        self.haplotype_count = 162
        self.alpha = .2

        # simreads config

        self.replicate_count = 100
        self.coverage = 200
        self.regions = ["3L:8000001-8000500", "3L:8000001-8001000", "3L:8000001-8002000", "3L:8000001-8005000", "3L:8000001-8010000", "3L:8000001-8020000", "3L:8000001-8050000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config6:
    def __init__(self):

        self.haplotype_count = 162
        self.alpha = .2

        # simreads config

        self.replicate_count = 100
        self.coverage = 200
        self.regions = ["3R:11000001-11000500", "3R:11000001-11001000", "3R:11000001-11002000", "3R:11000001-11005000", "3R:11000001-11010000", "3R:11000001-11020000", "3R:11000001-11050000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


def arm(region):
    if region.startswith("X"): return "X"
    else: return region[:2]


def draw_haplotype_frequencies(haplotype_count, alpha):
    p = np.random.mtrand.dirichlet(np.ones(haplotype_count) * alpha)
    #print("blah:", alpha, np.sum(-p*np.log(p)))
    return p


def write_config_files(config):

    home = os.environ["HOME"]
    filename_refseq = home + "/data/refseq/dmel-all-chromosome-r5.34.fasta"

    index = 0
    for region in config.regions:
        filename_snps = home + "/data/dgrp_snps/Variants_Sparse_" + arm(region) + ".sample_swap_fixed.txt"

        for replicate in range(config.replicate_count):
            index += 1

            haplotype_frequencies = draw_haplotype_frequencies(config.haplotype_count, alpha = config.alpha)
            haplotype_filter = "1-" + str(config.haplotype_count)

            # simreads config

            filename = str(index) + ".simreads.config"
            f = open(filename, "w")
            print("filename_refseq", filename_refseq, file=f)
            print("filename_snps", filename_snps, file=f)
            print("region", region, file=f)

            print("haplotype_frequencies", end=" ", file=f)
            for frequency in haplotype_frequencies: print(frequency, end=" ", file=f)
            print(file=f)

            print("error_rate", config.error_rate, file=f)
            if "seed" in dir(config): 
                print("seed", config.seed, file=f)
            else:
                print("seed", random.getrandbits(32), file=f)
            print("filename_stem", index, file=f) # important for makefile: this must match filename
            print("coverage", config.coverage, file=f)
            f.close()

            # harp like config

            filename = str(index) + ".harp.like.config"
            f = open(filename, "w")
            print("refseq =", filename_refseq, file=f)
            print("snps =", filename_snps, file=f)
            print("region =", region, file=f)
            f.close()

            # harp freq config

            filename = str(index) + ".harp.freq.config"
            f = open(filename, "w")
            print("refseq =", filename_refseq, file=f)
            print("snps =", filename_snps, file=f)
            print("region =", region, file=f)
            print("em_iter =", config.em_iter, file=f)
            print("em_converge =", config.em_converge, file=f)
            print("em_min_freq_cutoff =", config.em_min_freq_cutoff, file=f)
            print("haplotype_filter =", haplotype_filter, file=f)
            print("max_mismatch = 2", file=f)
            if "em_random_start_count" in dir(config):
                print("em_random_start_count =", config.em_random_start_count, file=f) 
                print("em_random_start_seed =", random.getrandbits(32), file=f) 

            f.close()

            # hippo config

            filename = str(index) + ".hippo.config"
            f = open(filename, "w")
            print("filename_snps", filename_snps, file=f)
            print("filename_stem", index, file=f)
            print("region", region, file=f)
            print("haplotype_count", config.haplotype_count, file=f)
            print("poolsize 1000", file=f)


def main():
    configs = [Config0(), Config1(), Config2(), Config3(), Config4(), Config5(), Config6()]

    if len(sys.argv) != 2:
        print("Usage: initialize_region_width.py n")
        sys.exit(0)

    config_index = int(sys.argv[1]) 
    write_config_files(configs[config_index])
    

if __name__ == '__main__':
    main()

