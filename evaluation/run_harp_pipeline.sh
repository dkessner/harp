#!/bin/bash
#
# run_harp_pipeline.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


source $HOME/.bash_profile


if [ "$SGE_TASK_ID" == "" ]
then
    echo '$SGE_TASK_ID not set'
    exit 1
fi

id=$SGE_TASK_ID

if [ "$1" == "3" ]
then
    targets="$id.freqs $id.likelihood.freqs $id.stringmatch.freqs"
elif [ "$1" == "5" ] 
then
    targets="$id.freqs $id.likelihood.freqs $id.stringmatch.freqs $id.hippo.freqs $id.aeml.freqs"
else
    targets=$id.freqs
fi

startdir=$(pwd)
workdir=$(echo $(pwd) | sed "s^$HOME^$SCRATCH^g")

#mkdir -p $workdir
#cp $id.* $workdir

pushd $workdir
make -f $startdir/harp_pipeline.mak $targets
cp $id*.freqs $startdir
popd

