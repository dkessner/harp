//
// harp_misc.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _HARP_MISC_
#define _HARP_MISC_


#include "Region.hpp"
#include <vector>
#include <string>
#include "boost/random/mersenne_twister.hpp"
#include "boost/random/gamma_distribution.hpp"
#include "boost/numeric/ublas/vector.hpp"


namespace harp_misc {


std::vector<bool> parse_haplotype_filter_string(size_t haplotype_count, const std::string& haplotype_filter_string);

Region get_extended_region(const Region& region, size_t left_buffer_size, size_t right_buffer_size);


class SymmetricDirichlet
{
    public:

    SymmetricDirichlet(double alpha, size_t n, size_t seed)
    :   n_(n), dist_(alpha, 1.0)
    {
        gen_.seed(seed);
    }

    boost::numeric::ublas::vector<double> operator()()
    {
        boost::numeric::ublas::vector<double> result(n_, 0);
        for (size_t i=0; i<n_; ++i) result[i] = dist_(gen_);
        result /= sum(result);    
        return result;
    }

    private:
    size_t n_;
    boost::mt19937 gen_;
    boost::gamma_distribution<> dist_;
};


} // namespace harp_misc


#endif // _HARP_MISC_


