//
// harp_like.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "harp_misc.hpp"
#include "HaplotypeLikelihoodCalculator.hpp"
#include "HaplotypeLikelihoodRecord.hpp"
#include "BAMFile.hpp"
#include "boost/lambda/lambda.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include <iostream>
#include <iomanip>
#include <iterator>
#include <cmath>
#include <numeric>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
using namespace bampp;
using namespace boost::lambda;
namespace bfs = boost::filesystem;


namespace {


vector<double> vector_sum(const vector<double>& a, const vector<double>& b)
{
    if (a.size() != b.size()) throw runtime_error("[harp_like] Vector sizes differ");
    vector<double> result(a.size());
    transform(a.begin(), a.end(), b.begin(), result.begin(), _1 + _2);
    return result;
}


void sum_assign(vector<double>& result, vector<double>& summand)
{
    if (result.empty()) result.resize(summand.size());

    for (vector<double>::iterator it=result.begin(), jt=summand.begin(); it!=result.end(); ++it, ++jt)
        *it += *jt;
}


class PairLikelihoodProcessor : public bampp::PairProcessor
{
    public:

    PairLikelihoodProcessor(const harp_config::Config& config,
                            shared_ptr<HaplotypeLikelihoodCalculator> calculator,
                            shared_ptr<HaplotypeLikelihoodRecords> records,
                            const bfs::path& outdir)
    :   region_(config.region),
        min_mapping_quality_(config.min_mapping_quality),
        calculator_(calculator),
        records_(records),
        outdir_(outdir),
        os_filtered_(outdir_ / "filtered.txt"),
        count_pairs_(0), count_pairs_passed_filter_(0), count_unpaired_(0)
    {}

    virtual void process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2);

    void report_summary(ostream& os) const;
    void report_pair_distances(ostream& os) const;

    private:

    Region region_; // copy
    const int min_mapping_quality_;
    shared_ptr<HaplotypeLikelihoodCalculator> calculator_;
    shared_ptr<HaplotypeLikelihoodRecords> records_;
    bfs::path outdir_;

    // output streams

    mutable bfs::ofstream os_filtered_;    

    // counts

    //public:

    size_t count_pairs_;
    size_t count_pairs_passed_filter_;
    size_t count_unpaired_;

    private:

    bool passes_filter(const Read& r) const;
    bool passes_start_position_filter(const Read& r) const;
};


bool PairLikelihoodProcessor::passes_filter(const Read& r) const
{
    if (r.mapping_quality < min_mapping_quality_)
    {
        os_filtered_ << r.name << " " << r.position << "\tmapping_quality " << r.mapping_quality << endl;
        return false;
    }

    if (r.cigar.size() != 1 || 
        r.cigar[0].op!='M' || 
        r.cigar[0].count != r.sequence.size())
    {
        os_filtered_ << r.name << " " << r.position << "\tcigar " << r.cigar << endl;
        return false;
    }

    return true;
}


bool PairLikelihoodProcessor::passes_start_position_filter(const Read& r) const
{
    // note: this filter is to prevent duplicates when computing likelihoods in sliding windows;
    //       samtools api returns all reads overlapping a region -- we restrict to read pairs where
    //       the first read start position is in the region

    if (!region_.contains(r.position))
    {
        os_filtered_ << r.name << " " << r.position << "\tfirst read start position not in region\n";
        return false;
    }

    return true;
}


void PairLikelihoodProcessor::report_summary(ostream& os) const
{
    os << "reads: " << count_pairs_*2 + count_unpaired_ << endl
        << "pairs: " << count_pairs_ << endl
        << "pairs_passed_filter: " << count_pairs_passed_filter_ << endl
        << "unpaired: " << count_unpaired_ << endl;
}


void PairLikelihoodProcessor::report_pair_distances(ostream& os) const
{
    for (HaplotypeLikelihoodRecords::const_iterator it=records_->begin(); it!=records_->end(); ++it)
        os << (*it)->read2_begin - (*it)->read1_begin << endl;
}


void PairLikelihoodProcessor::process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2)
{
    if (!r1.get()) throw runtime_error("[harp_like] Inconceivable: r1 is null.");

    if (!r2.get()) 
    {
        ++count_unpaired_;

        //os_filtered_ << r1->name << " " << r1->position << "\tunpaired\n";
        //return;
    }

    ++count_pairs_; 

    bool ok_1 = passes_filter(*r1) && passes_start_position_filter(*r1);
    bool ok_2 = r2.get() ? passes_filter(*r2) : true;

    if (!ok_1 || !ok_2) return;

    try 
    {
        vector<double> logl1 = calculator_->likelihood_variant(*r1);

        vector<double> logl2(logl1.size());
        if (r2.get()) logl2 = calculator_->likelihood_variant(*r2);

        ++count_pairs_passed_filter_;

        HaplotypeLikelihoodRecordPtr record(new HaplotypeLikelihoodRecord);
        records_->push_back(record);
        record->read1_begin = r1->position;
        record->read1_end = r1->position + r1->sequence.size();
        record->read2_begin = r2.get() ? r2->position : 0;
        record->read2_end = r2.get() ? r2->position + r2->sequence.size() : 0;
        record->read1_logl = logl1;
        record->read2_logl = logl2;
    }
    catch (exception& e)
    {
        cerr << "[harp_like] Warning: Exception caught in PairLikelihoodProcessor::process_pair().\n"
             << "\t" << e.what() << endl
             << "\tIgnoring read pair: " << r1->name << " " << r1->position;
        if (r2.get()) cerr << " " << r2->position;
        cerr << endl;

        os_filtered_ << r1->name << " " << r1->position << "\texception caught in harp_like::PairLikelihoodProcessor::process_pair()\n";

        // for debugging
        //   previous issue: occasional Illumina read with invalid base quality score 3
        //   caused exception to be thrown from BaseQualityInterpreter during validation
        //
        //string illumina_quality = bampp::quality_to_illumina(r1->quality);
        //copy(illumina_quality.begin(), illumina_quality.end(), ostream_iterator<int>(cout, " "));
        //throw e;
    }
}


int go(const harp_config::Config& config)
{
    if (config.dirname_output.empty())
        throw runtime_error("[harp_like] Empty dirname_output.");

    bfs::path outdir = bfs::path(config.dirname_output) / "like";
    bfs::create_directories(outdir);

    bfs::ofstream os_config(outdir / "config.txt");
    os_config << config;
    os_config.close();

    // We use an extended region for the reference sequence (to include reads that extend outside of it)
    // and for the bam read requests (to include the 2nd read from read pairs that begin in the region).
    // However, we use the exact region for filtering the read pairs.

    Region extended_region = harp_misc::get_extended_region(config.region, 200, 2000);

    // allocate objects and process reads

    cout << "[harp_like] Initializing.\n";

    shared_ptr<HaplotypeReference> hapref(
        new HaplotypeReference(config.filename_refseq,
                               config.filename_snps,
                               extended_region));

    shared_ptr<BaseQualityInterpreter> bqi(new BaseQualityInterpreter_Illumina(config.filename_bqi));

    shared_ptr<HaplotypeLikelihoodCalculator> calculator(
            new HaplotypeLikelihoodCalculator(hapref, bqi, config.illumina_base_quality_encoding));

    shared_ptr<HaplotypeLikelihoodRecords> records(new HaplotypeLikelihoodRecords(hapref->haplotype_count()));
    PairLikelihoodProcessor processor(config, calculator, records, outdir);

    cout << "[harp_like] Iterating through reads.\n";

    BAMFile bamfile(config.filename_bam);
    bampp::iterate_pairs(bamfile, extended_region, processor);
    
    // sort records -- note that order will not match order of other output streams (e.g. names)

    sort(records->begin(), records->end(), HaplotypeLikelihoodRecord_Read1Begin());

    // write records to hlk file

    cout << "[harp_like] Writing records.\n";

    bfs::ofstream os_records(config.filename_hlk);
    records->write(os_records);
    os_records.close();

    // write summary

    bfs::ofstream os_summary(outdir / "summary.txt");
    os_summary << "region: " << config.region << endl
        << "filename_bam: " << config.filename_bam << endl
        << "filename_refseq: " << config.filename_refseq << endl
        << "filename_snps: " << config.filename_snps << endl << endl;
    processor.report_summary(os_summary);
    os_summary.close();

    /*
    // write pair distances

    bfs::ofstream os_pair_distances(bfs::path(outdir) / "pair_distances.txt");
    processor.report_pair_distances(os_pair_distances);
    os_pair_distances.close();
    */

    return 0;
}


int harp_like(const harp_config::Config& config)
{
    if (config.filename_bam.empty())
        throw runtime_error("[harp_like] No BAM filename specified.");

    if (config.region.id.empty())
        throw runtime_error("[harp_like] No region specified.");

    if (config.filename_hlk.empty())
        throw runtime_error("[harp_like] This isn't happening -- filename_hlk empty.");

    if (config.filename_refseq.empty())
        throw runtime_error("[harp_like] No refseq specified.");

    if (config.filename_snps.empty())
        throw runtime_error("[harp_like] No SNP file specified.");

    return go(config);
}


string harp_like_info()
{
    return "calculate haplotype likelihoods (create .hlk file)";
}


string harp_like_usage()
{
    ostringstream oss;
    oss << "harp like required parameters:\n";
    oss << "  bam\n";
    oss << "  region\n";
    oss << "  refseq\n";
    oss << "  snps\n";
    return oss.str();
}


} // namespace


harp_config::JumpTableEntry entry_harp_like_(harp_like, harp_like_info, harp_like_usage);



