//
// BaseQualityGenerator.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BaseQualityGenerator.hpp"
#include "PiecewiseUniformDistribution.hpp"
#include "BaseQualityInterpreter.hpp"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <iostream>
#include <cmath>
#include <vector>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include <tr1/memory>
#include <tr1/functional>


using namespace std;
using std::tr1::shared_ptr;


namespace {
const size_t score_count_ = 41; // [0,40]
const size_t valid_score_count_ = score_count_ - 3; // 2 == bad 'B', [4,40] good
const size_t position_count_ = 100;
} // namespace


extern double empirical_qual_dist_[score_count_][position_count_];


namespace {


vector<size_t> create_illumina_valid_scores()
{
    vector<size_t> result;
    for (size_t i=40; i>=4; --i) result.push_back(i);
    result.push_back(2);
    if (result.size() != valid_score_count_) throw runtime_error("[BaseQualityGenerator: create_illumina_valid_scores()] I am insane!");
    return result;
}


} // namespace


//
// BaseQualityGenerator::Impl
//


struct BaseQualityGenerator::Impl
{
    double error_rate;                      // client-specified error rate

    vector<size_t> illumina_valid_scores;   // valid quality scores, sorted:  40, 39, ..., 5, 4, 2  (38 values)
    vector<double> illumina_error_probabilities;                // vector of error probabilities corresponding to illumina_valid_scores

    QualScoreDistributions qual_score_distributions; // per-position, 0-40

    boost::mt19937 gen;
    typedef shared_ptr< boost::random::discrete_distribution<> > DistPtr;
    vector<DistPtr> dists;

    Impl(double _error_rate, unsigned int seed);

    void initialize_distributions(double empirical_qual_dist[score_count_][position_count_]);
};


BaseQualityGenerator::Impl::Impl(double _error_rate, unsigned int seed)
:   error_rate(_error_rate),
    illumina_valid_scores(create_illumina_valid_scores()),
    illumina_error_probabilities(valid_score_count_ + 1)
{
    if (error_rate == 0.0) return;

    BaseQualityInterpreter_Illumina bqi;
    transform(illumina_valid_scores.begin(), illumina_valid_scores.end(), illumina_error_probabilities.begin()+1, 
            tr1::bind(&BaseQualityInterpreter_Illumina::error_probability, tr1::ref(bqi), tr1::placeholders::_1));
    initialize_distributions(empirical_qual_dist_);

    gen.seed(seed);
    for (QualScoreDistributions::const_iterator it=qual_score_distributions.begin(); it!=qual_score_distributions.end(); ++it)
        dists.push_back(DistPtr(new boost::random::discrete_distribution<>(*it)));
}


namespace {

typedef vector< shared_ptr<PiecewiseUniformDistribution> > Distributions;

double calculate_distributions_mean(const Distributions& distributions)
{
    double sum_distribution_means = 0.;
    for (Distributions::const_iterator it=distributions.begin(); it!=distributions.end(); ++it)
    {
        // cout << "dist " << it-distributions.begin() << " " << (*it)->mean() << endl;
        sum_distribution_means += (*it)->mean();
    }
    return sum_distribution_means / position_count_;
}

} // namespace


void BaseQualityGenerator::Impl::initialize_distributions(
    double empirical_qual_dist[score_count_][position_count_])
{

    // create empirical distributions from qual score distribution array

    Distributions empirical_distributions;  // distribution per position

    double sum_means = 0.;

    for (size_t position=0; position<position_count_; ++position)
    {
        vector<double> p(valid_score_count_ + 1);
        for (size_t i=1; i<=valid_score_count_; ++i)
        {
            size_t score = illumina_valid_scores[i-1];
            p[i] = empirical_qual_dist_[score][position];
        }

        shared_ptr<PiecewiseUniformDistribution> d(new PiecewiseUniformDistribution(illumina_error_probabilities, p));
        empirical_distributions.push_back(d);
        sum_means += d->mean();
    }

    double empirical_error_rate = sum_means/position_count_;

    //cout << "empirical means: ";
    //for (Distributions::const_iterator it=empirical_distributions.begin(); it!=empirical_distributions.end(); ++it)
    //    cout << (*it)->mean() << " ";
    //cout << endl;
    //cout << "empirical_error_rate: " << empirical_error_rate << endl;

    // shift empirical distributions to match client-specified error_rate

    double target_factor = error_rate / empirical_error_rate;

    Distributions target_distributions;

    // note: we calculate distribution means assuming piecewise uniform;
    // our largest bin is between illumina qual score 4 (.398) and score 2 (.75),
    // so the highest mean we can get is max_target_mean == the midpoint of [.398, .75]

    const double max_target_mean = .57405; 

    for (size_t position=0; position<position_count_; ++position)
    {
        const PiecewiseUniformDistribution& d = *empirical_distributions[position];
        double target_mean = d.mean() * target_factor;

        vector<double> p_new(valid_score_count_ + 1);

        if (target_mean < max_target_mean)
        {
            p_new = d.iterate_p_new(target_mean);
        }
        else
        {
            p_new.back() = 1.; // always qual score 2 'B'
        }

        shared_ptr<PiecewiseUniformDistribution> d_new(new PiecewiseUniformDistribution(illumina_error_probabilities, p_new));
        target_distributions.push_back(d_new);
    }

    // hack: clipping of distributions causes us to fall short of our overall target error rate;
    // we start at the last position and push each distribution to/toward the max_target_mean
    
    const double diff_tolerance = 1e-4;
    double diff = error_rate - calculate_distributions_mean(target_distributions);

    if (fabs(diff) > diff_tolerance)
    {
        //cout << "diff: " << diff << endl;
        if (diff < 0.) throw runtime_error("I don't know what's going on.");

        size_t position = position_count_;

        while (diff > 0)
        {
            --position;

            const PiecewiseUniformDistribution& d = *target_distributions[position];
            double current_mean = d.mean();

            //cout << "current: " << position << " " << current_mean << " " << fabs(current_mean-max_target_mean) << endl;

            if (fabs(current_mean-max_target_mean) < 1e-5) continue;
            
            double target_mean = current_mean + diff * position_count_;

            vector<double> p_new(valid_score_count_ + 1);

            if (target_mean < max_target_mean)
            {
                p_new = d.iterate_p_new(target_mean);
                diff -= (target_mean-current_mean) / position_count_;
            }
            else
            {
                p_new.back() = 1.; // always qual score 2 'B'
                diff -= (max_target_mean-current_mean) / position_count_;
            }

            shared_ptr<PiecewiseUniformDistribution> d_new(new PiecewiseUniformDistribution(illumina_error_probabilities, p_new));
            //cout << "replacing target distribution: " << position << " " << d.mean() << " " << d_new->mean() << endl;
            target_distributions[position] = d_new;

            if (position == 0) break;
        }
    }

    // verify that our error rate matches the client-specified error rate

    double distributions_mean = calculate_distributions_mean(target_distributions);
    if (fabs(distributions_mean - error_rate) > 1e-4)
        throw runtime_error("[BaseQualityGenerator::initialize_distributions()] Failed to match error rate.");

    // initialize qual_score_distributions

    qual_score_distributions.resize(position_count_);
    for (size_t position=0; position<position_count_; ++position)
    {
        vector<double>& q = qual_score_distributions[position];
        q.resize(score_count_); // [0,40]
        const PiecewiseUniformDistribution& d = *target_distributions[position];

        // note: this line produces memory free bug on panga & hoffman
        // for now, use for loop below
        //copy(d.p().rbegin()+1, d.p().rend(), q.begin()+4); // copy distribution for scores 4,...,40

        for (size_t i=0; i<37; i++)
            q[4+i] = d.p()[valid_score_count_ - 1 - i];

        q[2] = d.p().back(); // copy P(score==2)
    }

    // print distributions

    /*
    for (size_t position=0; position<position_count_; ++position)
    {
        vector<double>& q = qual_score_distributions[position];
        copy(q.begin(), q.end(), ostream_iterator<double>(cout, " "));
        cout << endl;
    }
    */
}


//
// BaseQualityGenerator
//


BaseQualityGenerator::BaseQualityGenerator(double error_rate, unsigned int seed)
:   impl_(new Impl(error_rate, seed))
{}


char BaseQualityGenerator::random_quality(size_t position) const
{
    if (impl_->error_rate == 0.0) return 40; // max illumina score

    if (position >= position_count_)
        throw runtime_error("[BaseQualityGenerator::operator()] Index out of bounds.");

    return (*impl_->dists[position])(impl_->gen);
}


const BaseQualityGenerator::QualScoreDistributions& BaseQualityGenerator::qual_score_distributions() const
{
    return impl_->qual_score_distributions;
}


