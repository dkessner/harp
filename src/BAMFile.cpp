//
// BAMFile.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BAMFile.hpp"
#include "samtools/bam.h"
#include "boost/lambda/lambda.hpp"
#include <tr1/memory>
#include <tr1/tuple>
#include <iterator>
#include <cmath>


using namespace std;
using tr1::shared_ptr;
using namespace boost::lambda; // for _1


namespace bampp {


string quality_to_ascii(const string& quality)
{
    string ascii;
    transform(quality.begin(), quality.end(), back_inserter(ascii), _1 + 33);
    return ascii;
}


std::string quality_to_illumina(const std::string& quality)
{
    string ascii;
    transform(quality.begin(), quality.end(), back_inserter(ascii), _1 - 31);
    return ascii;
}


double illumina_error_probability(char q)
{
    if (q < 0 || q > 41)
        throw runtime_error("[BAMFile::illumina_error_probability()] Invalid Illumina quality score.");
    if (q == 0 || q == 1 || q == 3) return 0; // Illumina unused
    if (q == 2) return .75; // Illumina 'B'
    return pow(10, -q/10.);
}


ostream& operator<<(ostream& os, const Read::CIGAR& cigar)
{
    os << cigar.count << cigar.op;
    return os;
}


ostream& operator<<(ostream& os, const vector<Read::CIGAR>& cigar)
{
    if (cigar.empty()) 
        os << "*";
    else
        copy(cigar.begin(), cigar.end(), ostream_iterator<Read::CIGAR>(os, ""));
    return os;
}


ostream& operator<<(ostream& os, const Read& read)
{
    os << read.name << "\t"
       << read.flags << "\t"
       << read.reference_name << "\t"
       << read.position + 1 << "\t"
       << read.mapping_quality << "\t"
       << read.cigar << "\t"
       << (read.next_reference_name == read.reference_name ? "=" : read.next_reference_name) << "\t"
       << read.next_position + 1 << "\t"
       << read.template_length << "\t"
       << read.sequence << "\t"
       << quality_to_ascii(read.quality)
       ;

    return os;
}
 

} // namespace bampp


namespace {


using namespace bampp;


string get_sequence(const bam1_t* b)
{
    const char* translation = "=ACMGRSVTWYHKDBN";
    size_t length = b->core.l_qseq;
    string result(length, '\0');
    uint8_t* s = bam1_seq(b);
    for (size_t i=0; i<length; i++)
        result[i] = translation[bam1_seqi(s, i)];
    return result;
}


Read::CIGAR bam_cigar_element_to_CIGAR(uint32_t u)
{
    const char* translation = "MIDNSHP=X";
    Read::CIGAR cigar;
    cigar.op = translation[u & 0xf];
    cigar.count = u >> 4;
    return cigar;
}


shared_ptr<Read> translate(const bam1_t* b, const vector<string>& reference_names)
{
    shared_ptr<Read> read(new Read);
    read->name = reinterpret_cast<const char*>(b->data);
    read->flags = b->core.flag;
    if (b->core.tid >= 0 && b->core.tid < (int)reference_names.size())
        read->reference_name = reference_names[b->core.tid];
    read->position = b->core.pos;
    read->mapping_quality = b->core.qual;
    transform(bam1_cigar(b), bam1_cigar(b)+b->core.n_cigar, back_inserter(read->cigar), bam_cigar_element_to_CIGAR);
    read->next_reference_name = (b->core.mtid == -1) ? "*" : reference_names[b->core.mtid];
    read->next_position = b->core.mpos;
    read->template_length = b->core.isize;
    read->sequence = get_sequence(b);
    read->quality = string(reinterpret_cast<const char*>(bam1_qual(b)), b->core.l_qseq);
    return read; 
}


// callback signature for bam_fetch():
//   typedef int (*bam_fetch_f)(const bam1_t *b, void *data);


typedef tr1::tuple<const BAMFile&, ReadProcessor&> RefTuple;


int callback(const bam1_t* b, void* data)
{
    using tr1::get;
    RefTuple* t = reinterpret_cast<RefTuple*>(data);
    shared_ptr<Read> read = translate(b, get<0>(*t).reference_names()); 
    return get<1>(*t).process_read(read);
}


} // namespace


namespace bampp {


//
// BAMFile
//


struct BAMFile::Impl
{
    shared_ptr<BGZF> bf;
    shared_ptr<bam_index_t> bai;
    shared_ptr<bam_header_t> header;
    vector<string> reference_names; 

    Impl(const string& filename)
    {
        bf = shared_ptr<BGZF>(bam_open(filename.c_str(), "r"), bgzf_close);
        if (!bf.get()) throw runtime_error(("[BAMFile] Unable to open file " + filename).c_str());

        bai = shared_ptr<bam_index_t>(bam_index_load(filename.c_str()), bam_index_destroy);
        if (!bai.get()) throw runtime_error(("[BAMFile] Unable to open index for file " + filename).c_str());

        header = shared_ptr<bam_header_t>(bam_header_read(bf.get()), bam_header_destroy);
        if (!header.get()) throw runtime_error("[BAMFile] Error reading header.");
        copy(header->target_name, header->target_name+header->n_targets, back_inserter(reference_names));

        // note: shared_ptrs call cleanup functions when scope ends:
        //   bam_header_destroy(header);
        //   bam_index_destroy(bai);
        //   bam_close(bf);
    }
};


BAMFile::BAMFile(const string& filename)
:   impl_(new BAMFile::Impl(filename))
{}


const vector<string>& BAMFile::reference_names() const
{
    return impl_->reference_names;
}


void BAMFile::process_reads(const Region& region, ReadProcessor& processor) const
{
    int tid=0, begin=0, end=0;
    bam_parse_region(impl_->header.get(), region.str().c_str(), &tid, &begin, &end);

    RefTuple data(*this, processor);
    bam_fetch(impl_->bf.get(), impl_->bai.get(), tid, begin, end, 
              reinterpret_cast<void*>(&data), callback);
}


} // namespace bampp


//
// iterate_pairs()
//


namespace {


struct PairMatcher : public ReadProcessor
{
    const BAMFile& bf; 
    const Region& region;
    PairProcessor& processor;

    typedef std::map<std::string, shared_ptr<Read> > UnmatchedReadMap; // Read.name -> Read
    shared_ptr<UnmatchedReadMap> unmatched_reads;

    PairMatcher(const BAMFile& _bf, const Region& _region, PairProcessor& _processor)
    :   bf(_bf), region(_region), processor(_processor), unmatched_reads(new UnmatchedReadMap)
    {}

    virtual int process_read(shared_ptr<Read> read)
    {
        if (unmatched_reads->count(read->name))
        {
            // we found a match -- send to processor
            processor.process_pair((*unmatched_reads)[read->name], read);
            unmatched_reads->erase(read->name);
        }
        else
        {
            // haven't seen this read -- store it
            (*unmatched_reads)[read->name] = read;
        }

        return 0;
    }
};


} // namespace


namespace bampp {


void iterate_pairs(const BAMFile& bf, const Region& region, PairProcessor& processor)
{
    PairMatcher matcher(bf, region, processor);

    // process pairs first
    bf.process_reads(region, matcher);

    // send unmatched reads to processor
    for (PairMatcher::UnmatchedReadMap::const_iterator it=matcher.unmatched_reads->begin(); it!=matcher.unmatched_reads->end(); ++it)
        processor.process_pair(it->second, shared_ptr<Read>());
}


void clear_bam1_t(bam1_t& b)
{
    b.core.tid = b.core.pos = b.core.bin = b.core.qual = b.core.l_qname
        = b.core.flag = b.core.n_cigar = b.core.l_qseq = b.core.mtid = b.core.mpos = b.core.isize = 0;
    b.l_aux = b.data_len = b.m_data = 0;
    b.data = 0;
}


void iterate_all(const string& filename_bam, ReadProcessor& processor)
{
    shared_ptr<BGZF> bf = shared_ptr<BGZF>(bam_open(filename_bam.c_str(), "r"), bgzf_close);
    if (!bf.get()) throw runtime_error(("[BAMFile::iterate_simple()] Unable to open file " + filename_bam).c_str());

    shared_ptr<bam_header_t> header = shared_ptr<bam_header_t>(bam_header_read(bf.get()), bam_header_destroy);
    if (!header.get()) throw runtime_error("[BAMFile] Error reading header.");
    vector<string> reference_names;
    copy(header->target_name, header->target_name+header->n_targets, back_inserter(reference_names));

    while (true)
    {
        bam1_t b;
        clear_bam1_t(b); // ugly: segfault without this (!)
        int bytes_read = bam_read1(bf.get(), &b);
        if (bytes_read < 1) break; // apparently bam_read1() returns -1 at EOF
        shared_ptr<Read> read = translate(&b, reference_names);
        processor.process_read(read);        
    }
}


} // namespace bampp


