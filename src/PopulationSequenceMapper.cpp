//
// PopulationSequenceMapper.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "PopulationSequenceMapper.hpp"
#include <algorithm> // min


using namespace std;


PopulationSequenceMapper::PopulationSequenceMapper(const Population& population,
                                                   size_t chromosome_pair_index,
                                                   size_t ids_per_haplotype,
                                                   const HaplotypeReference& hapref)
:   population_(population),
    chromosome_pair_index_(chromosome_pair_index),
    ids_per_haplotype_(ids_per_haplotype),
    hapref_(hapref)
{
    if (ids_per_haplotype_ == 0)
        throw runtime_error("[PopulationSequenceMapper] Invalid ids_per_haplotype.");
}


string PopulationSequenceMapper::full_sequence(size_t haplotype_index, 
                                               size_t position_begin, 
                                               size_t position_end) const
{
    string sequence;

    if (haplotype_index >= 2 * population_.population_size())
        throw runtime_error("[PopulationSequenceMapper] Invalid haplotype index.");

    size_t individual_index = haplotype_index / 2;
    bool second = haplotype_index % 2;

    ChromosomePairRange range = population_.chromosome_pair_range(individual_index);

    if (chromosome_pair_index_ >= range.size())
        throw runtime_error("[PopulationSequenceMapper] Invalid chromosome pair index.");

    const ChromosomePair* chromosome_pair = range.begin() + chromosome_pair_index_;
    const Chromosome& chromosome = second ? chromosome_pair->second : chromosome_pair->first;

    if (chromosome.haplotype_chunks().empty())
        throw runtime_error("[PopulationSequenceMapper] No haplotype chunks.");

    HaplotypeChunks::const_iterator chunk = chromosome.find_haplotype_chunk(position_begin);
    HaplotypeChunks::const_iterator chunk_last = chromosome.haplotype_chunks().end() - 1;

    size_t position = position_begin;

    const bool use_ref_for_ambiguous = false;

    while (position < position_end)
    {
        size_t chunk_end = position_end;
        if (chunk != chunk_last)
            chunk_end = min((chunk+1)->position, static_cast<unsigned int>(position_end));
        unsigned int chunk_id = chunk->id / ids_per_haplotype_;
        string chunk_sequence = hapref_.full_sequence(chunk_id + 1, position, chunk_end, use_ref_for_ambiguous);
        sequence += chunk_sequence;
        position = chunk_end;
        ++chunk;
    }

    if (sequence.size() != position_end - position_begin)
        throw runtime_error("[PopulationSequenceMapper] Something's wrong: bad sequence size.");

    return sequence;
}


