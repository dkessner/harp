//
// BaseQualityInterpreterTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BaseQualityInterpreter.hpp"
#include "unit.hpp"
#include <iostream>
#include <iterator>


using namespace std;


void test_default_illumina()
{
    const double epsilon = 1e-12;

    BaseQualityInterpreter_Illumina bqi;

    unit_assert(!bqi.is_valid(char(-1)));
    unit_assert(!bqi.is_valid(char(42)));

    for (char q=0; q<=40; ++q)
    {
        unit_assert(bqi.is_valid(q));

        if (q > 3)
            unit_assert_equal(bqi.error_probability(q), pow(10, -q/10.), epsilon);
    }
}


void test_read_write()
{
    vector<double> p_error;
    for (size_t q=0; q<42; ++q)
        p_error.push_back(q/100.);
   
    BaseQualityInterpreter_Illumina bqi(p_error);
    BaseQualityInterpreter_Illumina bqi_new;
    
    ostringstream oss;
    oss << bqi;
    istringstream iss(oss.str());
    iss >> bqi_new;

    unit_assert(bqi.error_probability(2) == bqi_new.error_probability(2));
    for (char q=4; q<41; ++q)
        unit_assert(bqi.error_probability(q) == bqi_new.error_probability(q));
}


int main()
{
    try
    {
        test_default_illumina();
        test_read_write();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

