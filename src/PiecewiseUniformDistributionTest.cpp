//
// PiecewiseUniformDistributionTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "PiecewiseUniformDistribution.hpp"
#include "unit.hpp"
#include <iostream>
#include <iterator>
#include <numeric>


using namespace std;


void test()
{
    double x_values[] = {0, .25, .5, .75, 1.};
    vector<double> x(x_values, x_values+sizeof(x_values)/sizeof(double));

    double p_values[] = {0, .2, .3, .3, .2};
    vector<double> p(p_values, p_values+sizeof(x_values)/sizeof(double));

    PiecewiseUniformDistribution d(x, p);
    unit_assert(d.mean() == .5);


    const double epsilon = 1e-6;
    unit_assert_equal(d.probability(.125, .875), .8, epsilon);
    unit_assert_equal(d.probability(.375, 1.25), .65, epsilon);
    unit_assert_equal(d.probability(.125, .375), .25, epsilon);
    unit_assert_equal(d.probability(.25 + .25*2/3, .5 + .25*2/3), .3, epsilon);
    unit_assert_equal(d.probability(.125,.25), .1, epsilon);
    unit_assert_equal(d.probability(0,.25), .2, epsilon);
    unit_assert_equal(d.probability(.25,.75), .6, epsilon);
    unit_assert_equal(d.probability(.25,.5), .3, epsilon);
    unit_assert_equal(d.probability(.75,1), .2, epsilon);
    unit_assert_equal(d.probability(.625,2), .35, epsilon);
    unit_assert_equal(d.probability(.875,2), .1, epsilon);
    unit_assert_equal(d.probability(.5,.75), .3, epsilon);

    unit_assert_equal(d.probability(0,.125), .1, epsilon);
}


void test_shift()
{
    double x_values[] = {0, .25, .5, .75, 1.};
    vector<double> x(x_values, x_values+sizeof(x_values)/sizeof(double));

    double p_values[] = {0, .2, .3, .3, .2};
    vector<double> p(p_values, p_values+sizeof(x_values)/sizeof(double));

    PiecewiseUniformDistribution d(x, p);
    unit_assert(d.mean() == .5);

    const double epsilon = 1e-6;

    double alpha = 1.2;
    double mean_manual = (.2*.125 + .3*.375 + .3*.625) * alpha + .2 * (.75*alpha + 1.)/2;

    PiecewiseUniformDistribution d2(d, alpha);
    unit_assert_equal(d2.mean(), mean_manual, epsilon);

    // find_alpha

    double a = d.find_alpha(.6);
    PiecewiseUniformDistribution d_shifted(d, a);
    unit_assert_equal(d_shifted.mean(), .6, epsilon);

    // p_new

    vector<double> p_new = d.p_new(a);

    //cout << "p_new:\n";
    //copy(p_new.begin(), p_new.end(), ostream_iterator<double>(cout, " "));
    //cout << endl;

    PiecewiseUniformDistribution d_new(x, p_new);
    //cout << "d_new.mean: " << d_new.mean() << endl;

    // discovered this requires iteration as well
    //unit_assert_equal(d_new.mean(), .6, epsilon);

    vector<double> p3 = d_new.p_new(d_new.find_alpha(.6));
    PiecewiseUniformDistribution d3(x, p3);
    //cout << "d3.mean: " << d3.mean() << endl;

    vector<double> p4 = d3.p_new(d3.find_alpha(.6));
    PiecewiseUniformDistribution d4(x, p4);
    //cout << "d4.mean: " << d4.mean() << endl;


    // iterate_p_new

    vector<double> p_iter = d.iterate_p_new(.6);

    //cout << "p_iter:\n";
    //copy(p_iter.begin(), p_iter.end(), ostream_iterator<double>(cout, " "));
    //cout << endl;

    PiecewiseUniformDistribution d_iter(x, p_iter);
    //cout << "d_iter.mean: " << d_iter.mean() << endl;
    unit_assert_equal(d_iter.mean(), .6, epsilon);

    // iterate_p_new: .25

    p_iter = d.iterate_p_new(.25);
    d_iter = PiecewiseUniformDistribution(x, p_iter);
    unit_assert_equal(d_iter.mean(), .25, epsilon);

    // iterate_p_new: .8

    p_iter = d.iterate_p_new(.8);
    d_iter = PiecewiseUniformDistribution(x, p_iter);
    unit_assert_equal(d_iter.mean(), .8, epsilon);
}


int main()
{
    try
    {
        test();
        test_shift();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

