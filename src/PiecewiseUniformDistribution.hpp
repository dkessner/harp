//
// PiecewiseUniformDistribution.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _PIECEWISEUNIFORMDISTRIBUTION_HPP_
#define _PIECEWISEUNIFORMDISTRIBUTION_HPP_


#include <iostream>
#include <vector>


//
// distribution on [x_0, x_n):
//
// [x_0, x_1)     p_1
// [x_1, x_2)     p_2
// ...
// [x_n-1, x_n)   p_n
//
// notes:  
//   x is assumed to be sorted
//   p_0 == 0
//   x_0 == 0 may be assumed for some functions
//


class PiecewiseUniformDistribution
{
    public:

    PiecewiseUniformDistribution(const std::vector<double>& x, // sorted
                                 const std::vector<double>& p);

    // Construction from existing distribution, with internal shift of x by alpha:
    //     x_0 -> x_0
    //     x_i -> alpha * x_i    (for i<n)
    //     x_n -> x_n
    PiecewiseUniformDistribution(const PiecewiseUniformDistribution& that,
                                 double alpha);

    double probability(double a, double b) const; // returns P(X in [a,b])

    double mean() const;

    const std::vector<double>& x() const {return x_;}
    const std::vector<double>& p() const {return p_;}

    //
    // these functions assume x_0 == 0
    //

    // find internal shift alpha for desired target mean
    double find_alpha(double mean_target) const;

    // p_new == redistribution of p after internal shift by alpha, using original x grid
    std::vector<double> p_new(double alpha) const;

    // iterates find_alpha and p_new to reach target mean
    std::vector<double> iterate_p_new(double mean_target) const;


    private:

    std::vector<double> x_;
    std::vector<double> p_;
    std::size_t n_;
};


#endif // _PIECEWISEUNIFORMDISTRIBUTION_HPP_


