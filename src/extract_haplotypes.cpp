//
// extract_haplotypes.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeReference.hpp"
#include <iostream>


using namespace std;


int main(int argc, char* argv[])
{
    try
    {
        if (argc < 3)
        {
            cout << "extract_haplotypes\n";
            cout << "Extract haplotypes from DGRP SNP file.\n";
            cout << endl;
            cout << "Usage: extract_haplotypes snpfile region [refseq]\n";
            return 1;
        }

        const char* filename_snps = argv[1];
        Region region(argv[2]);
        string filename_refseq = argc>3 ? argv[3] : "";
        
        HaplotypeReference hapref(filename_refseq, filename_snps, region);

        HaplotypeReference::SNPTable::const_iterator snp_table_begin = hapref.snp_table.lower_bound(region.begin);
        HaplotypeReference::SNPTable::const_iterator snp_table_end = hapref.snp_table.lower_bound(region.end);

        vector<size_t> positions;
        for (HaplotypeReference::SNPTable::const_iterator it=snp_table_begin; it!=snp_table_end; ++it)
            positions.push_back(it->first);

        for (size_t i=0; i<hapref.haplotype_count(); ++i)
        {
            string sequence = !filename_refseq.empty() ? hapref.full_sequence(i, region.begin, region.end) 
                                              : hapref.variant_sequence(i, positions);

            cout << i << " " << hapref.haplotype_ids[i] << " " << sequence << endl;
        }
       
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

