//
// HaplotypeReference.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeReference.hpp"
#include "samtools/faidx.h"
#include "boost/tokenizer.hpp"
#include "boost/lambda/lambda.hpp"
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iterator>
#include <cstdlib>
#include <tr1/memory>


using namespace std;
using namespace boost::lambda;
using tr1::shared_ptr;
using boost::iostreams::stream_offset;
using boost::iostreams::position_to_offset;
using boost::iostreams::offset_to_position;


SNPTableIndexPtr create_snp_table_index(const string& filename_snps, size_t lines_per_entry)
{
    ifstream is(filename_snps.c_str());
    if (!is) throw runtime_error(("[HaplotypeReference] Unable to open file " + filename_snps).c_str());

    string buffer;
    getline(is, buffer);

    SNPTableIndexPtr index(new SNPTableIndex);

    while (is)
    {
        for (size_t i=0; i<lines_per_entry; i++)
            getline(is, buffer);
        if (!is) break;
        size_t next_position = atoi(buffer.c_str()); // 0-based position + 1 == 1-based position
        (*index)[next_position] = position_to_offset(is.tellg());; 
    }

    return index;
}


void write_snp_table_index(SNPTableIndexPtr index, const string& filename)
{
    ifstream is(filename.c_str());
    if (is)
        throw runtime_error(("Index file " + filename + " already exists.").c_str());

    ofstream os(filename.c_str());
    for (SNPTableIndex::const_iterator it=index->begin(); it!=index->end(); ++it)
        os << it->first << "\t" << it->second << endl;
    os.close();
}


SNPTableIndexPtr read_snp_table_index(const string& filename)
{
    SNPTableIndexPtr index(new SNPTableIndex);

    ifstream is(filename.c_str());
    if (!is) return SNPTableIndexPtr();

    string buffer;
    while (is)
    {
        getline(is, buffer);
        if (!is) break;
        istringstream iss(buffer);
        size_t position;
        stream_offset offset;
        iss >> position >> offset;
        (*index)[position] = offset;
    }
    
    return index;
}


namespace {


vector<string> tokenize_commas(const string& s)
{
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
    tokenizer tokens(s, sep);
    vector<string> result;
    copy(tokens.begin(), tokens.end(), back_inserter(result));
    return result;
}


void parse_snps(const string& filename_snps,
                const Region& region,
                vector<string>& haplotype_ids,
                map<size_t, vector<char> >& snp_table)
{
    // format: 
    //
    // 2L,Ref,21,26,28,38,Coverage,
    // 30012,G,G,N,G,G,8,
    //

    // read in header line and sanity check

    if (filename_snps.empty())
        return;

    ifstream is(filename_snps.c_str());
    if (!is) throw runtime_error(("[HaplotypeReference] Unable to open file " + filename_snps).c_str());

    string buffer;
    getline(is, buffer);
    vector<string> tokens = tokenize_commas(buffer);

    if (tokens.size() < 3 || tokens[1] != "Ref")
        throw runtime_error("[HaplotypeReference] Invalid SNP file: first SNP column must be Ref");
    
    if (tokens[0] != region.id)
    {
        ostringstream oss;
        oss << "[HaplotypeReference] SNP file id does not match region id: " << tokens[0] << "!=" << region.id;
        throw runtime_error(oss.str());
    }

    int extra_column_count = (tokens[tokens.size()-1] == "") ? 1 : 0; // handle trailing comma
    extra_column_count += (tokens[tokens.size() - 1 - extra_column_count] == "Coverage") ? 1 : 0; // handle "Coverage" column

    copy(tokens.begin()+1, tokens.end()-extra_column_count, back_inserter(haplotype_ids)); // note: include Ref for future sanity checks

    // use SNPTableIndex if it exists

    SNPTableIndexPtr index = read_snp_table_index(filename_snps + ".idx");
    if (!index.get())
    {
        cerr << "[HaplotypeReference] Warning: no SNP table index.\n";
    }
    else
    {
        SNPTableIndex::const_iterator it = index->lower_bound(region.begin);
        if (it != index->begin())
        {
            --it; // we want the previous offset
            is.seekg(offset_to_position(it->second));
        }
    }
    
    // read in the table

    while (is)
    {
        string buffer;
        getline(is, buffer);
        if (!is) break;

        size_t position = atoi(buffer.c_str()) - 1; // convert to 0-based position
        if (!region.contains(position)) continue;

        vector<string> tokens = tokenize_commas(buffer);
        if (tokens.size() != haplotype_ids.size() + 1 + extra_column_count)
        {
            cout << buffer << endl;
            throw runtime_error("[HaplotypeReference] Invalid line.");
        }
        
        // size_t coverage = atoi(tokens[tokens.size()-2].c_str()); // need flag for coverage if we want to do this
        // (void) coverage; // filter on this?

        vector<char> variants;
        transform(tokens.begin()+1, tokens.end()-extra_column_count, back_inserter(variants), _1[0]); // boost::lambda magic string->char
        snp_table[position] = variants;
    }
}


string get_refseq(const string& filename_refseq, const Region& region)
{
    if (filename_refseq.empty())
        return string();

    shared_ptr<faidx_t> faidx(fai_load(filename_refseq.c_str()), fai_destroy);
    if (!faidx.get())
        throw runtime_error("[HaplotypeReference] fai_load() returned null.");

    int length = 0;
    shared_ptr<char> seq(fai_fetch(faidx.get(), region.str().c_str(), &length), free);
    if (!seq.get())
        throw runtime_error("[HaplotypeReference] fai_fetch() returned null.");

    return string(seq.get(), length);

    // note: done automatically by our shared_ptrs
    // free(seq); 
    // fai_destroy(faidx);
}


} // namespace


void sanity_check(const HaplotypeReference& hapref)
{
    vector<size_t> positions;
    for (HaplotypeReference::SNPTable::const_iterator it=hapref.snp_table.begin(); it!=hapref.snp_table.end(); ++it)
        positions.push_back(it->first);

    string refseq_variants;
    for (size_t i=0; i<positions.size(); ++i)
    {
        size_t index = positions[i]-hapref.region.begin;
        if (index > hapref.refseq.size()) throw runtime_error("[HaplotypeReference::sanity_check()] Bad refseq index.");
        refseq_variants += hapref.refseq[index];
    }

    //cout << "[HaplotypeReference::sanity_check()]" << endl
    //     << "  refseq[variant_positions]: " << refseq_variants << endl
    //     << "  snp_table[*][Ref]: " << hapref.variant_sequence(0, positions) << endl;

    if (refseq_variants != hapref.variant_sequence(0, positions))
    {
        ostringstream oss;
        oss << "[HaplotypeReference] refseq != snp_table\n"
            << "  refseq[variant_positions]: " << refseq_variants << endl
            << "  snp_table[*][Ref]: " << hapref.variant_sequence(0, positions) << endl;
        throw runtime_error(oss.str().c_str());
    }
}


HaplotypeReference::HaplotypeReference(const string& filename_refseq, 
                                       const string& filename_snps, 
                                       const Region& _region)
:   region(_region)
{
    parse_snps(filename_snps, region, haplotype_ids, snp_table);
    refseq = get_refseq(filename_refseq, region);

    if (!filename_refseq.empty() && !filename_snps.empty())
        sanity_check(*this);
}
 

string HaplotypeReference::variant_sequence(size_t haplotype_index, vector<size_t> positions) const
{
    string result(positions.size(), '\0');
    vector<size_t>::const_iterator it = positions.begin();
    string::iterator jt = result.begin();
    try
    {
        for (; it!=positions.end(); ++it, ++jt)
            *jt = snp_table.at(*it).at(haplotype_index);
    }
    catch (exception& e)
    {
        ostringstream oss;
        oss << "[HaplotypeReference] Error accessing snp_table: " << *it << " " << haplotype_index << endl;
        throw runtime_error(oss.str().c_str());
    }
    return result;
}


namespace {
inline bool isACGT(char base)
{
    return (base=='A' || base=='C' || base=='G' || base=='T');
}
} // namespace


string HaplotypeReference::full_sequence(size_t haplotype_index, size_t position_begin, size_t position_end,
                                         bool use_ref_for_ambiguous) const
{
    if (position_begin < region.begin || position_end > region.end || position_end < position_begin)
    {
        cout << "position_begin: " << position_begin << endl;
        cout << "position_end: " << position_end << endl;
        throw runtime_error("[HaplotypeReference::full_sequence] Invalid position.");
    }

    string result = refseq.substr(position_begin - region.begin, position_end - position_begin); // reference

    SNPTable::const_iterator it = snp_table.lower_bound(position_begin);
    SNPTable::const_iterator end = snp_table.lower_bound(position_end);

    if (!use_ref_for_ambiguous)
    {
        for (; it!=end; ++it)
            result[it->first - position_begin] = it->second[haplotype_index]; // fix up SNP
    }
    else
    {
        for (; it!=end; ++it)
        {
            char new_base = it->second[haplotype_index];
            if (isACGT(new_base))
                result[it->first - position_begin] = new_base;
        }
    }

    return result;
}


void HaplotypeReference::append_recombined_haplotype(size_t haplotype_index_1, size_t haplotype_index_2, 
                                                     size_t recombination_position)
{
    if (haplotype_index_1 >= haplotype_count() || haplotype_index_1 >= haplotype_count())
        throw runtime_error("[HaplotypeReference::append_recombined_haplotype()] Bad index.");

    ostringstream oss;
    oss << "recombined_" << haplotype_ids[haplotype_index_1] << "_" << haplotype_ids[haplotype_index_2] << "_" << recombination_position;
    haplotype_ids.push_back(oss.str());    

    for (HaplotypeReference::SNPTable::iterator it=snp_table.begin(); it!=snp_table.end(); ++it)
    {
        if (it->first < recombination_position)
            it->second.push_back(it->second[haplotype_index_1]);
        else
            it->second.push_back(it->second[haplotype_index_2]);
    }
}


