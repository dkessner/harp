//
// harp_misc_test.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_misc.hpp"
#include "unit.hpp"
#include <iostream>


using namespace std;


void test_parse_haplotype_filter_string()
{
    vector<bool> a = harp_misc::parse_haplotype_filter_string(10, "1-2,9,7-8");
    unit_assert(a[1] && a[2] && a[9] && a[7] && a[8]);
    unit_assert(!a[0] && !a[3] && !a[4] && !a[5] && !a[6]);

    vector<bool> b = harp_misc::parse_haplotype_filter_string(10, "");
    unit_assert(b.size() == 10);
    for (vector<bool>::const_iterator it=b.begin(); it!=b.end(); ++it)
        unit_assert(*it == true);
}


void test_get_extended_region()
{
    Region r("goo", 100, 1000);

    Region e1 = harp_misc::get_extended_region(r, 50, 2000);
    unit_assert(e1.begin == 50);
    unit_assert(e1.end == 3000);

    Region e2 = harp_misc::get_extended_region(r, 150, 2000);
    unit_assert(e2.begin == 0);
    unit_assert(e2.end == 3000);
}


int main()
{
    try
    {
        test_parse_haplotype_filter_string();
        test_get_extended_region();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

