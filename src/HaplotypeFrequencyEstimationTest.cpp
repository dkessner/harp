//
// HaplotypeFrequencyEstimationTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeFrequencyEstimation.hpp"
#include "unit.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/numeric/ublas/vector.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "boost/random/exponential_distribution.hpp"
#include <iostream>
#include <fstream>
#include <iterator>
#include <numeric>


using namespace std;
namespace ublas = boost::numeric::ublas;
namespace bfs = boost::filesystem;


//
// test_files/cash_2L_15004001-15005000.hlk created with:
// harp like cash_2L_15002500-15006500.bam ~/data/refseq/dmel-all-chromosome-r5.34.fasta ~/data/dgrp_snps/Variants_Sparse_2L.sample_swap_fixed.txt 2L:15004001-15005000 out
//


void test()
{
    HaplotypeLikelihoodRecords records("test_files/cash_2L_15004001-15005000.hlk");

    //cout << "haplotype count: " << records.haplotype_count << endl;
    //cout << "records: " << records.size() << endl;
    unit_assert(records.size() == 684);

    HaplotypeFrequencyEstimation::Config config;
    config.max_iteration_count = 1;

    vector<double> f = HaplotypeFrequencyEstimation::estimate_haplotype_frequencies(records,
            15004000, 15005000, config);

    //cout << "63 " << f[63] << endl;
    //cout << "103 " << f[103] << endl;
    //cout << "147 " << f[147] << endl;
    //cout << "141 " << f[141] << endl;
    //cout << "123 " << f[123] << endl;
    //cout << "26 " << f[26] << endl;
    //cout << "102 " << f[102] << endl;

    const double epsilon = 1e-6;
    unit_assert_equal(f[63], 0.0446137, epsilon);
    unit_assert_equal(f[103], 0.0425507, epsilon);
    unit_assert_equal(f[147], 0.032258, epsilon);
    unit_assert_equal(f[141], 0.032258, epsilon);
    unit_assert_equal(f[123], 0.032258, epsilon);
    unit_assert_equal(f[ 26], 0.0315337, epsilon);
    unit_assert_equal(f[102], 0.0259642, epsilon);
}


void test_filter()
{
    HaplotypeLikelihoodRecords records("test_files/cash_2L_15004001-15005000.hlk");
    unit_assert(records.size() == 684);

    HaplotypeFrequencyEstimation::Config config;
    config.max_iteration_count = 1;
    config.haplotype_filter = "1-103,105-147,149-162"; // 0 == Ref

    vector<double> f = HaplotypeFrequencyEstimation::estimate_haplotype_frequencies(records,
            15004000, 15005000, config);

    //cout << "63 " << f[63] << endl;
    //cout << "102 " << f[102] << endl;
    //cout << "103 " << f[103] << endl;
    //cout << "147 " << f[147] << endl;
    //cout << "141 " << f[141] << endl;
    //cout << "123 " << f[123] << endl;
    //cout << "26 " << f[26] << endl;
    //cout << "102 " << f[102] << endl;

    double s = accumulate(f.begin(), f.end(), 0.);

    const double epsilon = 1e-6;
    unit_assert_equal(s, 1., epsilon);
    unit_assert_equal(f[63], 0.0640887, epsilon);
    unit_assert_equal(f[103], 0, epsilon);
    unit_assert_equal(f[147], 0, epsilon);
    unit_assert_equal(f[141], 0.0444445, epsilon);
    unit_assert_equal(f[123], 0.0444445, epsilon);
    unit_assert_equal(f[ 26], 0.0436587, epsilon);
    unit_assert_equal(f[102], 0.0355987, epsilon);
}


void test_EM()
{
    HaplotypeLikelihoodRecords records("test_files/cash_2L_15004001-15005000.hlk");
    //cout << "haplotype count: " << records.haplotype_count << endl;
    //cout << "records: " << records.size() << endl;
    unit_assert(records.size() == 684);

    bfs::create_directories("temp");
    ofstream os("temp/em.txt");

    HaplotypeFrequencyEstimation::Config config;
    config.max_iteration_count = 100;
    config.minimum_frequency_cutoff = .001;
    config.convergence_threshold = 1e-8;

    vector<double> f = HaplotypeFrequencyEstimation::estimate_haplotype_frequencies(records,
            15004000, 15005000, config, &os);
}


void test_ublas()
{
    ublas::vector<double> v(3);
    for (size_t i=0; i<3; ++i) v[i] = i; // (0,1,2)
    //cout << "norm_2(0,1,2) == " << norm_2(v) << endl;
    unit_assert(norm_2(v) == sqrt(5));
}


void test_exponential()
{
    boost::mt19937 gen;
    gen.seed(static_cast<unsigned int>(std::time(0)));
    boost::exponential_distribution<> dist(2);

    const size_t v_size = 1000;
    ublas::vector<double> v(v_size);
    for (size_t i=0; i<v_size; ++i) v[i] = dist(gen);

    copy(v.begin(), v.end(), ostream_iterator<double>(cout, " "));
    cout << "mean: " << sum(v)/v_size << endl;
}
 

void test_EM_random() // exploratory
{
    HaplotypeLikelihoodRecords records("test_files/cash_2L_15004001-15005000.hlk");
    //cout << "haplotype count: " << records.haplotype_count << endl;
    //cout << "records: " << records.size() << endl;
    unit_assert(records.size() == 684);

    bfs::path outdir("temp");
    bfs::create_directories(outdir);

    for (size_t i=0; i<10; ++i)
    {
        ostringstream oss_filename;
        oss_filename << "em_random_" << i << ".txt";
        bfs::ofstream os(outdir / oss_filename.str());

        HaplotypeFrequencyEstimation::Config config;
        config.max_iteration_count = 800;
        //config.random_start = true; 

        vector<double> f = HaplotypeFrequencyEstimation::estimate_haplotype_frequencies(records,
                15004000, 15005000, config, &os);

        copy(f.begin(), f.end(), ostream_iterator<double>(cout, "\t"));
        cout << endl;
    }
}


int main()
{
    try
    {
        test();
        test_filter();
        test_EM();
        test_ublas();
        //test_exponential();
        //test_EM_random();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

