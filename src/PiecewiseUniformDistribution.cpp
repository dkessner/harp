//
// PiecewiseUniformDistribution.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "PiecewiseUniformDistribution.hpp"
#include "boost/lambda/lambda.hpp"
#include <iostream>
#include <stdexcept>
#include <numeric>
#include <algorithm>
#include <cmath>


using namespace std;
using namespace boost::lambda; // _1


PiecewiseUniformDistribution::PiecewiseUniformDistribution(const vector<double>& x,
                                                           const vector<double>& p)
:   x_(x), p_(p), n_(x.size()-1)
{
    if (x.size() < 2)
        throw runtime_error("[PiecewiseUniformDistribution] x.size() < 2");

    if (x.size() != p.size())
        throw runtime_error("[PiecewiseUniformDistribution] Unequal vector sizes");

    if (!p.empty() && p[0]!=0.)
        throw runtime_error("[PiecewiseUniformDistribution] p[0] != 0");

    if (fabs(accumulate(p.begin(), p.end(), 0.) - 1.) > 1e-10)
        throw runtime_error("[PiecewiseUniformDistribution] sum(p) != 1");
    
    for (vector<double>::const_iterator it=x.begin()+1; it!=x.end(); ++it)
        if (*it <= *(it-1))
            throw runtime_error("[PiecewiseUniformDistribution] x not sorted");
}


PiecewiseUniformDistribution::PiecewiseUniformDistribution(const PiecewiseUniformDistribution& that,
                                                           double alpha)
:   x_(that.x_), p_(that.p_), n_(that.n_)
{
    if (alpha <= 0 || alpha * x_[n_-1] >= x_[n_])
        throw runtime_error("[PiecewiseUniformDistribution] Illegal shift");

    for_each(x_.begin()+1, x_.end()-1, _1 *= alpha);
}


double PiecewiseUniformDistribution::probability(double a, double b) const
{
    if (a > b)
        throw runtime_error("[PiecewiseUniformDistribution::probability()] a > b");

    if (a < x_[0]) a = x_[0];
    if (b > x_[n_]) b = x_[n_];

    vector<double>::const_iterator it_a = upper_bound(x_.begin(), x_.end(), a);
    vector<double>::const_iterator it_b = lower_bound(x_.begin(), x_.end(), b);

    if (it_a <= x_.begin())
        throw runtime_error("[PiecewiseUniformDistribution::probability()] I am insane!");

    if (it_b >= x_.end())
        throw runtime_error("[PiecewiseUniformDistribution::probability()] I am insane!");

    size_t index_a = it_a - x_.begin();
    size_t index_b = it_b - x_.begin();

    //cout << "[a,b]: " << a << " " << b << endl;
    //cout << "(index_a, index_b): " << index_a << " " << index_b << endl;

    double left = 0;
    double right = 0;

    if (index_b > index_a)
    {
        left = p_[index_a] * (*it_a - a) / (*it_a - *(it_a-1));
        right = p_[index_b] * (b - *(it_b-1)) / (*it_b - *(it_b-1));
    }
    else
    {
        left = p_[index_a] * (b - a) / (*it_a - *(it_a-1));
    }

    //cout << "left: " << left << endl;
    //cout << "right: " << right << endl;

    double internal = 0.;
    if (index_a+1 < index_b)
        internal = accumulate(p_.begin() + index_a + 1, p_.begin() + index_b, 0.);

    //cout << "internal: " << internal << endl;
    //cout << endl;

    return left + internal + right;
}


double PiecewiseUniformDistribution::mean() const
{
    double sum = 0.;
    for (size_t i=1; i<=n_; ++i)
        sum += p_[i] * (x_[i] + x_[i-1])/2;
    return sum;
}


double PiecewiseUniformDistribution::find_alpha(double mean_target) const
{
    double alpha = mean_target / mean();
    double epsilon = 1e-12;
    const size_t max_iteration_count = 20;

    for (size_t i=0; i<max_iteration_count; ++i)
    {
        PiecewiseUniformDistribution temp(*this, alpha);
        double mean_current = temp.mean();
        if (fabs(mean_current - mean_target) < epsilon) break;
        alpha *= mean_target / mean_current;
    }

    return alpha;
}


vector<double> PiecewiseUniformDistribution::p_new(double alpha) const
{
    vector<double> result(n_ + 1);

    PiecewiseUniformDistribution shifted(*this, alpha);

    for (size_t i=1; i<=n_; ++i)
        result[i] = shifted.probability(x_[i-1], x_[i]);

    return result;
}


vector<double> PiecewiseUniformDistribution::iterate_p_new(double mean_target) const
{
    double epsilon = 1e-8;
    const size_t max_iteration_count = 60;

    PiecewiseUniformDistribution d(x_, p_);

    for (size_t i=0; i<max_iteration_count; ++i)
    {
        double a = 0;

        try
        {
            a = d.find_alpha(mean_target);
        }
        catch (runtime_error& e)
        {
            // hack: this happens when mean_target/mean is too large (> x_n/x_n-1)
            // in this case, choose alpha as large as possible, and continue iteration
            a = x_[n_]/x_[n_-1] - epsilon;
        }

        vector<double> p = d.p_new(a);
        d = PiecewiseUniformDistribution(x_, p); 
        if (fabs(d.mean() - mean_target) < epsilon) break;
    }

    return d.p_;
}


