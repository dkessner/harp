//
// dgrp_to_poolhap.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeReference.hpp"
#include <iostream>
#include <cstring>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;


char unambiguous(char base, char ref)
{
    if (base=='A' || base=='C' || base=='G' || base=='T')
        return base;
    else 
        return ref;
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc < 3)
        {
            cout << "Usage: dgrp_to_poolhap filename_dgrp region [unambiguous]\n";
            return 1;
        }

        const char* filename_dgrp = argv[1];
        Region region(argv[2]);
        bool force_unambiguous = (argc>3 && !strcmp(argv[3], "unambiguous"));

        shared_ptr<HaplotypeReference> hapref(new HaplotypeReference("", filename_dgrp, region));

        // first line: snp ids
        for (HaplotypeReference::SNPTable::const_iterator it=hapref->snp_table.begin(); it!=hapref->snp_table.end(); ++it)
            cout << region.id << ":" << it->first+1 << " "; // 1-based position
        cout << endl;

        // line for each haplotype
        for (size_t h=0; h<hapref->haplotype_count(); ++h) 
        {
            for (HaplotypeReference::SNPTable::const_iterator it=hapref->snp_table.begin(); it!=hapref->snp_table.end(); ++it)
            {
                if (force_unambiguous)
                    cout << unambiguous(it->second[h], it->second[0]) << " ";
                else
                    cout << it->second[h] << " ";
            }
            cout << endl;
        }
       
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

