//
// harp_likedump.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "HaplotypeLikelihoodRecord.hpp"
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <iterator>
#include <cstring>


using namespace std;


namespace {


string positions_string(const HaplotypeLikelihoodRecord& r)
{
    ostringstream oss;
    oss << "[" << r.read1_begin << "," << r.read1_end << ") "
        << "[" << r.read2_begin << "," << r.read2_end << ")";
    return oss.str();
}


int harp_likedump(const harp_config::Config& config)
{
    if (config.filename_hlk.empty())
        throw runtime_error("[harp_likedump] No haplotype likelihood filename (.hlk) specified.");

    HaplotypeLikelihoodRecords records(config.filename_hlk);

    cout << "# haplotypes: " << records.haplotype_count << endl;
    cout << "# records: " << records.size() << endl;

    if (!records.empty())
    {
        cout << "# first: " << positions_string(*records.front()) << endl;
        cout << "# last: " << positions_string(*records.back()) << endl;
    }

    if (config.verbose)
    {
        for (HaplotypeLikelihoodRecords::const_iterator it=records.begin(); it!=records.end(); ++it)
            cout << **it << endl;
    }

    return 0;
}


string harp_likedump_info()
{
    return "print info from likelihood (.hlk) file";
}


string harp_likedump_usage()
{
    ostringstream oss;
    oss << "harp likedump required parameters:\n"
        << "  hlk\n"
        << endl
        << "harp likedump optional parameters:\n"
        << "  verbose\n";
    return oss.str();
}


} // namespace


harp_config::JumpTableEntry entry_harp_likedump_(harp_likedump, harp_likedump_info, harp_likedump_usage);


