//
// harp_freq_stringmatch.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "harp_misc.hpp"
#include "BAMFile.hpp"
#include "HaplotypeReference.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/numeric/ublas/vector.hpp"
#include <iostream>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
using namespace bampp;
namespace ublas = boost::numeric::ublas;
namespace bfs = boost::filesystem;


namespace {


class PairStringMatchProcessor : public bampp::PairProcessor
{
    public:

    PairStringMatchProcessor(const harp_config::Config& config,
                             shared_ptr<HaplotypeReference> hapref,
                             const vector<bool>& haplotype_filter,
                             const bfs::path& outdir)
    :   mismatch_threshold_(config.max_mismatches),
        region_(config.region),
        hapref_(hapref),
        haplotype_filter_(haplotype_filter),
        outdir_(outdir),
        count_pairs_(0), count_unpaired_(0), 
        sum_assignments_(hapref_->haplotype_count()-1, 0.) // no 0/Ref
    {}

    virtual void process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2);

    void report_summary(ostream& os) const;
    ublas::vector<double> haplotype_frequency_estimate() const {return sum_assignments_/count_pairs_;}

    private:

    unsigned int mismatch_threshold_;

    Region region_; // copy
    shared_ptr<HaplotypeReference> hapref_;
    vector<bool> haplotype_filter_; // copy
    bfs::path outdir_;

    // counts

    size_t count_pairs_;
    size_t count_unpaired_;
    ublas::vector<double> sum_assignments_;

    void count_mismatches(const Read& read, vector<size_t>& result) const;
};


void PairStringMatchProcessor::report_summary(ostream& os) const
{
    os << "reads: " << count_pairs_*2 + count_unpaired_ << endl
        << "pairs: " << count_pairs_ << endl
        << "unpaired: " << count_unpaired_ << endl;
}


void PairStringMatchProcessor::count_mismatches(const Read& read, vector<size_t>& result) const
{
    if (result.size() != hapref_->haplotype_count()) 
        throw runtime_error("[harp_freq_stringmatch::count_mismatches] This isn't happening.");

    Region read_region(hapref_->region.id, read.position, read.position+read.sequence.size());

    HaplotypeReference::SNPTable::const_iterator read_begin = hapref_->snp_table.lower_bound(read_region.begin);
    HaplotypeReference::SNPTable::const_iterator read_end = hapref_->snp_table.lower_bound(read_region.end);
    for (HaplotypeReference::SNPTable::const_iterator it=read_begin; it!=read_end; ++it)
    {
        size_t read_index = it->first - read_region.begin;

        for (size_t h=0; h<result.size(); ++h)
            if (haplotype_filter_[h] && read.sequence[read_index] != it->second[h])
                ++result[h];
    }
}


void PairStringMatchProcessor::process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2)
{
    if (!r1.get()) throw runtime_error("[harp_freq_stringmatch] Inconceivable: r1 is null.");

    if (!r2.get()) 
    {
        ++count_unpaired_;
        return;
    }

    vector<size_t> mismatch_counts(hapref_->haplotype_count()); // includes 0/Ref
    count_mismatches(*r1, mismatch_counts);
    count_mismatches(*r2, mismatch_counts);

    ublas::vector<double> assignment(mismatch_counts.size()-1); // remove 0/Ref
    for (size_t h=1; h<mismatch_counts.size(); ++h)
        assignment[h-1] = (haplotype_filter_[h] && mismatch_counts[h] <= mismatch_threshold_) ? 1 : 0;

    double sum_assignment = ublas::sum(assignment);
    if (sum_assignment > 0)
    {
        sum_assignments_ += assignment/sum_assignment;
        ++count_pairs_; 
    }
}


int go(const harp_config::Config& config)
{
    bfs::path outdir = bfs::path(config.dirname_output) / "freq_stringmatch";
    bfs::create_directories(outdir);

    bfs::ofstream os_config(outdir / "config.txt");
    os_config << config;
    os_config.close();

    // We use an extended region for the reference sequence (to include reads that extend outside of it)
    // and for the bam read requests (to include the 2nd read from read pairs that begin in the region).
    // However, we use the exact region for filtering the read pairs.

    Region extended_region = harp_misc::get_extended_region(config.region, 200, 2000);

    // allocate objects and process reads

    shared_ptr<HaplotypeReference> hapref(
        new HaplotypeReference(config.filename_refseq,
                               config.filename_snps,
                               extended_region));

    vector<bool> haplotype_filter = 
        harp_misc::parse_haplotype_filter_string(hapref->haplotype_count(), // includes 0/Ref
                                                 config.freq_config.haplotype_filter);
    
    PairStringMatchProcessor processor(config, hapref, haplotype_filter, outdir);

    BAMFile bamfile(config.filename_bam);
    bampp::iterate_pairs(bamfile, extended_region, processor);

    // write summary

    boost::filesystem::ofstream os_summary(outdir / "summary.txt");
    processor.report_summary(os_summary);
    os_summary.close();

    // write haplotype frequency estimate

    ublas::vector<double> hapfreqs = processor.haplotype_frequency_estimate();

    bfs::ofstream os_hapfreqs(config.filename_freqs);
    os_hapfreqs << config.region.id << " " << config.region.begin << " " << config.region.end << " ";
    copy(hapfreqs.begin(), hapfreqs.end(), ostream_iterator<double>(os_hapfreqs, " "));
    os_hapfreqs << endl;

    return 0;
}


int harp_freq_stringmatch(const harp_config::Config& config)
{
    if (config.filename_bam.empty())
        throw runtime_error("[harp_freq_stringmatch] No BAM file specified.");

    if (config.region.id.empty())
        throw runtime_error("[harp_freq_stringmatch] No region specified.");

    if (config.filename_refseq.empty())
        throw runtime_error("[harp_freq_stringmatch] No refseq specified.");

    if (config.filename_snps.empty())
        throw runtime_error("[harp_freq_stringmatch] No SNP file specified.");

    return go(config);
}


string harp_freq_stringmatch_info()
{
    return "estimate haplotype frequencies using simple string matching";
}


string harp_freq_stringmatch_usage()
{
    ostringstream oss;
    oss << "harp freq_stringmatch required parameters:\n"
        << "  bam\n"
        << "  region\n"
        << "  refseq\n"
        << "  snps\n"
        << endl
        << "harp freq_stringmatch optional parameters:\n"
        << "  max_mismatch\n";
    return oss.str();
}


} // namespace


harp_config::JumpTableEntry entry_harp_freq_stringmatch_(harp_freq_stringmatch, harp_freq_stringmatch_info, harp_freq_stringmatch_usage);


