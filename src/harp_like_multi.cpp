//
// harp_like_multi.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "harp_misc.hpp"
#include "HaplotypeReferenceMulti.hpp"
#include "HaplotypeLikelihoodRecord.hpp"
#include "BAMFile.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/lambda/lambda.hpp"
#include <iostream>
#include <iterator>
#include <cmath>
#include <numeric>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
using bampp::Read;
namespace bfs = boost::filesystem;
//using boost::lambda::_1; // causes internal compiler error with Apple g++ 4.2.1 (build 5664)
using namespace boost::lambda;


namespace {


typedef map< string, HaplotypeLikelihoodRecordPtr > HaplotypeLikelihoodMap;


class HaplotypeLikelihoodProcessor : public bampp::ReadProcessor
{
    public:

    HaplotypeLikelihoodProcessor(size_t haplotype_index, size_t haplotype_count,
                                 const HaplotypeReferenceMulti& haprefmulti, 
                                 HaplotypeLikelihoodMap& haplikemap)
    :   haplotype_index_(haplotype_index), haplotype_count_(haplotype_count),
        haplotype_refseq_length_(haprefmulti.sequence_length(haplotype_index)),
        haprefmulti_(haprefmulti), haplikemap_(haplikemap)
    {}

    virtual int process_read(shared_ptr<Read> read);

    private:

    size_t haplotype_index_;
    size_t haplotype_count_;
    size_t haplotype_refseq_length_;
    const HaplotypeReferenceMulti& haprefmulti_;
    HaplotypeLikelihoodMap& haplikemap_;

    bool passes_filter(const Read& r) const;
};


double compute_log_likelihood(string read_sequence, string read_quality, string ref_sequence)
{
    if (read_sequence.size() != read_quality.size() ||
        read_sequence.size() != ref_sequence.size())
        throw runtime_error("[harp_like_multi::compute_log_likelihood()] Sequence lengths do not match.");

    const size_t length = read_sequence.size();

    double result = 0;

    for (size_t i=0; i<length; ++i)
    {
        double p_error = bampp::illumina_error_probability(read_quality[i]);
        double logp = (read_sequence[i] == ref_sequence[i]) ? log(1-p_error) : log(p_error/3);
        result += logp;
    }

    return result;
}


int HaplotypeLikelihoodProcessor::process_read(shared_ptr<Read> read)
{
    if (!passes_filter(*read)) return 0;

    size_t read_begin = read->position;
    size_t read_end = read->position + read->sequence.size();

    // create a new record if necessary

    if (!haplikemap_.count(read->name))
    {
        HaplotypeLikelihoodRecordPtr record(new HaplotypeLikelihoodRecord);
        haplikemap_[read->name] = record;
        const double logp_small = -100; // note: don't make this so small that exp(logp_small) == 0
        record->read1_logl.resize(haplotype_count_ + 1, logp_small); // 0 = Ref
        record->read2_logl.resize(haplotype_count_ + 1, logp_small); // 0 = Ref
        record->read1_begin = read_begin;
        record->read1_end = read_end;
    }

    //string quality(read->quality.size(), '\0');
    //transform(read->quality.begin(), read->quality.end(), quality.begin(), _1 - 33);
    // quality = bampp::quality_to_illumina(read->quality);

    // compute log likelihood

    double log_likelihood = compute_log_likelihood(read->sequence, read->quality,
        haprefmulti_.full_sequence(haplotype_index_, read_begin, read_end));

    // enter log likelihood into map -- note +1 indexing, due to 0 == Ref

    haplikemap_[read->name]->read1_logl[haplotype_index_ + 1] = log_likelihood;

    return 0;
}


bool HaplotypeLikelihoodProcessor::passes_filter(const Read& r) const
{
    const int min_mapping_quality = 15;
    if (r.mapping_quality < min_mapping_quality) 
        return false;

    if (r.cigar.size() != 1 || 
        r.cigar[0].op!='M' || 
        r.cigar[0].count != r.sequence.size())
        return false;

    if (r.position + r.sequence.size() > haplotype_refseq_length_)
        return false;

    return true;
}


int go(const harp_config::Config& config)
{
    vector<string> bam_filenames;
    ifstream is(config.filename_bamlist.c_str());
    if (!is) throw runtime_error(("[harp_like_multi] Unable to open BAM list " + config.filename_bamlist).c_str());
    copy(istream_iterator<string>(is), istream_iterator<string>(), back_inserter(bam_filenames));

    shared_ptr<HaplotypeReferenceMulti> haprefmulti;

    if (!config.filename_refseq.empty())
    {
        haprefmulti = shared_ptr<HaplotypeReferenceMulti>(new HaplotypeReferenceMulti(config.filename_refseq));
    }
    else
    {
        ifstream is(config.filename_refseqlist.c_str());
        if (!is)
            throw runtime_error(("[harp_like_multi] Unable to open file " + config.filename_refseqlist).c_str());

        vector<string> filenames_fasta;
        copy(istream_iterator<string>(is), istream_iterator<string>(), back_inserter(filenames_fasta));
        haprefmulti = shared_ptr<HaplotypeReferenceMulti>(new HaplotypeReferenceMulti(filenames_fasta));
    }

    if (bam_filenames.size() != haprefmulti->haplotype_count())
        throw runtime_error("[harp_like_multi] refseq count != BAM file count");

    size_t haplotype_count = haprefmulti->haplotype_count();
    HaplotypeLikelihoodMap haplikemap;

    for (size_t haplotype_index=0; haplotype_index<haplotype_count; ++haplotype_index)
    {
        cout << "haplotype_index: " << haplotype_index << endl;

        HaplotypeLikelihoodProcessor processor(haplotype_index, haplotype_count, *haprefmulti, haplikemap); 
        bampp::iterate_all(bam_filenames[haplotype_index], processor);
    }

    // copy from map to records

    const double logl_mean = config.logl_mean;
    const double logl_sd = config.logl_sd;

    HaplotypeLikelihoodRecords records(haplotype_count + 1); // hack: 0 == Ref
    for (HaplotypeLikelihoodMap::const_iterator it=haplikemap.begin(); it!=haplikemap.end(); ++it)
    {
        HaplotypeLikelihoodRecordPtr record = it->second;

        // haplotype likelihood filter

        const vector<double>& logls = record->read1_logl;
        if (logls.empty()) throw runtime_error("[harp_like_multi] I am insane: logls.empty()");
        double max_logl = *max_element(logls.begin(), logls.end());
        if ((max_logl-logl_mean)/logl_sd < config.logl_min_zscore) 
            continue;

        records.push_back(record);
    }

    sort(records.begin(), records.end(), HaplotypeLikelihoodRecord_Read1Begin());

    // write records to hlk file

    bfs::ofstream os_records(config.filename_hlk);
    records.write(os_records);
    os_records.close();

    return 0;
}


int harp_like_multi(const harp_config::Config& config)
{
    if (config.filename_refseq.empty() && config.filename_refseqlist.empty())
        throw runtime_error("[harp_like] No refseq or refseqlist specified.");

    if (!config.filename_refseq.empty() && !config.filename_refseqlist.empty())
        throw runtime_error("[harp_like] Both refseq and refseqlist specified -- not sure what to do.");

    if (config.filename_bamlist.empty())
        throw runtime_error("[harp_like] No BAM list specified.");

    if (config.filename_hlk.empty())
        throw runtime_error("[harp_like] This isn't happening -- filename_hlk empty.");

    return go(config);
}


string harp_like_multi_info()
{
    return "calculate haplotype likelihoods from multilple refseqs/alignments (create .hlk file)";
}


string harp_like_multi_usage()
{
    ostringstream oss;
    oss << "harp like required parameters:\n";
    oss << "  refseq or refseqlist\n";
    oss << "  bamlist\n";
    oss << "  hlk (or stem)\n";
    return oss.str();
}


} // namespace


harp_config::JumpTableEntry entry_harp_like_multi_(harp_like_multi, harp_like_multi_info, harp_like_multi_usage);



