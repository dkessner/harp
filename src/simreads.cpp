//
// simreads.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "Region.hpp"
#include "HaplotypeReference.hpp"
#include "BAMFile.hpp" // namespace bampp
#include "BaseQualityGenerator.hpp"
#include "BaseQualityInterpreter.hpp"
#include "harp_misc.hpp"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/poisson_distribution.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <tr1/memory>


using namespace std;
using std::tr1::shared_ptr;
using bampp::Read;
namespace bfs = boost::filesystem;
using namespace boost::lambda; // for _1
namespace ublas = boost::numeric::ublas;
                

const char* sam_header_ = // Drosophila refseq info
    "@SQ	SN:YHet	LN:347038\n"
    "@SQ	SN:dmel_mitochondrion_genome	LN:19517\n"
    "@SQ	SN:2L	LN:23011544\n"
    "@SQ	SN:X	LN:22422827\n"
    "@SQ	SN:3L	LN:24543557\n"
    "@SQ	SN:4	LN:1351857\n"
    "@SQ	SN:2R	LN:21146708\n"
    "@SQ	SN:3R	LN:27905053\n"
    "@SQ	SN:Uextra	LN:29004656\n"
    "@SQ	SN:2RHet	LN:3288761\n"
    "@SQ	SN:2LHet	LN:368872\n"
    "@SQ	SN:3LHet	LN:2555491\n"
    "@SQ	SN:3RHet	LN:2517507\n"
    "@SQ	SN:U	LN:10049037\n"
    "@SQ	SN:XHet	LN:204112\n"
    "@PG	ID:simreads PN:simreads VN:1.0\n";


struct RecombinedHaplotypeFrequency
{
    size_t haplotype_index_1;
    size_t haplotype_index_2;
    size_t position;
    double frequency;

    RecombinedHaplotypeFrequency(const string& s) // s == "(i,j,pos,freq)"
    :   haplotype_index_1(0), haplotype_index_2(0), position(0), frequency(0)
    {
        char open='\0', comma1='\0', comma2='\0', comma3='\0', close='\0';
        istringstream iss(s);
        iss >> open >> haplotype_index_1 >> comma1 >> haplotype_index_2 
            >> comma2 >> position >> comma3 >> frequency >> close;
        if (open != '(' || comma1 != ',' || comma2 != ',' || comma3 != ',' || close != ')')
            throw runtime_error("[RecombinedHaplotypeFrequency] Bad format.");
    }
};


ostream& operator<<(ostream& os, const RecombinedHaplotypeFrequency& rhf)
{
    os << "(" << rhf.haplotype_index_1 << "," << rhf.haplotype_index_2 
       << "," << rhf.position << "," << rhf.frequency << ")";
    return os;
}


class ReadSimulator
{
    public:

    struct Config
    {
        string filename_refseq;
        string filename_snps;
        string filename_stem;
        string filename_bqi;
        Region region;
        vector<double> haplotype_frequencies;
        vector<RecombinedHaplotypeFrequency> recombined_haplotype_frequencies;
        double coverage;
        double error_rate;
        size_t read_length;
        size_t mean_pair_distance; // pair_distance = read2.position - read1.position
        size_t max_pair_distance;
        unsigned int seed; 

        Config(const string& filename = "");
        static void print_sample_config();
        void parse_config_file(const string& filename);
        void validate() const;
    }; // Config


    ReadSimulator(const Config& config);

    void simulate_reads() const;


    private:

    Config config_;
    shared_ptr<HaplotypeReference> hapref_;
    shared_ptr<BaseQualityGenerator> base_quality_generator_;
    shared_ptr<BaseQualityInterpreter_Illumina> bqi_;

    typedef map< pair<size_t,size_t>, vector<double> > AlleleFrequencyMap;
    AlleleFrequencyMap allele_frequency_map_;

    mutable boost::mt19937 gen_;
    boost::random::discrete_distribution<> hapfreq_distribution_;
    boost::random::uniform_int_distribution<> position_distribution_;
    boost::random::poisson_distribution<> pair_distance_distribution_;

    mutable size_t id_count_;
    mutable vector<size_t> haplotype_counts_;

    void initialize_allele_frequency_map();

    size_t random_haplotype_index() const {return hapfreq_distribution_(gen_);}
    size_t random_position() const {return position_distribution_(gen_);}
    size_t random_pair_distance() const {return min(config_.max_pair_distance,
        (size_t)max(pair_distance_distribution_(gen_), (int)config_.read_length));}
    char random_base(const vector<double>& dist) const;

    string add_errors(const string& sequence, const string& illumina_quality,
                      size_t position, size_t haplotype) const;
    pair<Read,Read> random_read_pair() const;
};


//
// ReadSimulator::Config
//


ReadSimulator::Config::Config(const string& filename)
:   coverage(0), error_rate(0), read_length(100), 
    mean_pair_distance(140), max_pair_distance(300),
    seed(static_cast<unsigned int>(std::time(0)))
{
    if (!filename.empty())
        parse_config_file(filename);
}


void ReadSimulator::Config::print_sample_config()
{
    string home = getenv("HOME");
    Config config;
    config.filename_refseq = home + "/data/refseq/dmel-all-chromosome-r5.34.fasta";
    config.filename_snps = home + "/data/dgrp_snps/Variants_Sparse_2L.sample_swap_fixed.txt";
    config.filename_stem = "reads";
    config.region = Region("2L:15000001-15010000");
    config.haplotype_frequencies.resize(4);
    for (int i=0; i<4; i++) config.haplotype_frequencies[i] = .25; 
    config.coverage = 200;
    config.error_rate = 0.0;

    ostream& operator<<(ostream& os, const ReadSimulator::Config& config); // forward declaration
    cout << config;
}


void ReadSimulator::Config::parse_config_file(const string& filename)
{
    ifstream is(filename.c_str());
    if (!is) 
        throw runtime_error(("[ReadSimulator::Config::parse_config_file] Unable to open file " + filename).c_str());

    for (string line; getline(is, line);)
    {
        if (line.empty() || line[0] == '#') continue;

        istringstream iss(line);
        string name;
        iss >> name;

        if (name == "filename_refseq")
            iss >> filename_refseq;
        else if (name == "filename_snps")
            iss >> filename_snps;
        else if (name == "filename_stem")
            iss >> filename_stem;
        else if (name == "filename_bqi")
            iss >> filename_bqi;
        else if (name == "region")
        {
            string value;
            iss >> value;
            region = Region(value);
        }
        else if (name == "haplotype_frequencies")
            copy(istream_iterator<double>(iss), istream_iterator<double>(), back_inserter(haplotype_frequencies));
        else if (name == "recombined_haplotype_frequencies")
            copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(recombined_haplotype_frequencies)); // automatic conversion from string via constructor
        else if (name == "coverage")
            iss >> coverage;
        else if (name == "error_rate")
            iss >> error_rate;
        else if (name == "read_length")
            iss >> read_length;
        else if (name == "seed")
            iss >> seed;
    }
}


void ReadSimulator::Config::validate() const
{
    if (filename_refseq.empty()) throw runtime_error("[simreads::Config] missing filename_refseq");
    if (filename_snps.empty()) throw runtime_error("[simreads::Config] missing filename_snps");
    if (filename_stem.empty()) throw runtime_error("[simreads::Config] missing filename_stem");
    if (region.id.empty() || region.end==0) throw runtime_error("[simreads::Config] missing region");
    if (haplotype_frequencies.empty()) throw runtime_error("[simreads::Config] missing haplotype_frequencies");
    if (coverage == 0) throw runtime_error("[simreads::Config] missing coverage");
}


ostream& operator<<(ostream& os, const ReadSimulator::Config& config)
{
    os << "filename_refseq " << config.filename_refseq << endl;
    os << "filename_snps " << config.filename_snps << endl;
    os << "filename_stem " << config.filename_stem << endl;
    os << "region " << config.region << endl;

    os << "haplotype_frequencies ";
    copy(config.haplotype_frequencies.begin(), config.haplotype_frequencies.end(), ostream_iterator<double>(os, " "));
    os << endl;

    os << "recombined_haplotype_frequencies ";
    copy(config.recombined_haplotype_frequencies.begin(), config.recombined_haplotype_frequencies.end(),
         ostream_iterator<RecombinedHaplotypeFrequency>(os, " "));
    os << endl;

    os << "coverage " << config.coverage << endl;
    os << "error_rate " << config.error_rate << endl;
    os << "read_length " << config.read_length << endl;

    return os;
}


//
// ReadSimulator
//


char random_different_base(char notit)
{
    char result = notit;
    while (result == notit)
    {
        double d = rand()/double(RAND_MAX);
        if (d < .25) result = 'A';
        else if (d < .5) result = 'C';
        else if (d < .75) result = 'G';
        else result = 'T';
    }
    return result;
}


char opposite_2base_code(char code)
{
    switch(code)
    {
        case 'R': return 'Y';
        case 'Y': return 'R';
        case 'K': return 'M';
        case 'M': return 'K';
        case 'S': return 'W';
        case 'W': return 'S';
        default: throw runtime_error("[opposite_2base_code()] Bad 2-base code.");
    }
}


inline bool isACGT(char base)
{
    return (base=='A' || base=='C' || base=='G' || base=='T');
}


pair<char,char> translate_2base_code(char code)
{
    switch(code)
    {
        case 'R': return make_pair('G', 'A');
        case 'Y': return make_pair('T', 'C');
        case 'K': return make_pair('G', 'T');
        case 'M': return make_pair('A', 'C');
        case 'S': return make_pair('G', 'C');
        case 'W': return make_pair('A', 'T');
        default: throw runtime_error("[ambiguous_bases()] Bad 2-base code.");
    }
}


vector<double> random_2base_distribution(char code)
{
    pair<char,char> bases = translate_2base_code(code);
    pair<char,char> bases_zero = translate_2base_code(opposite_2base_code(code));
    map<char,double> temp;
    temp[bases.first] = rand()/double(RAND_MAX);
    temp[bases.second] = 1. - temp[bases.first];
    temp[bases_zero.first] = 0.;
    temp[bases_zero.second] = 0.;
    vector<double> result;
    for (map<char,double>::const_iterator it=temp.begin(); it!=temp.end(); ++it)
        result.push_back(it->second); // result in "ACGT" order
    return result;
}


ReadSimulator::ReadSimulator(const Config& config)
:   config_(config),
    hapref_(new HaplotypeReference(config.filename_refseq,
                                   config.filename_snps,
                                   Region(config.region.id, 
                                          config.region.begin, 
                                          config.region.end + config.max_pair_distance + config.read_length))),
    base_quality_generator_(new BaseQualityGenerator(config.error_rate, config.seed)),
    bqi_(new BaseQualityInterpreter_Illumina(config.filename_bqi)),
    position_distribution_(config.region.begin, config.region.end),
    pair_distance_distribution_(config.mean_pair_distance),
    id_count_(0),
    haplotype_counts_(hapref_->haplotype_count() - 1 + config.recombined_haplotype_frequencies.size())
{
    config.validate();
    gen_.seed(config.seed);
    srand(config.seed);

    initialize_allele_frequency_map();

    // sanity check for position_distribution_

    if (config.region.begin + config.max_pair_distance + config.read_length > config.region.end)
        throw runtime_error("[ReadSimulator] Region too small.");

    // handle recombined haplotypes

    vector<double> true_haplotype_frequencies = config.haplotype_frequencies;
    true_haplotype_frequencies.resize(hapref_->haplotype_count() - 1);

    if (!config.recombined_haplotype_frequencies.empty())
        cerr << "[simreads] Warning:  allele frequency map + recombined haplotypes not correct\n";
    
    for (vector<RecombinedHaplotypeFrequency>::const_iterator it=config.recombined_haplotype_frequencies.begin();
         it!=config.recombined_haplotype_frequencies.end(); ++it)
    {
        hapref_->append_recombined_haplotype(it->haplotype_index_1, it->haplotype_index_2, it->position);
        true_haplotype_frequencies.push_back(it->frequency);
    }

    if (fabs(accumulate(true_haplotype_frequencies.begin(), true_haplotype_frequencies.end(), 0.) - 1) > 1e-6)
        throw runtime_error("[ReadSimulator] Haplotype frequencies do not sum to 1.");

    hapfreq_distribution_ = boost::random::discrete_distribution<>(true_haplotype_frequencies);
}


vector<double> as_std_vector(const ublas::vector<double>& v)
{
    vector<double> result;
    copy(v.begin(), v.end(), back_inserter(result));
    return result;
}


void ReadSimulator::initialize_allele_frequency_map()
{
    harp_misc::SymmetricDirichlet N_dist(.1, 4, config_.seed);

    HaplotypeReference::SNPTable::const_iterator snp_table_begin = hapref_->snp_table.begin();
    HaplotypeReference::SNPTable::const_iterator snp_table_end = hapref_->snp_table.end();

    for (HaplotypeReference::SNPTable::const_iterator it=snp_table_begin; it!=snp_table_end; ++it)
    {
        size_t position = it->first;

        for (vector<char>::const_iterator jt=it->second.begin(); jt!=it->second.end(); ++jt)
        {
            if (isACGT(*jt)) continue;
            
            char true_base = *jt;
            size_t haplotype = jt - it->second.begin();

            if (true_base == 'N')
                allele_frequency_map_[make_pair(position,haplotype)] = as_std_vector(N_dist());
            else // 2-base code
                allele_frequency_map_[make_pair(position,haplotype)] = random_2base_distribution(true_base);
        }
    }

    /*
    for (AlleleFrequencyMap::const_iterator it=allele_frequency_map_.begin(); it!=allele_frequency_map_.end(); ++it)
    {
        cout << it->first.first << " " << it->first.second << " ";
        copy(it->second.begin(), it->second.end(), ostream_iterator<double>(cout, " "));
        cout << endl;
    }
    */
}


char ReadSimulator::random_base(const vector<double>& dist) const
{
    boost::random::discrete_distribution<> random(dist);
    return string("ACGT")[random(gen_)];
}


string ReadSimulator::add_errors(const string& sequence, const string& illumina_quality,
                                 size_t position, size_t haplotype) const
{
    if (sequence.size() != illumina_quality.size()) throw runtime_error("[add_errors] Bad vector sizes.");

    string result = sequence;

    for (size_t i=0; i<sequence.size(); ++i)
    {
        double P_error = bqi_->error_probability(illumina_quality[i]);
        char true_base = sequence[i];

        if (!isACGT(true_base)) // true_base is ambiguous
        {
            result[i] = random_base(allele_frequency_map_.at(make_pair(position + i, haplotype)));
            //cout << "random: " << position + 1 << " " << haplotype << " " << result[i] << endl;
        }

        if (rand()/double(RAND_MAX) < P_error)
            result[i] = random_different_base(result[i]);
    }

    return result;
}


pair<Read,Read> ReadSimulator::random_read_pair() const
{
    size_t h = random_haplotype_index();
    ++haplotype_counts_[h];

    size_t position1 = random_position();
    size_t position2 = position1 + random_pair_distance();

    string sequence1 = hapref_->full_sequence(h+1, position1, position1 + config_.read_length);
    string sequence2 = hapref_->full_sequence(h+1, position2, position2 + config_.read_length);

    string illumina_quality_1(config_.read_length, '\0');
    string illumina_quality_2(config_.read_length, '\0');

    for (size_t position=0; position<config_.read_length; ++position)
    {
        size_t relative_position = int((double(position)/config_.read_length) * 100); // relative_position in {0, ..., 99}
        illumina_quality_1[position] = base_quality_generator_->random_quality(relative_position);
        illumina_quality_2[position] = base_quality_generator_->random_quality(relative_position);
    }

    Read read1;
    Read read2;

    Read::CIGAR cigar;
    cigar.op = 'M';
    cigar.count = config_.read_length;
    
    read1.cigar.push_back(cigar);
    read2.cigar.push_back(cigar);
    
    read1.position = position1;
    read2.position = position2;
    
    ostringstream id;
    id << "random_read_" << id_count_++ << "_haplotype_" << h;

    read1.name = id.str();
    read2.name = id.str();

    read1.mapping_quality = 20;
    read2.mapping_quality = 20;

    // illumina_quality = ascii - 64 = quality - 31
    read1.quality = string(config_.read_length, '\0'); 
    read2.quality = string(config_.read_length, '\0');
    transform(illumina_quality_1.begin(), illumina_quality_1.end(), read1.quality.begin(), _1 + 31);
    transform(illumina_quality_2.begin(), illumina_quality_2.end(), read2.quality.begin(), _1 + 31);

    if (config_.error_rate > 0.)
    {
        read1.sequence = add_errors(sequence1, illumina_quality_1, position1, h+1);
        read2.sequence = add_errors(sequence2, illumina_quality_2, position2, h+1);
    }
    else
    {
        read1.sequence = sequence1;
        read2.sequence = sequence2;
    }

    read1.reference_name = read2.reference_name = read1.next_reference_name = read2.next_reference_name = config_.region.id;
    read1.flags = read2.flags = 0;
    read1.next_position = read2.next_position = 0;
    read1.template_length = read2.template_length = 0;

    return make_pair(read1, read2);
}


void ReadSimulator::simulate_reads() const
{
    bfs::path outdir(".");

    bfs::ofstream os_seed(outdir / (config_.filename_stem + ".seed"));
    os_seed << "seed: " << config_.seed << endl;

    bfs::ofstream os_true_freqs(outdir / (config_.filename_stem + ".true.freqs"));
    os_true_freqs << config_.region.id << " " << config_.region.begin << " " << config_.region.end << " ";
    copy(config_.haplotype_frequencies.begin(), config_.haplotype_frequencies.end(), 
         ostream_iterator<double>(os_true_freqs, " "));
    os_true_freqs << endl;
    os_true_freqs.close();

    size_t read_pair_count = size_t(config_.region.length() * config_.coverage / (config_.read_length*2));
    os_seed << "read_pair_count: " << read_pair_count << endl; // hack: report this in .seed
    os_seed.close();

    bfs::ofstream os_samfile(outdir / (config_.filename_stem + ".sam"));
    os_samfile << sam_header_;
    for (size_t i=0; i<read_pair_count; ++i)
    {
        pair<Read,Read> s_pair = random_read_pair();
        os_samfile << s_pair.first << endl << s_pair.second << endl; 
    }
    os_samfile.close();

    size_t sum_haplotype_counts = accumulate(haplotype_counts_.begin(), haplotype_counts_.end(), 0);
    if (sum_haplotype_counts != id_count_) throw runtime_error("I don't know how to count");

    bfs::ofstream os_actual_freqs(outdir / (config_.filename_stem + ".actual.freqs"));
    os_actual_freqs << config_.region.id << " " << config_.region.begin << " " << config_.region.end << " ";
    for (vector<size_t>::const_iterator it=haplotype_counts_.begin(); it!=haplotype_counts_.end(); ++it)
        os_actual_freqs << *it/double(sum_haplotype_counts) << " ";
    os_actual_freqs << endl;
    os_actual_freqs.close();
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc<2)
        {
            cout << "#\n";
            cout << "# Usage: simreads config_filename\n";
            cout << "#\n";
            cout << endl;
            ReadSimulator::Config::print_sample_config();
            return 0;
        }

        const char* filename = argv[1];
        ReadSimulator::Config config(filename);
        cout << config << endl;

        ReadSimulator sim(config);
        sim.simulate_reads();
        return 0;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch(...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


