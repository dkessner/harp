//
// hippo_to_freqs.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "Region.hpp"
#include <iostream>
#include <sstream>
#include <fstream>


using namespace std;


struct Config
{
    Region region;

    Config(const char* filename)
    {
        ifstream is(filename);
        for (string buffer; getline(is, buffer);)
        {
            istringstream iss(buffer);
            string name, value;
            iss >> name >> value;
            if (name == "region") region = Region(value);
        }
    }
};


int main(int argc, char* argv[])
{
    try
    {
        if (argc < 3)
        {
            cout << "Usage: hippo_to_freqs filename.config results.out\n";
            return 1;
        }

        const char* filename_config= argv[1];
        const char* filename_hippo(argv[2]);

        Config config(filename_config);
        cout << config.region.id << " " << config.region.begin << " " << config.region.end << " ";
        
        ifstream is(filename_hippo);
        for (string buffer; getline(is, buffer); )
        {
            istringstream iss(buffer);
            string sequence, frequency;
            iss >> sequence >> frequency;
            cout << frequency << " ";
        }
        cout << endl;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

