//
// HaplotypeLikelihoodRecord.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _HAPLOTYPELIKELIHOODRECORD_HPP_
#define _HAPLOTYPELIKELIHOODRECORD_HPP_


#include "stdint.h"
#include "boost/static_assert.hpp"
#include <tr1/memory>
#include <vector>


BOOST_STATIC_ASSERT(sizeof(double) == 8);


struct HaplotypeLikelihoodRecord
{
    uint32_t read1_begin; // 0-based, iterator semantics
    uint32_t read1_end; 
    uint32_t read2_begin; 
    uint32_t read2_end; 
    std::vector<double> read1_logl;
    std::vector<double> read2_logl;

    HaplotypeLikelihoodRecord() : read1_begin(0), read1_end(0), read2_begin(0), read2_end(0) {}

    std::vector<double> pair_logl() const; // read1_logl + read2_logl

    void read(std::istream& is, size_t haplotype_count);
    void write(std::ostream& os) const;
};


std::ostream& operator<<(std::ostream& os, const HaplotypeLikelihoodRecord& record);


typedef std::tr1::shared_ptr<HaplotypeLikelihoodRecord> HaplotypeLikelihoodRecordPtr;


struct HaplotypeLikelihoodRecords : public std::vector<HaplotypeLikelihoodRecordPtr>
{
    uint32_t haplotype_count;

    HaplotypeLikelihoodRecords(uint32_t _haplotype_count = 0)
    :   haplotype_count(_haplotype_count)
    {}

    HaplotypeLikelihoodRecords(const std::string& filename);

    void read(std::istream& is);
    void write(std::ostream& os) const;

    // convenience functions

    const_iterator lower_bound(size_t position) const;

    uint32_t max_read1_begin() const; // assumes sorted
};


// convenience functor predicate for sort() inlining
struct HaplotypeLikelihoodRecord_Read1Begin
{
    bool operator()(const HaplotypeLikelihoodRecordPtr& p1, const HaplotypeLikelihoodRecordPtr& p2)
    {
        return p1->read1_begin < p2->read1_begin;
    }
};


#endif // _HAPLOTYPELIKELIHOODRECORD_HPP_


