#include <iostream>
#include <vector>
#include <string>
#include "api/BamMultiReader.h"
#include "api/BamWriter.h"


using namespace std;
using namespace BamTools;



int main()
{
    vector<string> inputFilenames; 
    inputFilenames.push_back("example.bam");

    BamMultiReader reader; 
    if (!reader.Open(inputFilenames)) 
    {
        cerr << "Could not open input BAM files." << endl; 
        return 0;
    }

    const SamHeader header = reader.GetHeader(); 
    const RefVector references = reader.GetReferenceData();


    cout << header.ToString() << endl;


    cout << "has index:" << reader.HasIndexes() << endl;
    cout << "locate index:" << reader.LocateIndexes() << endl;
    cout << "has index:" << reader.HasIndexes() << endl;
    

    int chr2L = reader.GetReferenceID("2L");
    cout << "chr2L: " << chr2L << endl;

    reader.SetRegion(chr2L, 22000000, chr2L, 23000000);

    BamAlignment alignment;
    while (reader.GetNextAlignment(alignment))
    {
        cout << alignment.Position << " " << alignment.AlignedBases << endl;
    }



    return 0;
}
