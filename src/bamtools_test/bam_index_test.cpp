#include "bam.h"
#include <iostream>


using namespace std;


const char* translation = "=ACMGRSVTWYHKDBN";


string get_sequence(const bam1_t* b)
{
    size_t name_size = b->core.l_qname;
    size_t cigar_size = sizeof(uint32_t) * b->core.n_cigar;
    size_t l_seq = b->core.l_qseq;
    
    uint8_t* begin = b->data + name_size + cigar_size;
    uint8_t* end = begin + (l_seq+1)/2;

    string result;
    for (uint8_t* p=begin; p!=end; p++)
    {
        result += (char)translation[(*p) >> 4];
        result += (char)translation[(*p) & 0xf];
    }
    
    if (result.size() == l_seq+1) result.resize(l_seq); // in case l_seq%2 == 1

    return result;
}


const char* quality_begin(const bam1_t* b)
{
    size_t name_size = b->core.l_qname;
    size_t cigar_size = sizeof(uint32_t) * b->core.n_cigar;
    size_t l_seq = b->core.l_qseq;

    return (const char*)(b->data + name_size + cigar_size + (l_seq+1)/2);
}


const char* quality_end(const bam1_t* b)
{
    return quality_begin(b) + b->core.l_qseq;
}


// callback signature for bam_fetch():
//   typedef int (*bam_fetch_f)(const bam1_t *b, void *data);


int callback(const bam1_t* b, void* data)
{
    cout << "position: " << b->core.pos << endl;
    cout << "name: " << b->data << endl;

    string s = get_sequence(b);
    cout << "sequence: " << s << endl;

    cout << "quality: ";
    const char* end = quality_end(b);
    for (const char* p=quality_begin(b); p!=end; ++p)
        cout << char(*p + 33); // SAM == ASCII(value + 33)
    cout << endl;

    cout << endl;
}


int main()
{
    bamFile bf = bam_open("example.bam", "r");
    bam_index_t* bai = bam_index_load("example.bam");
    // real life: check for null pointers

    bam_header_t* header = bam_header_read(bf);

    cout << header->n_targets << endl;

    for (char** p = header->target_name; p != header->target_name+header->n_targets; p++)
        cout << *p << endl;
    cout << endl;

    int tid, begin, end;
	bam_parse_region(header, "2L:22000000-23000000", &tid, &begin, &end);
    cout << "parsed region: " << tid << " " << begin << " " << end << endl << endl;

    bam_fetch(bf, bai, tid, begin, end, (void*)0, callback);

    bam_index_destroy(bai);
    bam_close(bf);

    return 0;
}


