//
// harp_misc.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_misc.hpp"
#include <iostream>
#include <sstream>
#include <cstdlib>


using namespace std;


namespace harp_misc {


vector<bool> parse_haplotype_filter_string(size_t haplotype_count, const string& haplotype_filter_string)
{
    vector<bool> result(haplotype_count);

    if (haplotype_filter_string.empty()) return vector<bool>(haplotype_count, true);

    string hack = haplotype_filter_string + ',';
    istringstream iss(hack);

    while (iss)
    {
        string range;
        getline(iss, range, ','); // a-b or a
        if (!iss) break;

        size_t a=-1ul, b=-1ul;
        size_t index_dash = range.find('-');
        if (index_dash != string::npos)
        {
            a = atoi(range.substr(0, index_dash).c_str());
            b = atoi(range.substr(index_dash+1, string::npos).c_str());
        }
        else
        {
            a = atoi(range.c_str());
            b = a;
        }

        for (size_t i=a; i<=b; ++i)
            result[i] = true;
    }

    return result;
}


Region get_extended_region(const Region& region, size_t left_buffer_size, size_t right_buffer_size)
{
    Region extended_region(region);

    if (extended_region.begin > left_buffer_size) 
        extended_region.begin -= left_buffer_size;
    else
        extended_region.begin = 0;

    extended_region.end += right_buffer_size;

    return extended_region;
}


} // namespace harp_misc


