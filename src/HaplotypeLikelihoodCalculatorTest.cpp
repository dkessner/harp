//
// HaplotypeLikelihoodCalculatorTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeLikelihoodCalculator.hpp"
#include "unit.hpp"
#include "boost/lambda/lambda.hpp"
#include <iostream>
#include <iterator>


using namespace std;
using namespace bampp;
using namespace boost::lambda;


const char* refseq_ =  // manually extracted from dmel refseq
    "GCGGCCAGGCGTAAATTTGAGTGGGCAAAGGTGTTTGGAAGTTTAAGTCA"
    "TCCATGGCGTTCAGGATAGTATTGCCCAAAGGTAACTCCTTGTAACTAGT"
    "GACAGGATTCACTGGCTCGGGACTATGGGCCAGCACTAGATGCTTCAAAC"
    "AAACACTTGGCTTCCTATCACCCATGTTTTCTGAGCGGCATACGTGCATC"
    "TTTTCTACACATCTTTTTGTTGTTGAAACATTAGAATTTGGCATCTGGCT"
    "TCGATTTTTTTGTACACTTCTTTCGGATGGGTTTTTACCAAGCATGTCAT"
    "TAAATATTTGTTCCATTTTATCCATTGAAAATACTACTGTTCGATTATTC"
    "GACTGGTTAGTACAATTTTCGGTTAAAGGTACGGATAACGAAGTAGTTAA";


shared_ptr<HaplotypeReference> get_hapref(const Region& region)
{
    shared_ptr<HaplotypeReference> hapref(
        new HaplotypeReference("", // no refseq
                               "test_files/sparse_snps_2L_10m.txt",
                               region));

    unit_assert(hapref->snp_table.size() == 21);

    hapref->refseq = refseq_;

    void sanity_check(const HaplotypeReference& hapref);
    sanity_check(*hapref); // after manual refseq setting

    return hapref;
}


vector< shared_ptr<Read> > get_reads(const Region& region)
{
    BAMFile bamfile("test_files/test_1pair.bam");
    ReadCollector collector;
    bamfile.process_reads(region, collector);
    return collector.reads;
}


// forward declaration to allow hidden implementation testing
namespace HaplotypeLikelihoodCalculator_Implementation {
void get_positions_and_indices(const HaplotypeReference& hapref, const Region& read_region,
                               vector<size_t>& positions, vector<size_t>& indices_read, vector<size_t>& indices_refseq);
} // namespace HaplotypeLikelihoodCalculator_Implementation 


string indexed_substring(string s, const vector<size_t>& indices)
{
    string result(indices.size(), '\0');
    for (size_t i=0; i<indices.size(); ++i) result[i] = s[indices[i]];
    return result;
}


void test_positions_and_indices()
{
    //cout << "test_positions_and_indices()\n";

    // test this part specifically because it's easy to get wrong

    Region region("2L", 9999900, 10000300);

    shared_ptr<HaplotypeReference> hapref = get_hapref(region);
    vector< shared_ptr<Read> > reads = get_reads(region);

    const Read& read = *reads[0];
    Region read_region(hapref->region.id, read.position, read.position+read.sequence.size());

    using HaplotypeLikelihoodCalculator_Implementation::get_positions_and_indices;
    vector<size_t> positions, indices_read, indices_refseq;
    get_positions_and_indices(*hapref, read_region, positions, indices_read, indices_refseq);

    unit_assert(positions.size() == 5 && positions[0] == 10000015 && positions[1] == 10000022 &&
                positions[2] == 10000028 && positions[3] == 10000032 && positions[4] == 10000088);
    unit_assert(indices_read.size() == 5 && indices_read[0] == 15 && indices_read[1] == 22 &&
                indices_read[2] == 28 && indices_read[3] == 32 && indices_read[4] == 88);
    unit_assert(indices_refseq.size() == 5 && indices_refseq[0] == 115 && indices_refseq[1] == 122 &&
                indices_refseq[2] == 128 && indices_refseq[3] == 132 && indices_refseq[4] == 188);

    unit_assert(indexed_substring(read.sequence, indices_read) == "ACGAT");
    unit_assert(indexed_substring(hapref->refseq, indices_refseq) == "CCGGC");
}


void test_likelihood()
{
    //cout << "test_likelihood()\n";

    Region region("2L", 9999900, 10000300);

    shared_ptr<HaplotypeReference> hapref = get_hapref(region);
    shared_ptr<BaseQualityInterpreter_Illumina> bqi(new BaseQualityInterpreter_Illumina());

    vector< shared_ptr<Read> > reads = get_reads(region);
    const Read& read = *reads[0];
    Region read_region(hapref->region.id, read.position, read.position+read.sequence.size());

    HaplotypeLikelihoodCalculator calc(hapref, bqi, true);
    vector<double> result = calc.likelihood_variant(read);
    vector<double> result_full = calc.likelihood_full(read);

    using HaplotypeLikelihoodCalculator_Implementation::get_positions_and_indices;
    vector<size_t> positions, indices_read, indices_refseq;
    get_positions_and_indices(*hapref, read_region, positions, indices_read, indices_refseq);

    unit_assert(indexed_substring(read.sequence, indices_read) == "ACGAT");
    unit_assert(indexed_substring(hapref->refseq, indices_refseq) == "CCGGC");

    unit_assert(result.size() == hapref->haplotype_count());
    const double epsilon = 1e-4;

    unit_assert(hapref->variant_sequence(1, positions) == "CCGGC");
    unit_assert_equal(result[1], -26.5523, epsilon);

    unit_assert(hapref->variant_sequence(2, positions) == "ACGGT");
    unit_assert_equal(result[2], -9.85153, epsilon);

    unit_assert(hapref->variant_sequence(3, positions) == "CCGGT");
    unit_assert_equal(result[3], -19.4695, epsilon);

    unit_assert(hapref->variant_sequence(4, positions) == "ACGAT");
    unit_assert_equal(result[4], -0.00325022, epsilon);

    // check likelihood_full == shift of likelihood_variant 
   
    unit_assert_equal(result_full[2]-result_full[1], result[2]-result[1], epsilon);
    unit_assert_equal(result_full[3]-result_full[1], result[3]-result[1], epsilon);
    unit_assert_equal(result_full[4]-result_full[1], result[4]-result[1], epsilon);


    /*
    cout << indexed_substring(read.sequence, indices_read) << " read" << endl;
    cout << indexed_substring(hapref->refseq, indices_refseq) << " refseq" << endl << endl;

    for (size_t haplotype=0; haplotype<10; haplotype++)
        cout << hapref->variant_sequence(haplotype, positions) << " " << result[haplotype] 
             << " " << result_full[haplotype] << endl;
    cout << endl;
    */
}


namespace HaplotypeLikelihoodCalculator_Implementation {
void test_SiteLikelihoodFunctor();
} // namespace HaplotypeLikelihoodCalculator_Implementation 



void test_likelihood_explore()
{
    //cout << "test_likelihood_explore()\n";

    Region region("3L", 8000061, 8000161);

    shared_ptr<HaplotypeReference> hapref(new HaplotypeReference("", "test_files/sparse_snps_3L_8m_hacked.txt", region));
    hapref->refseq = "CTATTGGCCAGCATCTTTTCCATTGCAATTGAAATGCCTTCAAGTGTGAAGAGTATGTCATATCTACCAGCTCCCCTTTGTTTTTTACGATCTTCTGCCC";
    shared_ptr<BaseQualityInterpreter_Illumina> bqi(new BaseQualityInterpreter_Illumina());
    HaplotypeLikelihoodCalculator calc(hapref, bqi, true);

    Read read;
    read.position = 8000061;
    read.sequence = "GTTTTGGCCAGCATCTTTTCCATTGCAATTGAAGTGNCTCCAAGTGTGAAGAGTATGTGATATCTGCCAGCTCCCCTTTCGTTTTTACGATCTGCTGCC";
    string ascii = "ggUgbf\\egggefgggeffZgggbec]fdebTgedZfgcBe_e_gfgcadggfeggagFf\\egTeBedcgbabbeBdagBBcB`Xdc]Vgde_BQcdb\\e";
    transform(ascii.begin(), ascii.end(), back_inserter(read.quality), _1 - 33);

    vector<double> result = calc.likelihood_variant(read);

    cout << "result: " << endl;
    copy(result.begin(), result.end(), ostream_iterator<double>(cout, " "));
    cout << endl;
}



int main()
{
    try
    {
        test_positions_and_indices();
        test_likelihood();
        HaplotypeLikelihoodCalculator_Implementation::test_SiteLikelihoodFunctor();
        //test_likelihood_explore();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

