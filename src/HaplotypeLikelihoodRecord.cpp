//
// HaplotypeLikelihoodRecord.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeLikelihoodRecord.hpp"
#include "boost/lambda/lambda.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <iterator>
#include <cmath>
#include <numeric>


using namespace std;
using namespace boost::lambda;


//
// HaplotypeLikelihoodRecord
//


vector<double> HaplotypeLikelihoodRecord::pair_logl() const
{
    if (read1_logl.size() != read2_logl.size())
        throw runtime_error("[HaplotypeLikelihoodRecord::pair_log()] Unequal logl vector sizes.");
    vector<double> result;
    transform(read1_logl.begin(), read1_logl.end(), read2_logl.begin(), back_inserter(result), plus<double>());
    return result; 
}


void HaplotypeLikelihoodRecord::read(istream& is, size_t haplotype_count)
{
    is.read((char*)&read1_begin, sizeof(uint32_t));
    is.read((char*)&read1_end, sizeof(uint32_t));
    is.read((char*)&read2_begin, sizeof(uint32_t));
    is.read((char*)&read2_end, sizeof(uint32_t));
    read1_logl.resize(haplotype_count);
    read2_logl.resize(haplotype_count);
    is.read((char*)&read1_logl[0], read1_logl.size() * sizeof(double));
    is.read((char*)&read2_logl[0], read2_logl.size() * sizeof(double));
}


void HaplotypeLikelihoodRecord::write(ostream& os) const
{
    os.write((const char*)&read1_begin, sizeof(uint32_t));
    os.write((const char*)&read1_end, sizeof(uint32_t));
    os.write((const char*)&read2_begin, sizeof(uint32_t));
    os.write((const char*)&read2_end, sizeof(uint32_t));
    os.write((const char*)&read1_logl[0], read1_logl.size() * sizeof(double));
    os.write((const char*)&read2_logl[0], read2_logl.size() * sizeof(double));
}


ostream& operator<<(ostream& os, const HaplotypeLikelihoodRecord& record)
{
    os << record.read1_begin << " " << record.read1_end << " ";
    copy(record.read1_logl.begin(), record.read1_logl.end(), ostream_iterator<double>(os, " "));
    os << endl;

    os << record.read2_begin << " " << record.read2_end << " ";
    copy(record.read2_logl.begin(), record.read2_logl.end(), ostream_iterator<double>(os, " "));
    os << endl;

    return os;
}


//
// HaplotypeLikelihoodRecords
//


HaplotypeLikelihoodRecords::HaplotypeLikelihoodRecords(const string& filename)
{
    ifstream is(filename.c_str());
    if (!is) throw runtime_error(("[HaplotypeLikelihoodRecords] Unable to open file " + filename).c_str());
    read(is);
}


namespace {
const char* magic_ = "HARPLIKE";
const size_t magic_length_ = 8;
const uint32_t version_major_ = 1;
const uint32_t version_minor_ = 1;
} // namespace


// header:
//   char[8] magic_;
//   uint32_t version_major_;
//   uint32_t version_minor_;
//   uint32_t haplotype_count;
//   uint32_t record_count;


void HaplotypeLikelihoodRecords::read(istream& is)
{
    char magic[magic_length_];
    is.read(magic, strlen(magic_));
    if (strncmp(magic, magic_, magic_length_))
        throw runtime_error("[HaplotypeLikelihoodRecords::read()] Invalid haplotype likelihood file.");

    uint32_t version_major = uint32_t(-1), version_minor = uint32_t(-1);
    is.read((char*)&version_major, sizeof(uint32_t));
    is.read((char*)&version_minor, sizeof(uint32_t));
    if (version_major != version_major_ || version_minor != version_minor_)
        throw runtime_error("[HaplotypeLikelihoodRecords::read()] Version numbers do not match.");

    is.read((char*)&haplotype_count, sizeof(uint32_t));
    uint32_t record_count = 0;
    is.read((char*)&record_count, sizeof(uint32_t));
    resize(record_count);
    for (size_t i=0; i<record_count; ++i)
    {
        at(i) = HaplotypeLikelihoodRecordPtr(new HaplotypeLikelihoodRecord);
        at(i)->read(is, haplotype_count);
    }
}


void HaplotypeLikelihoodRecords::write(ostream& os) const
{
    os.write(magic_, strlen(magic_));
    os.write((const char*)&version_major_, sizeof(uint32_t));
    os.write((const char*)&version_minor_, sizeof(uint32_t));
    os.write((const char*)&haplotype_count, sizeof(uint32_t));
    uint32_t record_count = size();
    os.write((const char*)&record_count, sizeof(uint32_t));
    for (HaplotypeLikelihoodRecords::const_iterator it=begin(); it!=end(); ++it)
        (**it).write(os);
}


HaplotypeLikelihoodRecords::const_iterator HaplotypeLikelihoodRecords::lower_bound(size_t position) const
{
    HaplotypeLikelihoodRecordPtr dummy(new HaplotypeLikelihoodRecord);

    dummy->read1_begin = position;
    return std::lower_bound(begin(), end(), dummy, HaplotypeLikelihoodRecord_Read1Begin());
}


uint32_t HaplotypeLikelihoodRecords::max_read1_begin() const
{
    if (empty() || !back().get()) return 0;
    return back()->read1_begin;
}


