//
// harp_config_test.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "unit.hpp"
#include <iostream>
#include <vector>
#include <iterator>
#include <cstring>


using namespace std;


struct CommandLine
{
    int argc;
    char** argv;

    CommandLine(const string& command_line)
    {
        istringstream iss(command_line);
        copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(args));
        argc = args.size();
        for (vector<string>::const_iterator it=args.begin(); it!=args.end(); ++it)
            argp.push_back(const_cast<char*>(it->c_str()));
        argv = &argp[0];
    }

    private:
    vector<string> args;
    vector<char*> argp;
};


void test_CommandLine()
{
    CommandLine cl("The quick brown");
    unit_assert(cl.argc == 3);
    unit_assert(!strcmp(cl.argv[0], "The"));
    unit_assert(!strcmp(cl.argv[1], "quick"));
    unit_assert(!strcmp(cl.argv[2], "brown"));
}


void test_Config()
{
    CommandLine cl("/path/to/harp like -b /path/to/blah.bam -r 2L:1001-2000 --refseq dmel_blah.txt "
                   "--refseqlist blahlist.txt --snps snps_blah.txt freq -v --haplotype_filter 1-7,42 "
                   "--bqi hello.bqi --min_mapping_quality 14 "
                   "--logl_min_zscore -2 --logl_mean -13.1 --logl_sd 4.4 ");

    harp_config::Config config(cl.argc, cl.argv);
    //cout << "config:\n" << config << endl;

    unit_assert(config.function.size() == 2);
    unit_assert(config.function[0] == "like");
    unit_assert(config.function[1] == "freq");

    unit_assert(config.filename_bam == "/path/to/blah.bam");
    unit_assert(config.region.str() == "2L:1001-2000");
    unit_assert(config.filename_refseq == "dmel_blah.txt");
    unit_assert(config.filename_refseqlist == "blahlist.txt");
    unit_assert(config.filename_snps == "snps_blah.txt");
    unit_assert(config.filename_bqi == "hello.bqi");
    unit_assert(config.min_mapping_quality == 14);
    unit_assert(config.logl_min_zscore == -2);
    unit_assert_equal(config.logl_mean, -13.1, 1e-6);
    unit_assert_equal(config.logl_sd, 4.4, 1e-6);

    unit_assert(config.verbose);
    unit_assert(!config.compute_standard_errors);

    // test calculated defaults
    unit_assert(config.window_step_size == 1000);
    unit_assert(config.window_width == 1000);

    unit_assert(config.freq_config.haplotype_filter == "1-7,42");
}


void test_options_string()
{
    cout << "test_options_string()\n";
    cout << harp_config::options_string() << endl;
}


void test_filename_calculations()
{
    CommandLine cl("/path/to/harp like -b /path/to/blah.bam -r 2L:1001-2000");
    harp_config::Config config(cl.argc, cl.argv);
    unit_assert(config.filename_stem == "blah.2L_1001-2000");
    unit_assert(config.dirname_output == "blah.2L_1001-2000.output");
    unit_assert(config.filename_hlk == "blah.2L_1001-2000.hlk");
    unit_assert(config.filename_freqs == "blah.2L_1001-2000.freqs");
    
    CommandLine cl2("/path/to/harp like -b /path/to/blah.bam"); // no region specified
    harp_config::Config config2(cl2.argc, cl2.argv);
    unit_assert(config2.filename_stem.empty());
    unit_assert(config2.dirname_output.empty());
    unit_assert(config2.filename_hlk.empty());
    unit_assert(config2.filename_freqs.empty());

    CommandLine cl3("/path/to/harp like -b /path/to/blah.bam -r 2L:1001-2000 --stem goober");
    harp_config::Config config3(cl3.argc, cl3.argv);
    unit_assert(config3.filename_stem == "goober");
    unit_assert(config3.dirname_output == "goober.output");
    unit_assert(config3.filename_hlk == "goober.hlk");
    unit_assert(config3.filename_freqs == "goober.freqs");
 
    CommandLine cl4("/path/to/harp like -b /path/to/blah.bam -r 2L:1001-2000 --stem goober --freqs blah.freqs --hlk blah2.hlk --out blah3.output");
    harp_config::Config config4(cl4.argc, cl4.argv);
    unit_assert(config4.filename_stem == "goober");
    unit_assert(config4.dirname_output == "blah3.output");
    unit_assert(config4.filename_hlk == "blah2.hlk");
    unit_assert(config4.filename_freqs == "blah.freqs");
}


int main()
{
    try
    {
        test_CommandLine();
        //test_options_string();
        test_Config();
        test_filename_calculations();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

