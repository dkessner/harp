//
// harp_bqi.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "BAMFile.hpp"
#include "HaplotypeReference.hpp"
#include <iostream>
#include <iterator>
#include <cmath>
#include <algorithm>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;


namespace {


class ScoreCounter : public bampp::ReadProcessor
{
    public:

    static const size_t score_count_ = 41; // Illumina qual scores in [0,40]

    ScoreCounter(const HaplotypeReference& hapref)
    :   hapref_(hapref),
        count_monomorphic_correct_(score_count_),
        count_monomorphic_incorrect_(score_count_)
    {}

    virtual int process_read(shared_ptr<bampp::Read> read);
    void report();

    private:

    const HaplotypeReference& hapref_;

    vector<size_t> count_monomorphic_correct_;
    vector<size_t> count_monomorphic_incorrect_;
};


int ScoreCounter::process_read(shared_ptr<bampp::Read> read)
{
    const int min_mapping_quality = 15;
    if (read->mapping_quality < min_mapping_quality)
    {
        //cout << "filter: mapping quality\n";
        return 0;
    }

    if (read->cigar.size() != 1 ||
        read->cigar[0].op!='M' ||
        read->cigar[0].count != read->sequence.size())
    {
        //cout << "filter: cigar\n";
        return 0;
    }

    size_t read_position_end = read->position + read->sequence.size();
    string refseq = hapref_.full_sequence(0, read->position, read_position_end);
    vector<bool> polymorphic(refseq.size());

    for (size_t position=read->position; position!=read_position_end; ++position)
        if (hapref_.snp_table.count(position) > 0)
            polymorphic[position - read->position] = true;

    /*
    cout << read->position << " " << read->sequence << endl;
    cout << read->position << " " << refseq << endl;
    cout << read->position << " ";
    copy(polymorphic.begin(), polymorphic.end(), ostream_iterator<bool>(cout, ""));
    cout << endl;
    cout << endl;
    */

    string illumina_quality = bampp::quality_to_illumina(read->quality);

    if (read->flags & bampp::Read::Flag_Reversed)
    {
        string temp;
        copy(illumina_quality.rbegin(), illumina_quality.rend(), back_inserter(temp));
        illumina_quality = temp;
    }

    // sanity check: Illumina encoding of quality scores
    for (string::const_iterator it=illumina_quality.begin(); it!=illumina_quality.end(); ++it)
    {
        if (*it < 0 || *it >= int(score_count_))
        {
            cout << read << endl;
            throw runtime_error("[qual_hist] I am insane!");
        }
    }

    for (size_t i=0; i<read->sequence.size(); ++i)
    {
        if (polymorphic[i]) continue;

        if (read->sequence[i] == refseq[i])
            ++count_monomorphic_correct_[illumina_quality[i]];
        else
            ++count_monomorphic_incorrect_[illumina_quality[i]];
    }

    return 0;
}


double proportion_incorrect(size_t count_correct, size_t count_incorrect)
{
    double count_total = count_correct + count_incorrect;
    return count_total > 0 ? count_incorrect/count_total : .75;
}


void ScoreCounter::report()
{
    //cout << "# count_monorphic_correct: ";
    //copy(count_monomorphic_correct_.begin(), count_monomorphic_correct_.end(), ostream_iterator<size_t>(cout, " "));
    //cout << endl;
    //cout << "# count_monorphic_incorrect: ";
    //copy(count_monomorphic_incorrect_.begin(), count_monomorphic_incorrect_.end(), ostream_iterator<size_t>(cout, " "));
    //cout << endl;

    cout << "BaseQualityInterpreter_Illumina ";
    transform(count_monomorphic_correct_.begin(), count_monomorphic_correct_.end(), count_monomorphic_incorrect_.begin(),
              ostream_iterator<double>(cout, " "), proportion_incorrect);
    cout << endl;
}


int go(const harp_config::Config& config)
{
    Region extended_region(config.region.id, config.region.begin - 200, config.region.end + 200);
    HaplotypeReference hapref(config.filename_refseq, config.filename_snps, extended_region);

    bampp::BAMFile bam(config.filename_bam);
    ScoreCounter counter(hapref);

    bam.process_reads(config.region, counter);
    counter.report();

    return 0;
}


int harp_bqi(const harp_config::Config& config)
{
    if (config.filename_bam.empty())
        throw runtime_error("[harp_bqi] No BAM filename specified.");

    if (config.region.id.empty())
        throw runtime_error("[harp_bqi] No region specified.");

    if (config.filename_refseq.empty())
        throw runtime_error("[harp_bqi] No refseq specified.");

    if (config.filename_snps.empty())
        throw runtime_error("[harp_bqi] No SNP file specified.");

    return go(config);
}


string harp_bqi_info()
{
    return "calculate empirical base quality interpretation from monomorphic sites";
}


string harp_bqi_usage()
{
    ostringstream oss;
    oss << "harp bqi required parameters:\n";
    oss << "  bam\n";
    oss << "  region\n";
    oss << "  refseq\n";
    oss << "  snps\n";
    return oss.str();
}


} // namespace


harp_config::JumpTableEntry entry_harp_bqi_(harp_bqi, harp_bqi_info, harp_bqi_usage);



