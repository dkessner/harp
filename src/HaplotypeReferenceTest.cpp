//
// HaplotypeReferenceTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeReference.hpp"
#include "unit.hpp"
#include <tr1/array>
#include <iostream>
#include <iterator>
#include <fstream>


using namespace std;
using tr1::array;
using boost::iostreams::stream_offset;
using boost::iostreams::position_to_offset;
using boost::iostreams::offset_to_position;


void test_create_snp_table_index()
{
    const char* filename = "test_files/sparse_snps.txt";
    SNPTableIndexPtr index = create_snp_table_index(filename, 10);
    unit_assert(index->size() == 9);

    SNPTableIndex::const_iterator it = index->lower_bound(15000941);
    stream_offset offset = it->second;

    ifstream is(filename);
    if (!is) throw runtime_error("[test_create_snp_table_index()] Unable to open file.");
    is.seekg(offset);

    string buffer;
    getline(is, buffer);
    unit_assert(atoi(buffer.c_str()) == 15000947); // 1-based
}


void test_read_snp_table_index()
{
    SNPTableIndexPtr read_snp_table_index(const string& filename);
    SNPTableIndexPtr index = read_snp_table_index("test_files/sparse_snps.txt.idx");

    unit_assert(index->size() == 9);
    unit_assert((*index)[15000107] == 4036);
    unit_assert((*index)[15000214] == 7426);
    unit_assert((*index)[15001128] == 31127);
}


void print_stuff(const HaplotypeReference& hapref)
{
    cout << "region: " << hapref.region << endl;

    cout << "haplotype ids [" << hapref.haplotype_count() << "]: ";
    copy(hapref.haplotype_ids.begin(), hapref.haplotype_ids.end(), ostream_iterator<string>(cout," "));
    cout << endl;

    for (HaplotypeReference::SNPTable::const_iterator it=hapref.snp_table.begin(); it!=hapref.snp_table.end(); ++it)
    {
        cout << it->first << " ";
        copy(it->second.begin(), it->second.end(), ostream_iterator<char>(cout,","));
        cout << endl;
    }
    cout << endl;
}


void test_snp_table(const char* filename)
{
    cout << "test_snp_table(): " << filename << endl;

    HaplotypeReference hapref("", 
                              filename, 
                              Region("2L", 15000025, 15000200));

    // test sanity_check() with manual refseq

    hapref.refseq = 
        "GTGCTGCGATGGGAGATTGTTGTCTGTCGATGGAATGGTAATTGTAGTAT"
        "AATTATAATGGGCTTAATGGAAGCCGCAGTGCTTGCGATGGCAAATAAGC"
        "AGATTGCTTTTAAAAATTATAATAACTTACTGGATTTGCAGGGCTTATGA"
        "ATAAGCTATGATTAGTAAAATGTTG";

    void sanity_check(const HaplotypeReference& hapref);
    sanity_check(hapref);

    //print_stuff(hapref);

    unit_assert(hapref.region.id == "2L");
    unit_assert(hapref.haplotype_count() == 163); // including Ref
    unit_assert(hapref.snp_table.size() == 14);
    unit_assert(hapref.snp_table[15000100][0] == 'G');
    unit_assert(hapref.snp_table[15000100][8] == 'C');
    unit_assert(hapref.snp_table[15000105][0] == 'G');
    unit_assert(hapref.snp_table[15000152][0] == 'T');
    unit_assert(hapref.snp_table[15000152][1] == 'C');
    unit_assert(hapref.snp_table[15000152][2] == 'T');

    size_t a[] = {15000027, 15000051, 15000084, 15000100, 15000106};
    vector<size_t> positions(a, a+sizeof(a)/sizeof(size_t));
    unit_assert(hapref.variant_sequence(0, positions) == "GTGGC");
    unit_assert(hapref.variant_sequence(1, positions) == "GCGGC");
    unit_assert(hapref.variant_sequence(17, positions) == "GYRGC");

    string full0 = hapref.full_sequence(0, 15000025, 15000101);
    string full1 = hapref.full_sequence(1, 15000025, 15000101);
    string full17 = hapref.full_sequence(17, 15000025, 15000101);

    const char* full0_good =  "GTGCTGCGATGGGAGATTGTTGTCTGTCGATGGAATGGTAATTGTAGTATAATTATAATGGGCTTAATGGAAGCCG";
    const char* full1_good =  "GTGCTGCGATGGGAGATTGTTGTCTGCCGATGGAATGGTAATTGTAGTATAATTATAATGGGCTTAATGGAAGACG";
    const char* full17_good = "GTGCTGCGATGGGAGATTGTTGTCTGYCNATGGAATGGTAATTGTAGTATAATTATAATRGGCTTAATGGAAGACG";

    //cout << full0 << endl << full0_good << endl;
    //cout << full1 << endl << full1_good << endl;
    //cout << full17 << endl << full17_good << endl;
    unit_assert(full0 == full0_good);
    unit_assert(full1 == full1_good);
    unit_assert(full17 == full17_good);

    // test append_recombined_haplotype()

    const char* full_recombined_17_0_15000054_good = "GTGCTGCGATGGGAGATTGTTGTCTGYCNATGGAATGGTAATTGTAGTATAATTATAATGGGCTTAATGGAAGCCG";
    hapref.append_recombined_haplotype(17, 0, 15000054);
    unit_assert(hapref.haplotype_count() == 164); // including Ref
    unit_assert(hapref.variant_sequence(163, positions) == "GYGGC");
    unit_assert(hapref.full_sequence(163, 15000025, 15000101) == full_recombined_17_0_15000054_good);
}


void test_refseq()
{
    Region region("2L", 100, 200);
    HaplotypeReference hapref("test_files/test.fasta", 
                              "", 
                              region);

    const char* good = "GCCAACATATTGTGCTCTTTGATTTTTTGGCAACCCAAAATGGTGGCGGA"
                       "TGAACGAGATGATAATATATTCAAGTTGCCGCTAATCAGAAATAAATTCA";
    
    unit_assert(hapref.refseq.size() == 100);
    unit_assert(hapref.refseq == good);
}


int main()
{
    try
    {
        test_create_snp_table_index();
        test_read_snp_table_index();
        test_snp_table("test_files/sparse_snps.txt");
        test_snp_table("test_files/sparse_snps_1.txt");
        test_snp_table("test_files/sparse_snps_2.txt");
        test_refseq();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

