//
// harp_recomb.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "HaplotypeLikelihoodRecord.hpp"
#include "boost/numeric/ublas/vector.hpp"
#include <iostream>
#include <fstream>
#include <iterator>
#include <cmath>
#include <algorithm>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
namespace ublas = boost::numeric::ublas;


namespace {


void read_haplotype_frequencies(const string& filename, vector<double>& haplotype_frequencies, Region& region)
{
    ifstream is(filename.c_str());
    if (!is) throw runtime_error(("[harp_recomb] Unable to open file " + filename).c_str());

    string id;
    size_t begin, end;
    is >> id >> begin >> end;
    region = Region(id, begin, end);
    haplotype_frequencies.clear();
    copy(istream_iterator<double>(is), istream_iterator<double>(), back_inserter(haplotype_frequencies));
}


class Detector
{
    public:

    Detector(const Region& region, const vector<double>& haplotype_frequencies, 
             const HaplotypeLikelihoodRecords& records)
    :   region_(region), haplotype_frequencies_(haplotype_frequencies.size()), records_(records),
        sum_squared_differences_(region.length()), coverages_(region.length())
    {
        copy(haplotype_frequencies.begin(), haplotype_frequencies.end(), haplotype_frequencies_.begin());
    }

    void iterate_records();
    void report() const;

    private:

    Region region_;
    ublas::vector<double> haplotype_frequencies_;
    const HaplotypeLikelihoodRecords& records_;

    ublas::vector<double> sum_squared_differences_;
    ublas::vector<int> coverages_;

    ublas::vector<double> posterior(const vector<double>& logl);
    double sum_squared_difference(const HaplotypeLikelihoodRecord& record);
};


void Detector::iterate_records() // first try
{
    cout << "iterate_records\n";

    //size_t count = 0;

    for (HaplotypeLikelihoodRecords::const_iterator it=records_.begin(); it!=records_.end(); ++it)
    {
        double d2 = sum_squared_difference(**it);
        //cout << count++ << " " << (*it)->read1_begin << " " << (*it)->read2_begin << " " << d2 << endl; 
        
        size_t index_begin = (*it)->read1_end - region_.begin; 
        size_t index_end = (*it)->read2_begin - region_.begin; 
        for (size_t index=index_begin; index!=index_end; ++index)
        {
            sum_squared_differences_[index] += d2;
            ++coverages_[index];
        }
    }
}


/*
void Detector::iterate_records()
{
    cout << "iterate_records -- exploratory\n";

    //ublas::vector<double> likelihood_1 = shift_unlog(record.read1_logl); // <P(read1 | h) : h>
    //ublas::vector<double> likelihood_2 = shift_unlog(record.read2_logl); // <P(read2 | h) : h>

    ofstream os2("2.txt");
    ofstream os5("5.txt");
    ofstream os8("8.txt");
    ofstream osleftover("leftover.txt");

    for (HaplotypeLikelihoodRecords::const_iterator it=records_.begin(); it!=records_.end(); ++it)
    {
        //ublas::vector<double> posterior_pair = posterior(record.read1_logl + record.read2_logl);

        const HaplotypeLikelihoodRecord& record = **it;
        Region read_region("blah", record.read1_begin, record.read2_end);

        //vector<double> pair_logl = record.pair_logl();
        //double max_logl = *max_element(pair_logl.begin(), pair_logl.end());

        ublas::vector<double> posterior_pair = posterior(record.pair_logl());

        double max_posterior = *max_element(posterior_pair.begin(), posterior_pair.end());

        if (read_region.contains(15020000))
            os2<< max_posterior << endl;
        else if (read_region.contains(15050000))
            os5<< max_posterior << endl;
        else if (read_region.contains(15081000))
            os8<< max_posterior << endl;
        else
            osleftover << max_posterior << endl;
    }
}
*/

void Detector::report() const
{
    for (size_t i=0; i<sum_squared_differences_.size(); ++i)
    {
        size_t position = region_.begin + i;
        double mean_squared_difference = sum_squared_differences_[i]/coverages_[i];
        cout << position << " " << sum_squared_differences_[i] << " " << coverages_[i] << " " << mean_squared_difference << endl;
    }
}


ublas::vector<double> shift_unlog(vector<double> logl)
{
    ublas::vector<double> result(logl.size() - 1); // remove Ref (haplotype 0)
    transform(logl.begin() + 1, logl.end(), result.begin(), (double (*)(double))exp);
    return result;
}


ublas::vector<double> Detector::posterior(const vector<double>& logl)
{
    ublas::vector<double> likelihood = shift_unlog(logl); // <P(read | h) : h>
    ublas::vector<double> joint = element_prod(likelihood, haplotype_frequencies_);  // < P(read,h|f) : h >
    double marginal_likelihood = sum(joint); // P(read|f)
    ublas::vector<double> posterior = joint / marginal_likelihood; // < P(h|read) : h >  -- posterior haplotype assignment
    return posterior;
}


double Detector::sum_squared_difference(const HaplotypeLikelihoodRecord& record)
{
    ublas::vector<double> posterior_1 = posterior(record.read1_logl);
    ublas::vector<double> posterior_2 = posterior(record.read2_logl);
    ublas::vector<double> diff = posterior_1 - posterior_2;
    double sum_squared_diff = inner_prod(diff, diff);
    return sum_squared_diff;
}


int go(const harp_config::Config& config)
{
    cout << "harp recomb\n";

    vector<double> haplotype_frequencies;
    Region region;
    read_haplotype_frequencies(config.filename_freqs, haplotype_frequencies, region);

    cout << "region: " << region << endl;

    HaplotypeLikelihoodRecords records(config.filename_hlk);

    Detector detector(region, haplotype_frequencies, records);
    detector.iterate_records();
    detector.report();

    return 0;
}


int harp_recomb(const harp_config::Config& config)
{
    if (config.filename_hlk.empty())
        throw runtime_error("[harp_recomb] No .hlk filename specified.");

    if (config.filename_freqs.empty())
        throw runtime_error("[harp_recomb] No .hlk filename specified.");

    return go(config);
}


string harp_recomb_info()
{
    return "detect recombined haplotypes";
}


string harp_recomb_usage()
{
    ostringstream oss;
    oss << "harp recomb required parameters:\n";
    oss << "  hlk\n";
    oss << "  freqs\n";
    return oss.str();
}


} // namespace


harp_config::JumpTableEntry entry_harp_recomb_(harp_recomb, harp_recomb_info, harp_recomb_usage);



