//
// qual_hist.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BAMFile.hpp"
#include "HaplotypeReference.hpp"
#include <iostream>
#include <iterator>
#include <algorithm>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
using namespace bampp;


static const size_t score_count_ = 41; // Illumina qual scores in [0,40]


class QualHistogram : public ReadProcessor
{
    public:

    QualHistogram(const HaplotypeReference& hapref)
    :   hapref_(hapref), read_count_(0), 
        count_monomorphic_correct_(score_count_),
        count_monomorphic_incorrect_(score_count_)
    {
        initialize_histogram(100); // independent of read length
    }

    virtual int process_read(shared_ptr<Read> read);
    void report();

    private:

    const HaplotypeReference& hapref_;
    size_t read_count_;

    typedef vector< vector<size_t> > Histogram;
    Histogram histogram_;

    vector<size_t> count_monomorphic_correct_;
    vector<size_t> count_monomorphic_incorrect_;

    void initialize_histogram(size_t position_count);
};


void QualHistogram::initialize_histogram(size_t position_count)
{
    histogram_.resize(position_count);
    for (Histogram::iterator it=histogram_.begin(); it!=histogram_.end(); ++it)
        it->resize(score_count_); 
}


int QualHistogram::process_read(shared_ptr<Read> read)
{
    ++read_count_;

    size_t read_position_end = read->position + read->sequence.size();
    string refseq = hapref_.full_sequence(0, read->position, read_position_end);
    vector<bool> polymorphic(refseq.size());

    for (size_t position=read->position; position!=read_position_end; ++position)
        if (hapref_.snp_table.count(position) > 0)
            polymorphic[position - read->position] = true;

    /*
    cout << read->position << " " << read->sequence << endl;
    cout << read->position << " " << refseq << endl;
    cout << read->position << " ";
    copy(polymorphic.begin(), polymorphic.end(), ostream_iterator<bool>(cout, ""));
    cout << endl;
    cout << endl;
    */

    string illumina_quality = quality_to_illumina(read->quality);

    if (read->flags & Read::Flag_Reversed)
    {
        string temp;
        copy(illumina_quality.rbegin(), illumina_quality.rend(), back_inserter(temp));
        illumina_quality = temp;
    }

    // sanity check: Illumina encoding of quality scores
    for (string::const_iterator it=illumina_quality.begin(); it!=illumina_quality.end(); ++it)
    {
        if (*it < 0 || *it >= int(score_count_))
        {
            cout << read << endl;
            throw runtime_error("[qual_hist] I am insane!");
        }
    }

    for (size_t i=0; i<read->sequence.size(); ++i)
    {
        size_t bin = int(double(histogram_.size()) * i / read->sequence.size());
        ++histogram_[bin][illumina_quality[i]];

        if (!polymorphic[i])
        {
            if (read->sequence[i] == refseq[i])
                ++count_monomorphic_correct_[illumina_quality[i]];
            else
                ++count_monomorphic_incorrect_[illumina_quality[i]];
        }
    }

    return 0;
}


double proportion_incorrect(size_t count_correct, size_t count_incorrect)
{
    return double(count_incorrect)/(count_correct + count_incorrect);
}


void QualHistogram::report()
{
    cout << "# read count: " << read_count_ << endl;
    cout << "#\n";
    cout << "# count_monorphic_correct: ";
    copy(count_monomorphic_correct_.begin(), count_monomorphic_correct_.end(), ostream_iterator<size_t>(cout, " "));
    cout << endl;
    cout << "# count_monorphic_incorrect: ";
    copy(count_monomorphic_incorrect_.begin(), count_monomorphic_incorrect_.end(), ostream_iterator<size_t>(cout, " "));
    cout << endl;
    cout << "# proportion incorrect: ";
    transform(count_monomorphic_correct_.begin(), count_monomorphic_correct_.end(), count_monomorphic_incorrect_.begin(),
              ostream_iterator<double>(cout, " "), proportion_incorrect);
    cout << endl;
    cout << "#\n";

    for (size_t i=0; i<score_count_; ++i)
    {
        for (size_t j=0; j<histogram_.size(); ++j)
            cout << histogram_[j][i] << " ";
        cout << endl;
    }
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 5)
        {
            cout << "qual_hist\n";
            cout << "Generates a quality score per-position histogram from a BAM file.\n";
            cout << endl;
            cout << "Usage: qual_hist bam_file region refseq snpfile\n";
            return 1;
        }

        const char* filename_bam = argv[1];
        Region region(argv[2]);
        const char* filename_refseq = argv[3];
        const char* filename_snps = argv[4];
        
        Region extended_region(region.id, region.begin - 200, region.end + 200);
        HaplotypeReference hapref(filename_refseq, filename_snps, extended_region);

        BAMFile bam(filename_bam);
        QualHistogram qual_hist(hapref);

        bam.process_reads(region, qual_hist);
        qual_hist.report();


       
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

