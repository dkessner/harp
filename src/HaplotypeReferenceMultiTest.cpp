//
// HaplotypeReferenceMultiTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeReferenceMulti.hpp"
#include "unit.hpp"
#include <iostream>
#include <sstream>


using namespace std;


void test_mock_aux(const HaplotypeReferenceMulti& hapref)
{
    /*
    cout << "haplotypes: " << hapref.haplotype_count() << endl;
    for (size_t i=0; i<hapref.haplotype_count(); ++i)
        cout << i << " " << hapref.id(i) << endl;
    */

    unit_assert(hapref.haplotype_count() == 21);
    unit_assert(hapref.id(0) == "Acinetobacter baumannii");
    unit_assert(hapref.id(20) == "Streptococcus pneumoniae");
    unit_assert(hapref.sequence_length(0) == 1539);
    unit_assert(hapref.full_sequence(0, 0, 100) == "TTTAACTGAAGAGTTTGATCATGGCTCAGATTGAACGCTGGCGGCAGGCTTAACACATGCAAGTCGAGCGGGGGAAGGTAGCTTGCTACTGGACCTAGCG");
    unit_assert(hapref.sequence_length(20) == 1543);
    unit_assert(hapref.full_sequence(20, 400, 420) == "GCGTGAGTGAAGAAGGTTTT");
}


void test_mock()
{
    HaplotypeReferenceMulti hapref("test_files/mock.fasta");
    test_mock_aux(hapref);
}


void test_mock_vector_constructor()
{
    vector<string> filenames;
    for (size_t i=0; i<21; ++i)
    {
        ostringstream filename;
        filename << "test_files/mock.fasta.separate/ref" << i << ".fasta";
        filenames.push_back(filename.str());
    }

    HaplotypeReferenceMulti hapref(filenames);
    test_mock_aux(hapref);
}


int main()
{
    try
    {
        test_mock();
        test_mock_vector_constructor();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

