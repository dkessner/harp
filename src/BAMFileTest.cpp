//
// BAMFileTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BAMFile.hpp"
#include "unit.hpp"
#include "boost/lambda/lambda.hpp"
#include <iostream>
#include <vector>
#include <iterator>


using namespace std;
using namespace bampp;
using namespace boost::lambda; // _1


void test_translate()
{
    BAMFile bf("test_files/test.bam");
    unit_assert(bf.reference_names().size() == 15);
    unit_assert(bf.reference_names()[2] == "2L");

    ReadCollector collector;
    bf.process_reads(Region("2L:22188000-22188100"), collector);

    unit_assert(collector.reads.size() == 1);
    const Read& read = *collector.reads[0];

    // cout << read << endl;

    unit_assert(read.name == "HWI-ST611_0170:2:1:3014:2433#0");
    unit_assert(read.flags == 163);
    unit_assert(read.reference_name == "2L");
    unit_assert(read.position == 22188066);
    unit_assert(read.mapping_quality == 29);
    unit_assert(read.cigar.size() == 2);
    ostringstream cigar_string;
    cigar_string << read.cigar;
    unit_assert(cigar_string.str() == "74M26S");
    unit_assert(read.next_reference_name == "2L");
    unit_assert(read.next_position == 22188220);
    unit_assert(read.template_length == 254);
    unit_assert(read.sequence == "TATCATGCAATTGAGCTTTTTTGGGGTTTTGTTTGGCGATGACGTTGGCAACCTCGGTGGGGGTGATCGGCAGGGGCGTCTGTTGAGATTTGGGGGCAGA");
    unit_assert(quality_to_ascii(read.quality) == "U__[`WVWMV]][U__c``cffda]_Q^Y^a^[RYU^]_]ee_cc_cWc`_Y]_[__M_BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");

    string illumina_quality = quality_to_illumina(read.quality);
    unit_assert(illumina_quality[0] == 21);
    unit_assert(illumina_quality[1] == 31);
    unit_assert(illumina_quality[2] == 31);
    unit_assert(illumina_quality[58] == 31);
    for (size_t i=59; i<100; ++i)
        unit_assert(illumina_quality[i] == 2); // "B"
}


void test_iterate()
{
    BAMFile bf("test_files/test.bam");
    ReadCollector collector;
    bf.process_reads(Region("2L:22000001-23000000"), collector);

    // transform(collector.reads.begin(), collector.reads.end(), ostream_iterator<Read>(cout, "\n\n"), *_1);

    unit_assert(collector.reads.size() == 4);
    unit_assert(collector.reads[0]->position == 22188066);
    unit_assert(collector.reads[1]->position == 22188220);
    unit_assert(collector.reads[2]->position == 22250972);
    unit_assert(collector.reads[3]->position == 22251110);
}


struct PairCollector : public PairProcessor
{
    typedef vector< pair<shared_ptr<Read>, shared_ptr<Read> > > Pairs;
    Pairs pairs;
    vector< shared_ptr<Read> > unmatched_reads;

    virtual void process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2)
    {
        if (r2.get())
            pairs.push_back(make_pair(r1, r2));
        else
            unmatched_reads.push_back(r1);
    }
};


void test_iterate_pairs()
{
    BAMFile bf("test_files/test_2L.bam");
    Region region("2L:15000001-15000200"); 
    PairCollector collector;

    iterate_pairs(bf, region, collector);

    unit_assert(collector.pairs.size() == 213);
    unit_assert(collector.pairs.size()*2 + collector.unmatched_reads.size() == 755);

    for (PairCollector::Pairs::const_iterator it=collector.pairs.begin(); it!=collector.pairs.end(); ++it)
    {
        //cout << it->first->name << " " << it->first->position << endl;
        //cout << it->second->name << " " << it->second->position << endl;
        //cout << endl;

        unit_assert(it->first->name == it->second->name);
        unit_assert(it->first->position <= it->second->position);
    }
}


void test_iterate_all()
{
    ReadCollector collector;
    iterate_all("test_files/test_2L.bam", collector);
    unit_assert(collector.reads.size() == 2285);

    collector.reads.clear();
    iterate_all("test_files/test.bam", collector);
    unit_assert(collector.reads.size() == 984);
}


int main()
{
    try
    {
        test_translate();
        test_iterate();
        test_iterate_pairs();
        test_iterate_all();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

