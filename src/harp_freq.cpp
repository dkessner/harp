//
// harp_freq.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "harp_misc.hpp"
#include "Region.hpp"
#include "HaplotypeLikelihoodRecord.hpp"
#include "HaplotypeReference.hpp"
#include "HaplotypeFrequencyEstimation.hpp"
#include "StandardErrorEstimation.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include <iostream>
#include <iomanip>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
namespace bfs = boost::filesystem;


namespace {


struct HaplotypeFrequencyEntry
{
    size_t haplotype; // index
    double frequency;
    string sequence;

    HaplotypeFrequencyEntry(size_t _haplotype, double _frequency, const string& _sequence)
    :   haplotype(_haplotype), frequency(_frequency), sequence(_sequence)
    {}
};


ostream& operator<<(ostream& os, const HaplotypeFrequencyEntry& entry)
{
    os << setw(3) << entry.haplotype << setw(12) << entry.frequency << " " << entry.sequence;
    return os;
}


bool has_greater_frequency(const HaplotypeFrequencyEntry& a, const HaplotypeFrequencyEntry& b)
{
    return a.frequency > b.frequency;
}


void report_haplotype_frequencies(const bfs::path& outdir, 
                                  const vector<double>& haplotype_frequencies, 
                                  HaplotypeReference& hapref,
                                  size_t position_begin, size_t position_end)
{
    HaplotypeReference::SNPTable::const_iterator snp_table_begin = hapref.snp_table.lower_bound(position_begin);
    HaplotypeReference::SNPTable::const_iterator snp_table_end = hapref.snp_table.lower_bound(position_end);

    vector<size_t> positions;
    for (HaplotypeReference::SNPTable::const_iterator it=snp_table_begin; it!=snp_table_end; ++it)
        positions.push_back(it->first);

    vector<HaplotypeFrequencyEntry> haplotype_frequency_entries;

    assert(haplotype_frequencies.size()+1 == hapref.haplotype_count() || hapref.haplotype_count() == 0);

    for (size_t i=0; i<haplotype_frequencies.size(); ++i)
        haplotype_frequency_entries.push_back(HaplotypeFrequencyEntry(i, haplotype_frequencies[i], hapref.variant_sequence(i+1, positions)));

    bfs::ofstream os(outdir / "haplotype_frequencies.txt");
    if (!positions.empty())
        os << "# variant site count: " << positions.size() << endl;
    copy(haplotype_frequency_entries.begin(), haplotype_frequency_entries.end(), ostream_iterator<HaplotypeFrequencyEntry>(os, "\n"));

    sort(haplotype_frequency_entries.begin(), haplotype_frequency_entries.end(), has_greater_frequency); // inefficient copies

    bfs::ofstream os_sorted(outdir / "haplotype_frequencies_sorted.txt");
    if (!positions.empty())
        os_sorted << "# variant site count: " << positions.size() << endl;
    copy(haplotype_frequency_entries.begin(), haplotype_frequency_entries.end(), ostream_iterator<HaplotypeFrequencyEntry>(os_sorted, "\n"));
}


int go(const harp_config::Config& config)
{
    if (config.dirname_output.empty())
        throw runtime_error("[harp_freq] Empty dirname_output.");

    bfs::path outdir = bfs::path(config.dirname_output) / "freq";
    bfs::create_directories(outdir);

    bfs::ofstream os_config(outdir / "config.txt");
    os_config << config;
    os_config.close();

    HaplotypeLikelihoodRecords records(config.filename_hlk);
    if (records.empty()) throw runtime_error("[harp_freq] No records.");

    // get region from records if not specified

    Region region = config.region;
    if (region.id.empty())
    {
        region.id = "unknown";
        region.begin = records.front()->read1_begin;
        region.end = records.back()->read1_begin + 1;
    }

    size_t window_width = config.window_width;
    size_t window_step_size = config.window_step_size;
    if (window_step_size == 0) window_step_size = region.length();
    if (window_width == 0) window_width = window_step_size;

    shared_ptr<HaplotypeReference> hapref(
        new HaplotypeReference(config.filename_refseq,
                               config.filename_snps,
                               region));
   
    bfs::ofstream os_freqs_all(config.filename_freqs);
    bfs::ofstream os_se_all;
    if (config.compute_standard_errors) os_se_all.open(config.filename_freqs + ".se");

    for (size_t position=region.begin; position+window_width<=region.end; position+=window_step_size)
    {
        size_t position_end = position + window_width;

        // subdirectory for output files
    
        Region window(region.id, position, position_end);
        bfs::path subdir = outdir / window.filetag();
        bfs::create_directories(subdir);

        // frequency estimation

        cout << "[harp_freq] Estimating haplotype frequencies: " << window << endl;

        bfs::ofstream os_em(subdir / "em.txt");

        using HaplotypeFrequencyEstimation::estimate_haplotype_frequencies;
        vector<double> f = estimate_haplotype_frequencies(records, 
                                                          position, 
                                                          position_end, 
                                                          config.freq_config,
                                                          &os_em);
        os_em.close();

        // report

        os_freqs_all << region.id << " " << position << " " << position_end << " ";
        copy(f.begin(), f.end(), ostream_iterator<double>(os_freqs_all, " "));
        os_freqs_all << endl;

        report_haplotype_frequencies(subdir, f, *hapref, position, position_end);

        // standard error calculation and reporting
    
        if (config.compute_standard_errors)
        {
            cout << "[harp_freq] Computing standard errors\n";
            using StandardErrorEstimation::estimate_standard_errors;
            vector<double> standard_errors = estimate_standard_errors(records, position, position_end, f);

            os_se_all << region.id << " " << position << " " << position_end << " ";
            copy(standard_errors.begin(), standard_errors.end(), ostream_iterator<double>(os_se_all, " "));
            os_se_all << endl;
        }
    }

    return 0;
}


int harp_freq(const harp_config::Config& config)
{
    if (config.filename_hlk.empty())
        throw runtime_error("[harp_freq] No haplotype likelihood filename (.hlk) specified.");

    return go(config);
}


string harp_freq_info()
{
    return "estimate haplotype frequencies";
}


string harp_freq_usage()
{
    ostringstream oss;
    oss << "harp freq required parameters:\n"
        << "  hlk\n"
        << endl
        << "harp freq optional parameters:\n"
        << "  refseq, snps  (for reporting haplotype variant sequences)\n"
        << "  region\n"
        << "  window_step, window_width\n"
        << "  em_iter\n"
        << "  haplotype_filter\n";
    return oss.str();
}


} // namespace


harp_config::JumpTableEntry entry_harp_freq_(harp_freq, harp_freq_info, harp_freq_usage);


