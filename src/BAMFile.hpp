//
// BAMFile.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _BAMFILE_HPP_
#define _BAMFILE_HPP_


#include "Region.hpp"
#include <tr1/memory>
#include <string>
#include <vector>
#include <map>


namespace bampp {


using std::tr1::shared_ptr;


struct Read
{
    struct CIGAR
    {
        char op;
        unsigned int count;
    };

    std::string name;
    unsigned int flags;
    std::string reference_name;
    size_t position;
    int mapping_quality;
    std::vector<CIGAR> cigar;
    std::string next_reference_name;
    size_t next_position;
    int template_length;
    std::string sequence;
    std::string quality;

    enum Flag
    {
        Flag_MultipleFragments = 0x1,
        Flag_AllAligned = 0x2,
        Flag_Unmapped = 0x4,
        Flag_NextUnmapped = 0x8,
        Flag_Reversed = 0x10,
        Flag_NextReversed = 0x20,
        Flag_FirstInTemplate = 0x40,
        Flag_LastInTemplate = 0x80,
        Flag_SecondaryAlignment = 0x100,
        Flag_NotPassingQC = 0x200,
        Flag_Duplicate = 0x400
    };
};


std::string quality_to_ascii(const std::string& quality); // SAM: ascii == quality + 33


//
// Illumina scores in [0,40], encoded in ASCII [64,104], 
// which samtools shifts by 33 when converting to BAM: [31,71]
// According to wikipedia (http://en.wikipedia.org/wiki/FASTQ_format):
//   - Illumina scores may go up to 41
//   - scores 0,1 not used;  score 2 "B" indicates sequence that should not be used in downstream analysis
//
std::string quality_to_illumina(const std::string& quality); // illumina_quality = ascii - 64 = quality - 31


double illumina_error_probability(char q); // returns P(error | q), where q in [0,40]


std::ostream& operator<<(std::ostream& os, const Read::CIGAR& cigar);
std::ostream& operator<<(std::ostream& os, const std::vector<Read::CIGAR>& cigar); // write CIGAR string
std::ostream& operator<<(std::ostream& os, const Read& read); // write SAM format


class ReadProcessor
{
    public:
    virtual int process_read(shared_ptr<Read> read) = 0;
    virtual ~ReadProcessor(){}
};


class BAMFile
{
    public:

    BAMFile(const std::string& filename);
    const std::vector<std::string>& reference_names() const;
    void process_reads(const Region& region, ReadProcessor& processor) const;

    private:
    struct Impl;
    shared_ptr<Impl> impl_;
};


//
// convenience classes
//


struct ReadCollector : public ReadProcessor
{
    std::vector< shared_ptr<Read> > reads;

    virtual int process_read(shared_ptr<Read> read)
    {
        reads.push_back(read);
        return 0;
    }
};


class PairProcessor
{
    public:
    virtual void process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2) = 0;
    virtual ~PairProcessor(){}
};


// iterate through read pairs first, then unmatched reads
void iterate_pairs(const BAMFile& bf, const Region& region, PairProcessor& processor);


// iterate through all reads in a BAM file
void iterate_all(const std::string& filename_bam, ReadProcessor& processor);


} // namespace bampp


#endif // _BAMFILE_HPP_

