//
// RegionTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "Region.hpp"
#include "unit.hpp"
#include <iostream>


using namespace std;


void test()
{
    Region region("X", 0, 100); // 0-based, iterator semantics
    unit_assert(region.str() == "X:1-100"); // 1-based, inclusive

    Region region2("Hello:201-300");   // 1-based, inclusive
    unit_assert(region2.id == "Hello");
    unit_assert(region2.begin == 200); // 0-based
    unit_assert(region2.end == 300);   // iterator semantics
    unit_assert(region2.length() == 100);
}


void test_intersection()
{
    Region r1("A", 100, 200);
    Region r2("A", 150, 2000);
    Region r3 = r1 & r2;
    unit_assert(r3.id == "A" && r3.begin == 150 && r3.end == 200);

    Region r4("B", 150, 2000);
    Region r5 = r1 & r4; 
    unit_assert(r5.id == "" && r5.begin == 0 && r5.end == 0);

    Region r6("A", 2000, 3000);
    Region r7 = r1 & r6; 
    unit_assert(r7.id == "" && r7.begin == 0 && r7.end == 0);

    Region r8("A", 0, 2000);
    Region r9("A", 100, 200);
    Region r10 = r8 & r9;
    unit_assert(r10.id == "A" && r10.begin == 100 && r10.end == 200);
}


int main()
{
    try
    {
        test();
        test_intersection();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

