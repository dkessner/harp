//
// HaplotypeFrequencyEstimation.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeFrequencyEstimation.hpp"
#include "harp_misc.hpp"
#include "boost/lambda/lambda.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/numeric/ublas/vector.hpp"
#include <iostream>
#include <cmath>
#include <numeric>


using namespace std;
using tr1::shared_ptr;
using namespace boost::lambda;
namespace ublas = boost::numeric::ublas;


namespace {


typedef vector< ublas::vector<double> > LikelihoodVectors;


void apply_haplotype_filter(LikelihoodVectors& likelihood_vectors, const vector<bool>& haplotype_filter)
{
    ublas::vector<double> filter(haplotype_filter.size() - 1); // remove Ref/0
    copy(haplotype_filter.begin() + 1, haplotype_filter.end(), filter.begin());

    for (LikelihoodVectors::iterator it=likelihood_vectors.begin(); it!=likelihood_vectors.end(); ++it)
        *it = element_prod(*it, filter);
}


ublas::vector<double> shift_unlog(shared_ptr<const HaplotypeLikelihoodRecord> record)
{
    vector<double> pair_logl = record->pair_logl();
    ublas::vector<double> result(pair_logl.size() - 1); // remove Ref (haplotype 0)
    transform(pair_logl.begin() + 1, pair_logl.end(), result.begin(), (double (*)(double))exp);
    return result;
}

/*
void report_haplotype(ostream& os, const HaplotypeLikelihoodRecord& record, const vector<double>& posterior)
{
    if (!os) return;

    os << "[" << record.read1_begin << "," << record.read1_end << ") "
       << "[" << record.read2_begin << "," << record.read2_end << ")";

    for (vector<double>::const_iterator it=posterior.begin(); it!=posterior.end(); ++it)
        if (*it > .05)
            os << " " << it-posterior.begin() << ":" << *it;
    os << endl;
}
*/


struct GreaterValue
{
    bool operator()(const pair<size_t,double>& a, const pair<size_t,double>& b) const
    {
        return a.second > b.second;
    }
};


vector< pair<size_t, double> > sorted_with_index(const ublas::vector<double>& v)
{
    vector< pair<size_t, double> > result(v.size());

    for (size_t i=0; i<v.size(); ++i)
        result[i] = make_pair(i, v[i]);

    sort(result.begin(), result.end(), GreaterValue());

    return result;
}


void cutoff_low_frequencies(ublas::vector<double>& f, double minimum_frequency_cutoff)
{
    for (ublas::vector<double>::iterator it=f.begin(); it!=f.end(); ++it)
        if (*it < minimum_frequency_cutoff)
            *it = 0;
   
    f /= sum(f);
}


double do_em(ublas::vector<double>& f, 
             const LikelihoodVectors& likelihood_vectors, 
             const HaplotypeFrequencyEstimation::Config& config,
             ostream* debug_log)
{
    size_t n = f.size(); // # of haplotypes
    size_t read_count = likelihood_vectors.size();
    double logl = 0.;

    for (size_t iteration=0; iteration<config.max_iteration_count; ++iteration)
    {
        double sum_log_marginal_likelihood = 0.;
        ublas::vector<double> sum_posterior = ublas::zero_vector<double>(n);

        // iterate over reads r 
        // likelihood vector l = < P(r|h) : h >

        for (LikelihoodVectors::const_iterator l=likelihood_vectors.begin(); l!=likelihood_vectors.end(); ++l)
        {
            ublas::vector<double> joint = element_prod(*l, f);  // < P(r,h|f) : h >
            double marginal_likelihood = sum(joint);            // P(r|f)

            sum_log_marginal_likelihood += log(marginal_likelihood);

            ublas::vector<double> posterior = joint / marginal_likelihood;
            sum_posterior += posterior;
        }

        ublas::vector<double> f_new = sum_posterior / read_count; // new estimate

        if (config.minimum_frequency_cutoff > 0.)
            cutoff_low_frequencies(f_new, config.minimum_frequency_cutoff);

        ublas::vector<double> diff = f - f_new;
        double sum_squared_diff = inner_prod(diff, diff); // equiv: pow(norm_2(f-f_new), 2);
        f = f_new;

        if (debug_log)
        {
            *debug_log << "log_likelihood: " << sum_log_marginal_likelihood << endl;
            *debug_log << "sum_squared_diff: " << sum_squared_diff << endl;
            *debug_log << "estimate:\n";
            copy(f.begin(), f.end(), ostream_iterator<double>(*debug_log, " "));
            *debug_log << endl;

            *debug_log << "estimate sorted:\n";
            vector< pair<size_t, double> > f_sorted = sorted_with_index(f);
            for (vector< pair<size_t,double> >::const_iterator it=f_sorted.begin(); it!=f_sorted.end(); ++it)
                *debug_log << it->first << ":" << it->second << " ";
            *debug_log << endl;

            *debug_log << endl;
        }

        if (sum_squared_diff < config.convergence_threshold)
        {
            if (debug_log)
                *debug_log << "reached convergence threshold " << config.convergence_threshold << endl
                           << "iteration count: " << iteration << endl;
            break;
        }

        logl = sum_log_marginal_likelihood;
    }

    return logl;
}


} // namespace


namespace HaplotypeFrequencyEstimation {


vector<double> estimate_haplotype_frequencies(const HaplotypeLikelihoodRecords& records,
                                                   size_t position_begin,
                                                   size_t position_end,
                                                   const Config& config,
                                                   ostream* debug_log)
{
    HaplotypeLikelihoodRecords::const_iterator begin = records.lower_bound(position_begin);
    HaplotypeLikelihoodRecords::const_iterator end = records.lower_bound(position_end);
    size_t read_count = end - begin;

    if (debug_log)
    {
        *debug_log << "position_begin: " << position_begin << endl
                   << "position_end: " << position_end << endl
                   << "read_count: " << read_count << endl
                   << endl;
    }

    LikelihoodVectors likelihood_vectors(read_count);
    transform(begin, end, likelihood_vectors.begin(), shift_unlog);

    if (!config.haplotype_filter.empty()) 
    {
        vector<bool> filter = harp_misc::parse_haplotype_filter_string(records.haplotype_count, config.haplotype_filter);
        apply_haplotype_filter(likelihood_vectors, filter);
    }

    size_t n = records.haplotype_count - 1;
    ublas::vector<double> f(n, 1./n); // initial estimate: uniform distribution over haplotypes h

    vector<double> result(f.size());
    size_t em_run_best = 0;
    double logl_best = -numeric_limits<double>::infinity();

    harp_misc::SymmetricDirichlet random_start(config.random_start_alpha, n, config.random_start_seed);

    for (size_t em_run=0; em_run<=config.random_start_count; ++em_run)
    {
        if (em_run > 0) f = random_start(); // em_run==0 starts from uniform

        if (debug_log)
        {
            *debug_log << "em_run: " << em_run << endl << "initial_estimate: ";
            copy(f.begin(), f.end(), ostream_iterator<double>(*debug_log, " "));
            *debug_log << endl << endl;
        }

        double logl = do_em(f, likelihood_vectors, config, debug_log);
        if (logl > logl_best)
        {
            em_run_best = em_run;
            logl_best = logl;
            copy(f.begin(), f.end(), result.begin());
        }

        if (debug_log)
        {
            *debug_log << "em_run: " << em_run << endl
                       << "logl: " << logl << endl
                       << "estimate: ";
            copy(f.begin(), f.end(), ostream_iterator<double>(*debug_log, " "));
            *debug_log << endl << endl;
        }
    }

    if (debug_log)
    {
        *debug_log << "em_run_best: " << em_run_best << endl
                   << "logl_best: " << logl_best << endl
                   << "estimate_best: ";
        copy(result.begin(), result.end(), ostream_iterator<double>(*debug_log, " "));
        *debug_log << endl << endl;
    }

    return result;
}


} // namespace HaplotypeFrequencyEstimation 



