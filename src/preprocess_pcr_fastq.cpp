//
// preprocess_pcr_fastq.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BAMFile.hpp"
#include "HaplotypeReference.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/lambda/lambda.hpp"
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <vector>
#include <cmath>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
namespace bfs = boost::filesystem;
using namespace boost::lambda; // for _1


vector<string> read_refseq(const string& filename)
{
    ifstream is(filename.c_str());
    if (!is)
        throw runtime_error(("[read_refseq] Couldn't open file " + filename).c_str());
    
    string buffer;

    getline(is, buffer);
    istringstream iss(buffer);
    char dummy;
    string id;
    iss >> dummy >> id;

    getline(is, buffer);
    istringstream iss2(buffer);
    string sequence;
    iss2 >> sequence;

    vector<string> result;
    result.push_back(id);
    result.push_back(sequence);
    return result;
}


vector<string> read_primers(const string& filename)
{
    ifstream is(filename.c_str());
    if (!is)
        throw runtime_error(("[read_primers] Couldn't open file " + filename).c_str());

    vector<string> primers;
    copy(istream_iterator<string>(is), istream_iterator<string>(), back_inserter(primers));
    return primers;
}


struct FastqRecord
{
    string id;
    string sequence;
    string quality;
};


istream& operator>>(istream& is, FastqRecord& record)
{
    string id_1, sequence, id_2, quality;
    getline(is, id_1);
    getline(is, sequence);
    getline(is, id_2);
    getline(is, quality);
    if (!is) return is;

    istringstream iss_id(id_1);
    char dummy_at;
    iss_id >> dummy_at >> record.id;
    if (dummy_at != '@')
        throw runtime_error("[preprocess_pcr_fastq] Bad fastq");

    istringstream iss_sequence(sequence);
    iss_sequence >> record.sequence;

    istringstream iss_quality(quality);
    iss_quality >> record.quality;

    return is;
}


int count_mismatches(const string& a, const string& b)
{
    int result = 0;

    for (string::const_iterator it=a.begin(), jt=b.begin(); it!=a.end() && jt!=b.end(); ++it, ++jt)
        if (*it != *jt) ++result;

    return result;
}


int count_mismatches(string::const_iterator a_begin, string::const_iterator a_end, string::const_iterator b_begin)
{
    int result = 0;

    for (; a_begin!=a_end; ++a_begin, ++b_begin)
        if (*a_begin != *b_begin) ++result;

    return result;
}


struct Complement
{
    char operator()(char c)
    {
        switch (c)
        {
            case 'A': return 'T';
            case 'T': return 'A';
            case 'C': return 'G';
            case 'G': return 'C';
            case 'N': return 'N';
            default: 
            {
                ostringstream oss;
                oss << "Error in Complement: " << c;
                throw runtime_error(oss.str().c_str());
            }
        }
    }
};


string reverse_complement(const string& s)
{
    string r(s.size(), '\0');
    transform(s.begin(), s.end(), r.rbegin(), Complement());
    return r;
}


size_t find_first_indel(const string& sequence, const string& refseq)
{
    const size_t buffer_size = 10;
    const int max_mismatches = 5;

    size_t position_end = min(sequence.size(), refseq.size()) - buffer_size;

    for (size_t position=0; position<position_end; ++position)
    {
        int mismatches = count_mismatches(sequence.begin() + position, 
                                          sequence.begin() + position + buffer_size, 
                                          refseq.begin() + position);

        if (mismatches > max_mismatches)
            return position + buffer_size - max_mismatches - 1;
    }

    return min(sequence.size(), refseq.size());
}


class ReadProcessor
{
    public:

    ReadProcessor(const string& refseq_id, const string& refseq_sequence, 
                  const string& primer1, const string& primer2, const string& filename_outdir)
    :   refseq_id_(refseq_id), 
        refseq_(refseq_sequence), 
        refseq_reverse_(reverse_complement(refseq_sequence)),
        primer1_(primer1), 
        primer2_(primer2),
        outdir_(filename_outdir),
        os_log_(outdir_ / "log.txt"),
        os_filtered_(outdir_ / "filtered.txt"),
        os_reads_trimmed_(outdir_ / "reads_trimmed_aligned.txt"),
        os_sam_(outdir_ / "reads.sam"),
        read_count_(0),
        base_quality_histogram_(41), 
        trimmed_read_length_histogram_(refseq_sequence.size() + 1)
    {
        os_log_ << "refseq:\n" << refseq_ << "\n\n"; 
        os_log_ << "primers:\n" << primer1_ << endl << primer2_ << "\n\n";

        os_sam_ << "@SQ\tSN:" << refseq_id << "\tLN:" << refseq_sequence.size() << "\n"
                   << "@PG\tID:preprocess_pcr_fastq\tPN:preprocess_pcr_fastq VN:1.0\n";
    }

    void process_file(const string& filename);
    void process_read(const FastqRecord& record);

    private:

    string refseq_id_;
    string refseq_;
    string refseq_reverse_;
    string primer1_;
    string primer2_;

    bfs::path outdir_;
    bfs::ofstream os_log_;
    bfs::ofstream os_filtered_;
    bfs::ofstream os_reads_trimmed_;
    bfs::ofstream os_sam_;

    size_t read_count_;
    vector<size_t> base_quality_histogram_;
    vector<size_t> trimmed_read_length_histogram_;

    bool is_forward(const string& sequence);
    size_t find_start(const string& sequence, bool forward);
};


bool ReadProcessor::is_forward(const string& sequence)
{
    int mismatches_primer1 = count_mismatches(sequence, primer1_);
    int mismatches_primer2 = count_mismatches(sequence, primer2_);
    return mismatches_primer1 < mismatches_primer2;
}


size_t ReadProcessor::find_start(const string& sequence, bool forward)
{
    const size_t prefix_length = 10;
    const int max_mismatches = 2;
    const size_t search_radius = 5;

    const string& primer = forward ? primer1_ : primer2_;
    const string& refseq = forward ? refseq_ : refseq_reverse_;

    const size_t search_begin = primer.size() - search_radius;
    const size_t search_end = primer.size() + search_radius;

    if (search_end + prefix_length > sequence.size())
        return string::npos;

    for (size_t i=search_begin; i<search_end; ++i)
    {
        int mismatches = count_mismatches(sequence.substr(i, prefix_length), refseq);
        if (mismatches < max_mismatches)
            return i;
    }

    return string::npos; 
}


void ReadProcessor::process_read(const FastqRecord& record)
{
    ++read_count_;

    bool forward = is_forward(record.sequence);
    string which = forward ? "forward" : "reverse";

    // trim primer from front of read

    size_t index_start = find_start(record.sequence, forward);

    if (index_start == string::npos)
    {
        os_filtered_ << "read " << read_count_ << " " << record.id << " "
                     << "refseq start not found\n";
        return;
    }

    const string& refseq = forward ? refseq_ : refseq_reverse_;
    string sequence_no_primer = record.sequence.substr(index_start); // copy
    string quality_no_primer = record.quality.substr(index_start);   // copy
    
    // trim end of read starting at first indel

    size_t index_first_indel = find_first_indel(sequence_no_primer, refseq);
    string sequence_trimmed = sequence_no_primer.substr(0, index_first_indel);
    string quality_trimmed = quality_no_primer.substr(0, index_first_indel);
    size_t sequence_trimmed_position = forward ? 0 : refseq_.size() - sequence_trimmed.size();
    if (!forward) sequence_trimmed = reverse_complement(sequence_trimmed);

    os_reads_trimmed_ << string(sequence_trimmed_position, ' ') << sequence_trimmed << endl;

    // write SAM record

    bampp::Read read;
    read.name = record.id;
    read.flags = 0;
    read.reference_name = read.next_reference_name = refseq_id_;
    read.position = sequence_trimmed_position;
    read.mapping_quality = 25;

    bampp::Read::CIGAR cigar;
    cigar.op = 'M';
    cigar.count = sequence_trimmed.size();
    read.cigar.push_back(cigar);

    read.next_position = 0;
    read.template_length = 0;
    read.sequence = sequence_trimmed;

    string quality_binary(quality_trimmed.size(), '\0');
    transform(quality_trimmed.begin(), quality_trimmed.end(), quality_binary.begin(), _1 - 33);
    read.quality = quality_binary;

    os_sam_ << read << endl;

    // update counts

    for (size_t position=0; position<record.quality.size(); ++position)
    {
        size_t score = record.quality[position] - 33;
        ++base_quality_histogram_[score];
    }

    ++trimmed_read_length_histogram_[sequence_trimmed.size()];
}


void ReadProcessor::process_file(const string& filename)
{
    cout << "Processing file " << filename << endl;

    ifstream is(filename.c_str());
    if (!is) throw runtime_error(filename + " not found.");

    while (is)
    {
        FastqRecord record;
        is >> record;
        if (!is) break;

        process_read(record);
    }

    // write stuff to log

    os_log_ << "read count: " << read_count_ << "\n\n";

    os_log_ << "base quality score histogram:\n";
    for (size_t i=0; i<base_quality_histogram_.size(); ++i)
        os_log_ << i << " " << base_quality_histogram_[i] << endl;
    os_log_ << endl;

    os_log_ << "trimmed read length histogram:\n";
    size_t total = 0;
    for (size_t i=0; i<trimmed_read_length_histogram_.size(); ++i)
    {
        total += trimmed_read_length_histogram_[i];
        os_log_ << i << " " << trimmed_read_length_histogram_[i] << " " << total <<  endl;
    }
    os_log_ << endl;
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 5)
        {
            cout << "preprocess_pcr_fastq\n";
            cout << endl;
            cout << "Usage: preprocess_pcr_fastq reads.fastq refseq.fasta primers.txt outdir\n";
            cout << endl;
            return 1;
        }

        const char* filename_reads = argv[1];
        const char* filename_refseq = argv[2];
        const char* filename_primers = argv[3];
        const char* filename_outdir = argv[4];

        if (bfs::exists(filename_outdir))
            throw runtime_error((string("Output directory already exists: ") + filename_outdir).c_str());

        bfs::create_directories(filename_outdir);

        vector<string> id_refseq = read_refseq(filename_refseq);
        const string& refseq_id = id_refseq[0];
        const string& refseq_sequence = id_refseq[1];

        vector<string> primers = read_primers(filename_primers);
        if (primers.size() != 2)
            throw runtime_error("Exactly 2 primers must be specified.");

        ReadProcessor processor(refseq_id, refseq_sequence, primers[0], primers[1], filename_outdir);
        processor.process_file(filename_reads);
          
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

