//
// StandardErrorEstimationTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "StandardErrorEstimation.hpp"
#include "unit.hpp"
#include <iostream>
#include <iterator>
#include <limits>


using namespace std;
using tr1::shared_ptr;


struct TestData
{
    HaplotypeLikelihoodRecords hlk;
    vector<double> f;
    size_t position_begin;
    size_t position_end;
};


TestData create_test_data_simple()
{
    // 8 haplotypes: 4 good + 4 unused (interleaved)

    const size_t replicate_count = 100;
    const size_t good_haplotype_count = 4;
    const size_t haplotype_count = 2*good_haplotype_count;

    TestData test_data;

    test_data.hlk = HaplotypeLikelihoodRecords(1 + haplotype_count); // Ref/0 included in .hlk

    for (size_t i=0; i<replicate_count; ++i)
    {
        for (size_t j=0; j<good_haplotype_count; ++j)
        {
            size_t good_haplotype_index = 2*j + 1;
            HaplotypeLikelihoodRecordPtr record(new HaplotypeLikelihoodRecord);

            record->read1_logl = vector<double>(1 + haplotype_count, -numeric_limits<double>::infinity());
            record->read2_logl = vector<double>(1 + haplotype_count, -numeric_limits<double>::infinity());
            //record->read1_logl = vector<double>(1 + haplotype_count, log(1e-6));
            //record->read2_logl = vector<double>(1 + haplotype_count, log(1e-6));

            record->read1_logl[good_haplotype_index] = log(.5); // value doesn't matter, as long as it's <= 0
            record->read2_logl[good_haplotype_index] = log(.5);
            size_t dummy = i*good_haplotype_count + j;
            record->read1_begin = 100+dummy;
            record->read1_end = 200+dummy;
            record->read2_begin = 300+dummy;
            record->read2_end = 400+dummy;
            test_data.hlk.push_back(record);
        }
    }

    vector<double>& f = test_data.f;
    f.resize(haplotype_count);
    f[0] = f[2] = f[4] = f[6] = .25;

    test_data.position_begin = 0;
    test_data.position_end = 1000000;

    return test_data;
}


void test()
{
    TestData test_data = create_test_data_simple();

    /*
    for (HaplotypeLikelihoodRecords::const_iterator it=test_data.hlk.begin(); it!=test_data.hlk.end(); ++it)
        cout << **it << endl;

    copy(test_data.f.begin(), test_data.f.end(), ostream_iterator<double>(cout, " "));
    cout << endl;
    */

    vector<double> standard_errors = 
        StandardErrorEstimation::estimate_standard_errors(test_data.hlk, 
                                                          test_data.position_begin,
                                                          test_data.position_end,
                                                          test_data.f);
    
    /*
    cout << "returned standard_errors: ";
    copy(standard_errors.begin(), standard_errors.end(), ostream_iterator<double>(cout, " "));
    cout << endl;
    */

    const double epsilon = 1e-6;

    // regression test
    for (size_t i=0; i<standard_errors.size(); ++i)
    {
        if (i%2 == 0)
            unit_assert_equal(standard_errors[i], 0.0216506, epsilon);
        else
            unit_assert_equal(standard_errors[i], 0, epsilon);
    }
}


int main()
{
    try
    {
        test();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

