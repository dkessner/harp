//
// qual_hist_fastq.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BAMFile.hpp"
#include "HaplotypeReference.hpp"
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <vector>
#include <cmath>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;


static const size_t score_count_ = 42; // Illumina qual scores in [0,41]


class QualHistogram
{
    public:

    QualHistogram(size_t read_length, const string& output_filename_base)
    :   read_length_(read_length), 
        output_filename_base_(output_filename_base), 
        read_count_(0)
    {
        initialize_histogram();

        cout << "read_length: " << read_length << endl;
        cout << "output_filename_base: " << output_filename_base << endl;
    }

    void process_file(const string& filename);
    void report() const;

    double E_logl() const;
    double V_logl() const;

    private:

    size_t read_length_;
    const string output_filename_base_;

    size_t read_count_;

    typedef vector< vector<size_t> > Histogram; // [position][score]
    Histogram histogram_;

    void initialize_histogram();

    // log-likelihood distribution calculation

    double E_logl_per_site_conditional(size_t q) const;
    double E_logl2_per_site_conditional(size_t q) const;
    inline double P(size_t q, size_t position) const {return histogram_[position][q]/double(read_count_);}
    double E_logl_per_site(size_t position) const;
    double E_logl2_per_site(size_t position) const;
    double V_logl_per_site(size_t position) const;
};


void QualHistogram::initialize_histogram()
{
    histogram_.resize(read_length_);
    for (Histogram::iterator it=histogram_.begin(); it!=histogram_.end(); ++it)
        it->resize(score_count_); 
}


void QualHistogram::report() const
{
    // per-position quality score histogram

    string filename_qh = output_filename_base_ + ".qh";

    ofstream os_qh(filename_qh.c_str());
    if (!os_qh) throw runtime_error(("[QualHistogram::report()] Unable to open file " + filename_qh).c_str());

    cout << "Writing " << filename_qh << endl;

    os_qh << "# read count: " << read_count_ << endl;

    for (size_t i=0; i<score_count_; ++i)
    {
        for (size_t j=0; j<read_length_; ++j)
            os_qh << histogram_[j][i] << " "; // transpose: [score][position]
        os_qh << endl;
    }
    
    os_qh.close();

    // config parameters for haplotype likelihood distribution stats

    string filename_config = output_filename_base_ + ".config";

    ofstream os_config(filename_config.c_str());
    if (!os_config) throw runtime_error(("[QualHistogram::report()] Unable to open file " + filename_config).c_str());

    cout << "Writing " << filename_config << endl;

    os_config << "logl_min_zscore = -2\n";
    os_config << "logl_mean = " << E_logl() << endl;
    double v = V_logl();
    os_config << "logl_sd = " << sqrt(v) << endl;

    os_config.close();
}


void QualHistogram::process_file(const string& filename)
{
    cout << "Processing file " << filename << endl;

    ifstream is(filename.c_str());
    if (!is) throw runtime_error(filename + " not found.");

    while (is)
    {
        string id_1, sequence, id_2, quality;
        getline(is, id_1);
        getline(is, sequence);
        getline(is, id_2);
        getline(is, quality);
        if (!is) break;

        ++read_count_;

        for (size_t position=0; position<quality.size(); ++position)
        {
            size_t score = quality[position]-33;
            if (score>=score_count_) throw runtime_error("bad score");
            ++histogram_[position][score];
        }
    }
}


double QualHistogram::E_logl_per_site_conditional(size_t q) const // E[ logl(s_i) | q_i ]
{
    if (q==0 || q==1 || q==3) return 0;
    double e = bampp::illumina_error_probability(q);
    return (1-e)*log(1-e) + e*log(e/3);
}


double QualHistogram::E_logl2_per_site_conditional(size_t q) const // E[ logl(s_i)^2 | q_i ]
{
    if (q==0 || q==1 || q==3) return 0;
    double e = bampp::illumina_error_probability(q);
    return (1-e)*log(1-e)*log(1-e) + e*log(e/3)*log(e/3);
}


double QualHistogram::E_logl_per_site(size_t position) const
{
    double result = 0;
    for (size_t q=0; q<41; ++q)
        result += E_logl_per_site_conditional(q) * P(q, position);
    return result;
}


double QualHistogram::E_logl2_per_site(size_t position) const
{
    double result = 0;
    for (size_t q=0; q<41; ++q)
        result += E_logl2_per_site_conditional(q) * P(q, position);
    return result;
}


double QualHistogram::V_logl_per_site(size_t position) const
{
    double mu = E_logl_per_site(position);
    return E_logl2_per_site(position) - mu*mu;
}


double QualHistogram::E_logl() const
{
    double result = 0;
    for (size_t position=0; position<read_length_; ++position)
        result += E_logl_per_site(position);
    return result;
}


double QualHistogram::V_logl() const
{
    double result = 0;
    for (size_t position=0; position<read_length_; ++position)
        result += V_logl_per_site(position);
    return result;
}


// note: these calculations can be made more efficient by caching intermediate
// results, e.g. E_logl_per_site


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 4)
        {
            cout << "qual_hist_fastq\n";
            cout << "Generates a quality score per-position histogram from a fastq file.\n";
            cout << endl;
            cout << "Usage: qual_hist read_length file.fastq output_filename_base\n";
            cout << endl;
            cout << "Output:\n";
            cout << "    <output_filename_base>.qh     : per-position histogram of Illumina quality scores\n";
            cout << "    <output_filename_base>.config : configuration parameters for harp\n";
            return 1;
        }

        const size_t read_length = atoi(argv[1]);
        const char* filename = argv[2];
        const char* output_filename_base = argv[3];

        QualHistogram qual_hist(read_length, output_filename_base);
        qual_hist.process_file(filename);
        qual_hist.report();
           
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

