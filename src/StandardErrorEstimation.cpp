//
// StandardErrorEstimation.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "StandardErrorEstimation.hpp"
#include "boost/numeric/ublas/vector.hpp"
#include "boost/numeric/ublas/matrix.hpp"
#include "boost/numeric/ublas/matrix_proxy.hpp"
#include "boost/numeric/ublas/io.hpp"
#include <boost/numeric/ublas/lu.hpp>


using namespace std;
using tr1::shared_ptr;
namespace ublas = boost::numeric::ublas;


namespace {


template<class T>
void invert_matrix(const ublas::matrix<T>& input, ublas::matrix<T>& inverse) // copied from web and modified 
{
    using namespace boost::numeric::ublas;
    typedef permutation_matrix<size_t> pmatrix;
    // create a working copy of the input
    matrix<T> A(input);
    // create a permutation matrix for the LU-factorization
    pmatrix pm(A.size1());
    // perform LU-factorization
    int res = lu_factorize(A,pm);
    if( res != 0 ) throw runtime_error("[StandardErrorEstimation] Error in lu_factorize()");
    // create identity matrix of "inverse"
    inverse.assign(ublas::identity_matrix<T>(A.size1()));
    // backsubstitute to get the inverse
    lu_substitute(A, pm, inverse);
}


struct Impl
{
    size_t haplotype_count;
    ublas::vector<double> f;
    vector<size_t> nonzero_indices;

    typedef vector< ublas::vector<double> > LikelihoodVectors;
    LikelihoodVectors likelihood_vectors;

    ublas::vector<double> sum_p;
    ublas::matrix<double> sum_dp;

    ublas::vector<double> S;
    ublas::matrix<double> dS;

    vector<double> standard_errors;


    void extract_nonzero_frequencies(const vector<double>& f_in);

    void process_haplotype_likelihood_records(const HaplotypeLikelihoodRecords& records,
                                              size_t position_begin,
                                              size_t position_end);
    void compute_sums();
    void compute_score_and_information();
    void compute_standard_errors();
};


/*
ostream& operator<<(ostream& os, const Impl& impl)
{
    os << "haplotype_count: " << impl.haplotype_count << endl;

    os << "f: ";
    copy(impl.f.begin(), impl.f.end(), ostream_iterator<double>(os, " "));
    os << endl;

    os << "nonzero_indices: ";
    copy(impl.nonzero_indices.begin(), impl.nonzero_indices.end(), ostream_iterator<size_t>(os, " "));
    os << endl;

//    os << "likelihoods:\n";
//    copy(impl.likelihood_vectors.begin(), impl.likelihood_vectors.end(), ostream_iterator< ublas::vector<double> >(os, "\n"));
//    os << endl;

    os << "sum_p: " << impl.sum_p << endl;
    os << "sum_dp: " << impl.sum_dp << endl;

    os << "S: " << impl.S << endl;
    os << "dS: " << impl.dS << endl;

    os << "standard_errors: ";
    copy(impl.standard_errors.begin(), impl.standard_errors.end(), ostream_iterator<double>(os, " "));
    os << endl;

    return os;
}
*/


void Impl::extract_nonzero_frequencies(const vector<double>& f_in)
{
    vector<double> f_temp;

    for (size_t i=0; i<f_in.size(); ++i)
    {
        if (f_in[i] > 0)
        {
            f_temp.push_back(f_in[i]);
            nonzero_indices.push_back(i);
        }
    }

    f.resize(f_temp.size());
    copy(f_temp.begin(), f_temp.end(), f.begin());

    haplotype_count = f.size();
}


class LikelihoodFilter
{
    public:

    LikelihoodFilter(const vector<size_t>& nonzero_indices)
    :   nonzero_indices_(nonzero_indices)
    {}

    ublas::vector<double> operator()(shared_ptr<const HaplotypeLikelihoodRecord> record) const
    {
        ublas::vector<double> result(nonzero_indices_.size());
        vector<double> pair_logl = record->pair_logl();

        size_t result_index = 0;
        for (vector<size_t>::const_iterator it=nonzero_indices_.begin(); it!=nonzero_indices_.end(); ++it)
        {
            size_t index = *it + 1; // record includes Ref/0
            result[result_index++] = exp(pair_logl[index]);
        }

        return result;
    }

    private:
    const vector<size_t>& nonzero_indices_;
};


void Impl::process_haplotype_likelihood_records(const HaplotypeLikelihoodRecords& records,
                                                size_t position_begin,
                                                size_t position_end)
{
    HaplotypeLikelihoodRecords::const_iterator begin = records.lower_bound(position_begin);
    HaplotypeLikelihoodRecords::const_iterator end = records.lower_bound(position_end);
    size_t read_count = end - begin;
    likelihood_vectors.resize(read_count);
    transform(begin, end, likelihood_vectors.begin(), LikelihoodFilter(nonzero_indices));
}


void Impl::compute_sums()
{
    sum_p = ublas::zero_vector<double>(haplotype_count);
    sum_dp = ublas::zero_matrix<double>(haplotype_count, haplotype_count);

    for (LikelihoodVectors::const_iterator l=likelihood_vectors.begin(); l!=likelihood_vectors.end(); ++l)
    {
        double P = ublas::inner_prod(*l, f);
        ublas::vector<double> p = element_prod(*l, f)/P;
        sum_p += p;

        // dp(i,h) = dp_{jh}/df_i
        ublas::matrix<double> dp = ublas::zero_matrix<double>(haplotype_count, haplotype_count);
        for (size_t i=0; i<haplotype_count; ++i)
        {
            ublas::unit_vector<double> one_i(haplotype_count, i);
            row(dp, i) = element_prod(*l, one_i)/P - 
                         element_prod(*l, f) * (*l)[i] / P / P;            
        }
        sum_dp += dp;
    }
}


void Impl::compute_score_and_information()
{
    S = element_div(sum_p, f);

    // dS(i,h) = dS_h/df_i
    dS = ublas::zero_matrix<double>(haplotype_count, haplotype_count);
    for (size_t i=0; i<haplotype_count; ++i)
    {
        ublas::unit_vector<double> one_i(haplotype_count, i);
        ublas::vector<double> f2 = element_prod(f, f);
        row(dS, i) = element_div(row(sum_dp, i), f) -
                     element_prod(element_div(one_i, f2), sum_p);                     
    }
}


void Impl::compute_standard_errors()
{
    // compute covariance matrix, handling linear constraint 

    ublas::matrix<double> W(haplotype_count, haplotype_count - 1);
    for (size_t j=0; j<haplotype_count - 1; ++j)
        column(W, j) = ublas::unit_vector<double>(haplotype_count, 0) - 
                       ublas::unit_vector<double>(haplotype_count, j+1);

    ublas::matrix<double> dS_W = prod(dS, W);
    ublas::matrix<double> Wt_dS_W = prod(trans(W), dS_W);

    ublas::matrix<double> Wt_dS_W_inv(haplotype_count - 1, haplotype_count - 1);
    invert_matrix(Wt_dS_W, Wt_dS_W_inv);
   
    ublas::matrix<double> Wt_dS_W_inv_Wt = prod(Wt_dS_W_inv, trans(W));
    ublas::matrix<double> covariance = prod(W, Wt_dS_W_inv_Wt);
    covariance *= -1;

    standard_errors.resize(haplotype_count);

    for (size_t i=0; i<haplotype_count; ++i)
        standard_errors[i] = sqrt(covariance(i,i));
}


} // namespace


namespace StandardErrorEstimation {


vector<double> estimate_standard_errors(const HaplotypeLikelihoodRecords& records,
                                        size_t position_begin,
                                        size_t position_end,
                                        vector<double> f)
{
    Impl impl;
    impl.extract_nonzero_frequencies(f);
    impl.process_haplotype_likelihood_records(records, position_begin, position_end);
    impl.compute_sums();
    impl.compute_score_and_information();
    impl.compute_standard_errors();

    //cout << "impl:\n" << impl << endl;

    vector<double> result(f.size(), 0);
    for (size_t i=0; i<impl.haplotype_count; ++i)
        result[impl.nonzero_indices[i]] = impl.standard_errors[i];

    return result;
}


} // namespace StandardErrorEstimation


