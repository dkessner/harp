//
// HaplotypeReferenceMulti.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeReferenceMulti.hpp"
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <iterator>


using namespace std;


namespace {

// extract species name from GreenGenes 16S fasta id format
string extract_species_name_from_id(const string& fasta_id)
{
    istringstream iss(fasta_id);
    vector<string> tokens;
    copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(tokens));
    if (tokens.size() < 4) return "haprefmulti_unknown";
    return tokens[2] + " " + tokens[3]; 
}

} // namespace


void HaplotypeReferenceMulti::Haplotype::read_fasta_record(std::istream& is)
{
    // TODO: proper .fasta parsing with samtools api

    string id;
    getline(is, id);
    getline(is, this->sequence);
    if (!is) return;

    this->id = extract_species_name_from_id(id);
}


HaplotypeReferenceMulti::HaplotypeReferenceMulti(const string& filename_fasta)
{
    ifstream is(filename_fasta.c_str());
    if (!is) throw runtime_error(("[] File not found: " + filename_fasta).c_str());

    while (is)
    {
        HaplotypePtr haplotype(new Haplotype);
        haplotype->read_fasta_record(is);
        if (!haplotype->id.empty())
            haplotypes_.push_back(haplotype);
    }
}


HaplotypeReferenceMulti::HaplotypeReferenceMulti(const vector<string>& filenames_fasta)
{
    for (vector<string>::const_iterator it=filenames_fasta.begin(); it!=filenames_fasta.end(); ++it)
    {
        ifstream is(it->c_str());
        if (!is)
            throw runtime_error(("[HaplotypeReferenceMulti(vector<string>)] Unable to open file " + *it).c_str());

        HaplotypePtr haplotype(new Haplotype);
        haplotype->read_fasta_record(is);
        if (!haplotype->id.empty())
            haplotypes_.push_back(haplotype);
    }
}


size_t HaplotypeReferenceMulti::haplotype_count() const 
{
    return haplotypes_.size();
}


std::string HaplotypeReferenceMulti::id(size_t haplotype_index) const 
{
    return haplotypes_.at(haplotype_index)->id;
}


size_t HaplotypeReferenceMulti::sequence_length(size_t haplotype_index) const
{
    return haplotypes_.at(haplotype_index)->sequence.size();
}


string HaplotypeReferenceMulti::full_sequence(size_t haplotype_index, 
                                               size_t position_begin, 
                                               size_t position_end) const
{
    if (haplotype_index > haplotypes_.size())
        throw runtime_error("[HaplotypeReferenceMulti::full_sequence()] Bad haplotype index.");
    const Haplotype& haplotype = *haplotypes_[haplotype_index];
    if (position_end > haplotype.sequence.size() || position_begin > position_end)
        throw runtime_error("[HaplotypeReferenceMulti::full_sequence()] Bad haplotype position.");
    return haplotype.sequence.substr(position_begin, position_end-position_begin);
}


