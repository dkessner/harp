#include "faidx.h"
#include <iostream>


using namespace std;


int main()
{
    faidx_t* faidx = fai_load("../../../data/refseq/dmel-all-chromosome-r5.34.fasta");
    cout << faidx << endl;

    int length = 0;
    char* seq = fai_fetch(faidx, "YHet:51-100", &length);
    cout << length << " " << seq << endl;
    free(seq);

    seq = fai_fetch(faidx, "2L:101-200", &length);
    cout << length << " " << seq << endl;
    free(seq);

    fai_destroy(faidx);

    return 0;
}


