#
# Makefile for simulated read pipeline
#
# Darren Kessner
# Novembre Lab, UCLA
#


REGION := $(shell awk '/region/ {print $$2}' config.simreads)
FILENAME_REFSEQ := $(shell awk '/filename_refseq/ {print $$2}' config.simreads)
FILENAME_SNPS := $(shell awk '/filename_snps/ {print $$2}' config.simreads)


main: info out_harp_freq/haplotype_frequencies_all.txt


info: 
	@echo REGION: $(REGION)
	@echo FILENAME_REFSEQ: $(FILENAME_REFSEQ)
	@echo FILENAME_SNPS: $(FILENAME_SNPS)
	@echo

out_harp_freq/haplotype_frequencies_all.txt: out_harp_calc/haplotype_likelihoods.hlk
	harp freq $(REGION) out_harp_freq out_harp_calc/haplotype_likelihoods.hlk # TODO: add options (step, width, iter)

out_harp_calc/haplotype_likelihoods.hlk: reads.sorted.bam.bai
	harp calc reads.sorted.bam $(FILENAME_REFSEQ) $(FILENAME_SNPS) $(REGION) out_harp_calc

reads.sorted.bam.bai: reads.sorted.bam
	samtools index reads.sorted.bam

reads.sorted.bam: reads.bam
	samtools sort reads.bam reads.sorted

reads.bam: reads.sam
	samtools view -S -b reads.sam > reads.bam

reads.sam: config.simreads
	simreads



