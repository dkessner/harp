//
// likestat.cpp
//
// Darren Kessner
// Novembre Lab, UCLA
//


#include "HaplotypeLikelihoodRecord.hpp"
#include "HaplotypeFrequencyEstimation.hpp"
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <iterator>
#include <cmath>
#include <cstdlib>


using namespace std;


void calculate_chisq(const vector<double>& haplotype_frequencies, double N, double& chisq, double& chisq_zscore)
{
    double k = haplotype_frequencies.size();
    double p = 1./k;

    double sum = 0.;
    for (vector<double>::const_iterator it=haplotype_frequencies.begin(); it!=haplotype_frequencies.end(); ++it)
        sum += (*it - p) * (*it - p);
    
    chisq = N / p * sum;
    chisq_zscore = (chisq - (k-1))/sqrt(2*(k-1));  // df = k-1, mean = df, var = 2*df
}


double calculate_diversity_index(const vector<double>& f) // haplotype heterozygosity
{
    double sum = 0.;
    for (vector<double>::const_iterator it=f.begin(); it!=f.end(); ++it)
        sum += (*it * *it);
    return 1. - sum;
}


double calculate_entropy(const vector<double>& f)
{
    double sum = 0.;
    const double log2 = log(2.); 
    for (vector<double>::const_iterator it=f.begin(); it!=f.end(); ++it)
        sum += (- *it * log(*it));
    return sum / log2; // log_2(x) == log(x)/log(2)
}


void calculate_stats(size_t window_begin, size_t window_end, size_t record_count,
                     const vector<double>& f)
{
    double chisq = 0., chisq_zscore = 0.;
    calculate_chisq(f, record_count, chisq, chisq_zscore);

    double diversity = calculate_diversity_index(f);
    double entropy = calculate_entropy(f);

    cout << window_begin << "\t" << window_end 
        << "\t" << chisq << "\t" << chisq_zscore 
        << "\t" << diversity 
        << "\t" << entropy
        << endl;
}


void go(const HaplotypeLikelihoodRecords& records, size_t position_begin, size_t step)
{
    if (records.empty()) throw runtime_error("[likestat] No records.");

    size_t position_end = records.back()->read1_begin + 1;

    for (size_t position=position_begin; position<position_end; position+=step)
    {
        using HaplotypeFrequencyEstimation::estimate_haplotype_frequencies;
        vector<double> f = estimate_haplotype_frequencies(records, position, position+step, 1);

        size_t record_count = records.lower_bound(position+step) - records.lower_bound(position);
        calculate_stats(position, position+step, record_count, f);
    }
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 4)
        {
            cout << "Usage: likestat haplotype_likelihoods.hlk position_begin step\n";
            return 1;
        }

        const char* filename = argv[1];
        size_t position_begin = atoi(argv[2]);
        size_t step = atoi(argv[3]);

        HaplotypeLikelihoodRecords records(filename);
        go(records, position_begin, step);

        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


