//
// likedump.cpp
//
// Darren Kessner
// Novembre Lab, UCLA
//


#include "HaplotypeLikelihoodRecord.hpp"
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <iterator>


using namespace std;


string positions_string(const HaplotypeLikelihoodRecord& r)
{
    ostringstream oss;
    oss << "[" << r.read1_begin << "," << r.read1_end << ") "
        << "[" << r.read2_begin << "," << r.read2_end << ")";
    return oss.str();
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 3)
        {
            cout << "Usage: likedump [summary | records] haplotype_likelihoods.hlk\n";
            return 1;
        }

        string function = argv[1];
        const char* filename_hlk = argv[2];

        HaplotypeLikelihoodRecords records(filename_hlk);

        if (function == "summary")
        {
            cout << "haplotypes: " << records.haplotype_count << endl;
            cout << "records: " << records.size() << endl;

            if (!records.empty())
            {
                cout << "first: " << positions_string(*records.front()) << endl;
                cout << "last: " << positions_string(*records.back()) << endl;
            }
        }
        else if (function == "records")
        {
            for (HaplotypeLikelihoodRecords::const_iterator it=records.begin(); it!=records.end(); ++it)
                cout << **it << endl;
        }
        else
        {
            throw runtime_error(("[likedump] Unknown function " + function).c_str());
        }

        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


