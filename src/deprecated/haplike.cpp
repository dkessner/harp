//
// haplike.cpp
//
// Darren Kessner
// Novembre Lab, UCLA
//


#include "HaplotypeLikelihoodCalculator.hpp"
#include "HaplotypeLikelihoodRecord.hpp"
#include "BAMFile.hpp"
#include "boost/lambda/lambda.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include <iostream>
#include <iomanip>
#include <iterator>
#include <cmath>
#include <numeric>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
using namespace bampp;
using namespace boost::lambda;


vector<double> vector_sum(const vector<double>& a, const vector<double>& b)
{
    if (a.size() != b.size()) throw runtime_error("[haplike] Vector sizes differ");
    vector<double> result(a.size());
    transform(a.begin(), a.end(), b.begin(), result.begin(), _1 + _2);
    return result;
}


void sum_assign(vector<double>& result, vector<double>& summand)
{
    if (result.empty()) result.resize(summand.size());

    for (vector<double>::iterator it=result.begin(), jt=summand.begin(); it!=result.end(); ++it, ++jt)
        *it += *jt;
}


class PairLikelihoodProcessor : public bampp::PairProcessor
{
    public:

    PairLikelihoodProcessor(const Region& region,
                            shared_ptr<HaplotypeLikelihoodCalculator> calculator,
                            shared_ptr<HaplotypeLikelihoodRecords> records,
                            const string& outdir)
    :   region_(region),
        calculator_(calculator),
        records_(records),
        outdir_(outdir),
        os_filtered_(outdir_ / "filtered.txt"),
        os_haplotype_calls_(outdir_ / "haplotype_calls.txt"),
        count_pairs_(0), count_pairs_passed_filter_(0), count_unpaired_(0),
        posterior_count_(0)
    {}

    virtual void process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2);

    vector<double> get_haplotype_frequencies() const;

    void report_summary(ostream& os) const;

    private:

    Region region_; // copy
    shared_ptr<HaplotypeLikelihoodCalculator> calculator_;
    shared_ptr<HaplotypeLikelihoodRecords> records_;
    boost::filesystem::path outdir_;

    // output streams

    mutable boost::filesystem::ofstream os_filtered_;    
    boost::filesystem::ofstream os_haplotype_calls_;

    // counts

    public:

    size_t count_pairs_;
    size_t count_pairs_passed_filter_;
    size_t count_unpaired_;

    private:

    // haplotype frequencies

    vector<double> posterior_sum_;
    size_t posterior_count_;

    // private methods

    bool passes_filter(const Read& r) const;
    void report_haplotype(ostream& os, const string& name, const vector<double>& posterior) const;
};


bool PairLikelihoodProcessor::passes_filter(const Read& r) const
{
    if (!region_.contains(r.position))
    {
        // note: this filter is to prevent duplicates when computing likelihoods in sliding windows;
        //       samtools api returns all reads overlapping a region -- we restrict to reads whose
        //       start position is in the region

        os_filtered_ << r.name << " " << r.position << "\tread start position not in region\n";
        return false;
    }

    const int min_mapping_quality = 15;
    if (r.mapping_quality < min_mapping_quality) 
    {
        os_filtered_ << r.name << " " << r.position << "\tmapping_quality " << r.mapping_quality << endl;
        return false;
    }

    if (r.cigar.size() != 1 || 
        r.cigar[0].op!='M' || 
        r.cigar[0].count != r.sequence.size())
    {
        os_filtered_ << r.name << " " << r.position << "\tcigar " << r.cigar << endl;
        return false;
    }

    return true;
}


vector<double> PairLikelihoodProcessor::get_haplotype_frequencies() const
{
    vector<double> result(posterior_sum_.size());
    transform(posterior_sum_.begin(), posterior_sum_.end(), result.begin(), _1 / posterior_count_);
    return result;
}


vector<double> haplotype_posterior(const vector<double>& likelihood)
{
    // assuming uniform prior, i.e. un-log and rescale likelihood

    vector<double> posterior(likelihood.size() - 1); // remove Ref

    transform(likelihood.begin() + 1, likelihood.end(), posterior.begin(), (double (*)(double))exp);
    double sum = accumulate(posterior.begin(), posterior.end(), 0.);
    transform(posterior.begin(), posterior.end(), posterior.begin(), _1 / sum);

    return posterior;
}


void PairLikelihoodProcessor::report_haplotype(ostream& os, const string& name, const vector<double>& posterior) const
{
    os << name;
    for (vector<double>::const_iterator it=posterior.begin(); it!=posterior.end(); ++it)
        if (*it > .05)
            os << " " << it-posterior.begin() << ":" << *it;
    os << endl;
}


void PairLikelihoodProcessor::report_summary(ostream& os) const
{
    os << "reads: " << count_pairs_*2 + count_unpaired_ << endl
        << "pairs: " << count_pairs_ << endl
        << "pairs_passed_filter: " << count_pairs_passed_filter_ << endl
        << "unpaired: " << count_unpaired_ << endl;
}


void PairLikelihoodProcessor::process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2)
{
    if (!r1.get()) throw runtime_error("[haplike] Inconceivable: r1 is null.");

    if (!r2.get()) 
    {
        ++count_unpaired_;
        os_filtered_ << r1->name << " " << r1->position << "\tunpaired\n";
        return;
    }

    ++count_pairs_; 

    bool ok_1 = passes_filter(*r1);
    bool ok_2 = passes_filter(*r2);

    if (ok_1 && ok_2)
    {
        ++count_pairs_passed_filter_;

        vector<double> likelihood_1 = calculator_->likelihood(*r1);
        vector<double> likelihood_2 = calculator_->likelihood(*r2);

        HaplotypeLikelihoodRecordPtr record(new HaplotypeLikelihoodRecord);
        records_->push_back(record);
        record->read1_begin = r1->position;
        record->read1_end = r1->position + r1->sequence.size();
        record->read2_begin = r2->position;
        record->read2_end = r2->position + r2->sequence.size();
        record->likelihoods = vector_sum(likelihood_1, likelihood_2);

        vector<double> posterior = haplotype_posterior(record->likelihoods);
        sum_assign(posterior_sum_, posterior);
        ++posterior_count_;

        report_haplotype(os_haplotype_calls_, r1->name, posterior);
    }
}


struct Config
{
    string filename_bam;
    string filename_refseq;
    string filename_snps;
    string region;
    string outdir;

    Config(int argc, char* argv[])
    {
        const char* usage = "Usage: haplike file_bam file_refseq file_snps region outdir\n";

        if (argc != 6) throw runtime_error(usage);
        filename_bam = argv[1];
        filename_refseq = argv[2];
        filename_snps = argv[3];
        region = argv[4];
        outdir = argv[5];
    }
};


struct HaplotypeFrequencyEntry
{
    size_t haplotype; // index
    double frequency;
    string sequence;

    HaplotypeFrequencyEntry(size_t _haplotype, double _frequency, const string& _sequence)
    :   haplotype(_haplotype), frequency(_frequency), sequence(_sequence)
    {}
};


ostream& operator<<(ostream& os, const HaplotypeFrequencyEntry& entry)
{
    os << setw(3) << entry.haplotype << setw(12) << entry.frequency << " " << entry.sequence;
    return os;
}


bool has_greater_frequency(const HaplotypeFrequencyEntry& a, const HaplotypeFrequencyEntry& b)
{
    return a.frequency > b.frequency;
}


void print_haplotype_frequencies(const boost::filesystem::path& outdir, const vector<double>& haplotype_frequencies, const HaplotypeReference& hapref)
{
    vector<size_t> positions;
    for (HaplotypeReference::SNPTable::const_iterator it=hapref.snp_table.begin(); it!=hapref.snp_table.end(); ++it)
        positions.push_back(it->first);

    vector<HaplotypeFrequencyEntry> haplotype_frequency_entries;

    assert(haplotype_frequencies.size()+1 == hapref.haplotype_count());

    for (size_t i=0; i<haplotype_frequencies.size(); ++i)
        haplotype_frequency_entries.push_back(HaplotypeFrequencyEntry(i, haplotype_frequencies[i], hapref.variant_sequence(i+1, positions)));

    boost::filesystem::ofstream os(outdir / "haplotype_frequencies.txt");
    os << "# variant site count: " << positions.size() << endl;
    copy(haplotype_frequency_entries.begin(), haplotype_frequency_entries.end(), ostream_iterator<HaplotypeFrequencyEntry>(os, "\n"));

    sort(haplotype_frequency_entries.begin(), haplotype_frequency_entries.end(), has_greater_frequency); // inefficient copies

    boost::filesystem::ofstream os_sorted(outdir / "haplotype_frequencies_sorted.txt");
    os_sorted << "# variant site count: " << positions.size() << endl;
    copy(haplotype_frequency_entries.begin(), haplotype_frequency_entries.end(), ostream_iterator<HaplotypeFrequencyEntry>(os_sorted, "\n"));
}


void calculate_chisq(const vector<double>& haplotype_frequencies, double N, double& chisq, double& chisq_zscore)
{
    double k = haplotype_frequencies.size();
    double p = 1./k;

    double sum = 0.;
    for (vector<double>::const_iterator it=haplotype_frequencies.begin(); it!=haplotype_frequencies.end(); ++it)
        sum += (*it - p) * (*it - p);
    
    chisq = N / p * sum;
    chisq_zscore = (chisq - (k-1))/sqrt(2*(k-1));  // df = k-1, mean = df, var = 2*df
}


void go(const Config& config)
{
    if (!boost::filesystem::create_directories(config.outdir))
        throw runtime_error(("[haplike] Unable to create directory " + config.outdir).c_str());

    // We use an extended region for the reference sequence (to include reads that extend outside of it)
    // and for the bam read requests (to include the 2nd read from read pairs that begin in the region).
    // However, we use the exact region for filtering the read pairs.

    const size_t left_buffer_size = 200;
    const size_t right_buffer_size = 2000;

    Region extended_region(config.region);

    if (extended_region.begin > left_buffer_size) 
        extended_region.begin -= left_buffer_size;
    else
        extended_region.begin = 0;

    extended_region.end += right_buffer_size;

    // allocate objects and process reads

    shared_ptr<HaplotypeReference> hapref(
        new HaplotypeReference(config.filename_refseq,
                               config.filename_snps,
                               extended_region));

    shared_ptr<HaplotypeLikelihoodCalculator> calculator(new HaplotypeLikelihoodCalculator(hapref));
    shared_ptr<HaplotypeLikelihoodRecords> records(new HaplotypeLikelihoodRecords(hapref->haplotype_count()));
    PairLikelihoodProcessor processor(config.region, calculator, records, config.outdir);

    BAMFile bamfile(config.filename_bam);
    bampp::iterate_pairs(bamfile, extended_region, processor);
    
    // sort records -- note that order will not match order of other output streams (e.g. names)

    sort(records->begin(), records->end(), HaplotypeLikelihoodRecord_Read1Begin());

    // write records

    boost::filesystem::ofstream os_records(boost::filesystem::path(config.outdir) / "haplotype_likelihoods.hlk");
    records->write(os_records);
    os_records.close();

    // write haplotype frequencies

    vector<double> haplotype_frequencies = processor.get_haplotype_frequencies();
    if (!haplotype_frequencies.empty())
        print_haplotype_frequencies(config.outdir, haplotype_frequencies, *hapref);

    // write summary

    double chisq = 0., chisq_zscore = 0.;
    calculate_chisq(haplotype_frequencies, processor.count_pairs_passed_filter_, chisq, chisq_zscore);

    boost::filesystem::ofstream os_summary(boost::filesystem::path(config.outdir) / "summary.txt");
    os_summary << "region: " << config.region << endl
        << "filename_bam: " << config.filename_bam << endl
        << "filename_refseq: " << config.filename_refseq << endl
        << "filename_snps: " << config.filename_snps << endl << endl;
    processor.report_summary(os_summary);
    os_summary << endl
        << "chisq: " << chisq << endl
        << "chisq_zscore: " << chisq_zscore << endl;
    os_summary.close();
}


int main(int argc, char* argv[])
{
    try
    {
        Config config(argc, argv);
        cout << "region: " << config.region << endl;
        go(config);
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


