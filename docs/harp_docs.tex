%
% harp_docs.tex
%
% Copyright 2013 Darren Kessner
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%       http:%www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%

\documentclass{article}


\usepackage{graphicx}
\usepackage{hyperref}


% by default, \includegraphics (graphicx package) handles underscores in filenames
% underscore package allows use of non-escaped underscores, but breaks \includegraphics
% [strings] option fixes this, but underscore package must be loaded last (from underscore documentation)
\usepackage[strings]{underscore}


\newcommand{\harp}{\texttt{harp} }


\begin{document}


\title{\harp \\ Haplotype Analysis of Reads in Pools}
\author{Darren Kessner}
\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Introduction}

\harp is a command-line program for estimating the frequencies of known
haplotypes from pooled sequence data.  \harp implements an
Expectation-Maximization (EM) algorithm to obtain a maximum-likelihood 
estimate of the haplotype frequencies.

\begin{center}
\includegraphics[width=.50\textwidth]{fig/em_single_window.pdf}
\end{center}

\noindent \harp can be run in two modes:
\begin{enumerate}
    \item \emph{Single reference}.  Pooled reads have been mapped to a single reference, with
          the assumption that the haplotypes represent strains that are identical to the
          reference except for single-nucleotide variants.  In this mode, \harp needs:
          \begin{itemize}
              \item single BAM file with mapped reads
              \item reference sequence in FASTA format
              \item SNP file in DGRP format (comma-separated table containing variants
                    by genomic position and strain -- see Examples)
          \end{itemize}
    \item \emph{Multiple reference}.  Pooled reads have been mapped to multiple references,
          one for each strain/species of interest.  In this mode, \harp needs:
          \begin{itemize}
              \item list of BAM files (one for each mapping/species)
              \item corresponding list of reference sequences in FASTA format
          \end{itemize}
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Citation}

Details and evaluation of the method can be found in the paper: \\

Kessner D, Turner TL, Novembre J. 2013.  
Maximum Likelihood Estimation of Frequencies of Known Haplotypes from Pooled Sequence Data.
Molecular Biology and Evolution (accepted January 2013, Open Access) \\

\noindent \url{http://mbe.oxfordjournals.org/cgi/content/abstract/mst016}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Usage}

\harp is written in C++ and has been tested on OSX and Linux.  
The program includes multiple functions, which can be run as follows:
\begin{verbatim}
    harp <function_name> [arguments]
\end{verbatim}

Running \harp with no function name or arguments will print the usage information,
including the list of available functions and parameters.  Running 
$ \texttt{harp <function\_name>}$
with no arguments will give the list of required parameters for that function.

Parameters are \emph{(name, value)} pairs, and may be specified on the command line:
\begin{verbatim}
    --name value
\end{verbatim}
or in a configuration file:
\begin{verbatim}
    name = value
\end{verbatim}

\bigskip
Typical usage of \harp proceeds in two stages:
\begin{enumerate}
    \item \emph{Haplotype likelihood calculation}.  The harp function \texttt{like}
(single reference) or \texttt{like_multi} (multiple reference) creates an
intermediate binary file (extension \texttt{.hlk}) containing the computed
haplotype likelihoods.
    \item \emph{Frequency estimation}.  The harp function \texttt{freq} uses the 
computed haplotype likelihoods (\texttt{.hlk} file) to perform the frequency estimation.
\end{enumerate}
This allows the haplotype likelihoods to be calculated in larger regions, with the 
frequency estimation performed in smaller (possibly overlapping) windows.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Examples}

Included in the \harp package are example files to demonstrate usage of \texttt{harp}.  The
examples use a relative path to the \harp binary (\texttt{../../bin/harp}) so that
they may be run directly from the example directory after unzipping the package.

\subsection*{Example 1: Single Reference}

The following files may be found in \texttt{examples/example1_single_reference}:
\begin{itemize}
    \item \texttt{example1.bam}:  BAM file containing simulated mapped pooled reads.
          \texttt{example1.bam.bai} is an index file for faster access, created by 
          \texttt{samtools}.
    \item \texttt{example1.true.freqs}:  True haplotype frequencies under which the reads
        were drawn in the simulation used to create this example.  
        In this example, there are 4 haplotypes at frequences .4, .3, .2, .1.
    \item \texttt{example1.actual.freqs}:  Actual haplotype frequencies in the BAM file.  Note
        that these differ from the true frequecies, as they do in an experimental setting.
    \item \texttt{dmel_chr2L.fasta}: Reference sequence in FASTA format
        (first 100kb of \emph{D. melanogaster} chromosome 2L).  
        \texttt{dmel_chr2L.fasta.fai} is an index file for faster access,
        created by \texttt{samtools}.
    \item \texttt{snps.txt}: SNP file in DGRP format.  In the first
        row, \harp expects the first field to be the chromosome identifier, and
        the second field to be ``Ref'' (reference), followed by strain (haplotype)
        identifiers.  For subsequent rows, \harp expects the first column to be
        position, followed by the reference base, and then the other haplotype
        bases.  \harp currently ignores the trailing ``Coverage'' field and
        comma, and handles files without these.
        \texttt{snps.txt.idx} is an index for faster access, which can
        be created with the \texttt{index_snp_table} tool included in the \harp
        package.
    \item \texttt{harp_like.config, harp_freq.config}:  Example \harp configuration files 
        for the haplotype likelihood calculation and haplotype frequency estimation, respectively.
    \item \texttt{run_example1.sh}:  Shell script containing the \harp commands for this example.
\end{itemize}

\bigskip
\noindent First we perform the likelihood calculation with this command line:
\begin{verbatim}
../../bin/harp like --bam example1.bam --region 2L:30001-50000 
    --refseq dmel_chr2L.fasta --snps snps.txt --stem example1 -I
\end{verbatim}
We are using the \texttt{like} function, which has 4 required parameters:
\begin{itemize}
    \item \texttt{bam}: filename of the BAM file
    \item \texttt{region}: genomic region under consideration
    \item \texttt{refseq}: filename for the reference
    \item \texttt{snps}: filename for the SNP file
\end{itemize}
We are also using an optional parameter \texttt{--stem example1}, which \harp
uses as the filestem for constructing output filenames (e.g.
\texttt{example1.hlk}.  By default, \harp uses the BAM filename and region to
create the filestem.  In addition, we have added the \texttt{-I} flag (equivalent to
\texttt{--illumina_base_quality_encoding}) to indicate that the base quality scores
have been encoded using the Illumina encoding scheme.

Alternatively, we could have specified the parameters in a configuration file (\texttt{harp_freq.config}),
and we specify this on the command line:
\begin{verbatim}
../../bin/harp like -c harp_like.config
\end{verbatim}

In either case, \harp creates the file \texttt{example1.hlk}, which contains the computed
haplotype likelihoods.

\bigskip
Next, we perform the haplotype frequency estimation:
\begin{verbatim}
../../bin/harp freq --hlk example1.hlk --region 2L:30001-50000
\end{verbatim}
or:
\begin{verbatim}
../../bin/harp freq -c harp_freq.config
\end{verbatim}
This creates the file \texttt{example1.freqs} with the estimated haplotype frequencies.

These commands are collected in the shell script \texttt{run_example1.sh}, which you can run
directly to avoid typing in the above commands.


\subsection*{Example 2: Multiple Reference}

The following files may be found in \texttt{examples/example2_multiple_reference}:
\begin{itemize}
    \item \texttt{example1.reads.fastq}:  Contains the raw reads and base quality scores, in FASTQ format.
    \item \texttt{example2.true.freqs, example2.actual.freqs}:  The true haplotype frequencies for the simulated
        data, and the actual realized frequencies of the reads, as in Example 1.
    \item \texttt{ref?.fasta}:  Reference sequences in FASTA format.
    \item \texttt{ref?.bam}:  BAM mapping file for each reference sequence.  (These were created by
        mapping the reads in \texttt{example1.reads.fastq} to each reference sequence, 
        using \texttt{bwa} and \texttt{samtools}).
    \item \texttt{refseqlist.txt, bamlist.txt}:  Text files containing the lists of reference sequences
        and BAM files, respectively.
    \item \texttt{run_example2.sh}:  Shell script containing the commands for this example.
\end{itemize}

In this example, there are 4 known haplotypes in the pool, as well as 1 unknown haplotype.  Because
of the unknown, we first analyze our read data to calculate parameter values for the haplotype
likelihood filter, using the \mbox{\texttt{qual_hist_fastq}} tool included in the \harp package:
{\small
\begin{verbatim}
../../bin/qual_hist_fastq 75 example2.reads.fastq example2.harp_like_multi
\end{verbatim} }
This creates the file \texttt{example2.harp_like_multi.config}, which is a text file containing 
three parameters needed for haplotype likelihood filtering.  Now we can calculate haplotype likelihoods:
\begin{verbatim}
../../bin/harp like_multi --refseqlist refseqlist.txt --bamlist bamlist.txt 
    --stem example2 -c example2.harp_like_multi.config 
\end{verbatim}
Note that we passed the configuration file directly to \texttt{harp}, but we could have passed 
the parameters on the command line or appended them to an existing configuration file.

Finally, we perform the haplotype frequency estimation:
\begin{verbatim}
../../bin/harp freq --hlk example2.hlk
\end{verbatim}
which creates the file \texttt{example2.freqs} with the estimated haplotype frequencies.

These commands are collected in the shell script \texttt{run_example2.sh}, which you can run
directly to avoid typing in the above commands.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Function and Parameter Summary}

Running \harp with no arguments will print the usage text, which includes a summary of
functions and parameters available in \texttt{harp}.  This text is reproduced here for
convenience:

\begin{verbatim}
harp: Haplotype Analysis of Reads in Pools

Usage: harp function_name [args]

Available functions:
  bqi : calculate empirical base quality interpretation from monomorphic sites
  freq : estimate haplotype frequencies
  freq_stringmatch : estimate haplotype frequencies using simple string matching
  like : calculate haplotype likelihoods (create .hlk file)
  like_multi : calculate haplotype likelihoods from multilple refseqs/alignments 
               (create .hlk file)
  likedump : print info from likelihood (.hlk) file

For required parameters, use 'harp function_name' with no arguments.

Parameters may be specified on the command line:
  --name value    (e.g. --bam filename.bam)
or in a configuration file:
  name = value    (e.g. bam = filename.bam)

Parameters:

Command line only:
  -c [ --config ] arg (=config.harp) filename of configuration file

Input:
  -b [ --bam ] arg      BAM filename
  --bamlist arg         text file list of BAM files
  -r [ --region ] arg   region (e.g. 2L:1001-2000)
  --refseq arg          refseq filename
  --refseqlist arg      text file list of refseq filenames
  --snps arg            SNP filename

Output:
  --stem arg                 output filename stem [default: generated from BAM 
                             filename and region]
  --out arg                  directory for additional output files [default: 
                             (stem).output]
  --hlk arg                  haplotype likelihood filename (.hlk) [default: 
                             (stem).hlk]
  --freqs arg                haplotype frequencies filename (.freqs) [default: 
                             (stem).freqs]
  -v [ --verbose ]           verbose output
  --compute_standard_errors  compute standard errors

Likelihood calculation (like):
  --bqi arg                       base quality interpretation filename
  --min_mapping_quality arg (=15) minimum mapping quality

Likelihood multiple-reference calculation (like_multi):
  --logl_min_zscore arg (=-inf) filter out reads below specified minimum 
                                log-likelihood
  --logl_mean arg (=0)          required with logl_min_zscore
  --logl_sd arg (=1)            required with logl_min_zscore

Frequency estimation (freq):
  --window_step arg                  window step size [default: length of 
                                     region, i.e. single window]
  --window_width arg                 window width [default: window_step]
  --em_iter arg (=30)                EM iteration count
  --em_converge arg (=0)             EM convergence threshold
  --em_random_start_count arg (=0)   number of additional random starts
  --em_random_start_alpha arg (=1)   symmetric dirichlet parameter for random 
                                     initial estimates
  --em_random_start_seed arg (=0)    seed for random initial estimates
  --em_min_freq_cutoff arg (=0.0001) EM minimum frequency cutoff
  --haplotype_filter arg             haplotype filter (0 == Ref) [e.g. 
                                     1-3,5-7,10]

String matching requency estimation (freq_stringmatch):
  --max_mismatch arg (=3) maximum # of mismatches allowed

\end{verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Building}

Source code for harp is hosted on github: \\
\url{https://github.com/dkessner/harp} \\

\harp uses the \texttt{samtools} API for accessing BAM files.  Before building \texttt{harp}, you
will need to download and build \texttt{samtools}.  This can be done with the script
\texttt{get\_samtools.sh} in the source directory.

\harp makes extensive use of the Boost C++ libraries, as well as the Boost build
system, i.e. you will need to install Boost before building \harp.  To build,
run \texttt{bjam} from the project directory, where it will find \texttt{Jamroot} for the build
instructions.  Executables will be placed in the \texttt{bin} subdirectory.



\end{document}


