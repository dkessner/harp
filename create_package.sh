#!/bin/bash
#
# create_package.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


#
# filenames
#

datestamp=$(date +%y%m%d_%H%M%S)

if [ $(uname) == "Darwin" ]; then
    platform=osx
else
    platform=linux
fi

filename_stage=harp_${platform}_${datestamp}
filename_archive=harp_${platform}_${datestamp}.zip


#
# build
#

echo Building docs.
pushd docs
pdflatex harp_docs.tex > /dev/null
popd

echo Building executables.
pushd src
bjam
popd

echo Building examples.

pushd examples/build_example1_single_reference
./build_example1.sh
popd

pushd examples/build_example2_multiple_reference
./build_example2.sh
popd


#
# stage
#

echo Staging files in $filename_stage.

if [ -e $filename_stage ]
then
    echo "$filename_stage already exists."
    exit 1
fi

mkdir $filename_stage

docs="README NOTICE LICENSE docs/harp_docs.pdf"
cp $docs $filename_stage

mkdir $filename_stage/bin
binaries=bin/*
cp $binaries $filename_stage/bin

mkdir $filename_stage/examples
cp -r examples/example1_single_reference $filename_stage/examples
cp -r examples/example2_multiple_reference $filename_stage/examples

#
# package
#

echo Creating $filename_archive.
zip -r $filename_archive $filename_stage


